package com.auz.SupportedUtils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQLDBConnection {


	//JDBC driver name and database URL
	final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	final String DB_URL = "jdbc:mysql://10.59.248.209:3306/identity_staging?useSSL=false";
	final String PSQL_DB_URL = "jdbc:postgresql://10.59.246.90:5432/workflow_staging";
	//Database credentials
	final String user= "hirereadonly";
	final String pass = "LeTEctEkEtimenaNdAch";
	final String PSQLuser= "readonly";
	final String PSQLpass = "auzmor@123";

	Connection conn = null ;
	Statement stmt = null ;
	PreparedStatement prepStmt;

	public  String GetOrganisationUUID(String id) {
		String uuid = null;
		try {
			//Register JDBC driver
			Class.forName(JDBC_DRIVER);
			try {
				//Open a connection
				conn = DriverManager.getConnection(DB_URL, user, pass);
				//Execute a query
				stmt = conn.createStatement();
				String sql = "SELECT uuid from identity_staging.organizations WHERE domain_name=" + "'" +id +"'";
				ResultSet executeQuery = stmt.executeQuery(sql);
				//Extract data from Result Set
				while(executeQuery.next()) {
					uuid = executeQuery.getString("UUID");
					System.out.println("Organization id is: " +uuid);
				}
				//Close all connections
				executeQuery.close();
				stmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return uuid;

	}


	public  String GetOrganisationName(String id) {
		String name = null;
		try {
			//Register JDBC driver
			Class.forName(JDBC_DRIVER);


			try {
				//Open a connection
				conn = DriverManager.getConnection(DB_URL, user, pass);

				//Execute a query

				stmt = conn.createStatement();
				String sql = "SELECT domain_name from identity_staging.organizations WHERE uuid=" + "'" +id +"'";
				ResultSet executeQuery = stmt.executeQuery(sql);

				//Extract data from Result Set
				while(executeQuery.next()) {
					name = executeQuery.getString("domain_name");
					System.out.println("Organization name is: " +name);
				}
				//Close all connections
				executeQuery.close();
				stmt.close();
				conn.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return name;

	}
	
	public  String PSQLdbquery(String query) {
		String uuid = null;
		try {
			//Register JDBC driver
			Class.forName(JDBC_DRIVER);
			try {
				//Open a connection
				conn = DriverManager.getConnection(PSQL_DB_URL, PSQLuser, PSQLpass);
				//Execute a query
				stmt = conn.createStatement();
				ResultSet executeQuery = stmt.executeQuery(query);
				//Extract data from Result Set
				while(executeQuery.next()) {
					uuid = executeQuery.getString("id");
				}
				//Close all connections
				executeQuery.close();
				stmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return uuid;

	}


}
