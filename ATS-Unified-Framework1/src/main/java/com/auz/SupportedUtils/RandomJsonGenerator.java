package com.auz.SupportedUtils;

import java.nio.charset.Charset;
import java.util.Random;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;

import com.fasterxml.uuid.Generators;
public final class RandomJsonGenerator {

	public static String random(Integer length, PermittedCharacters permittedCharacters) {
		String randomString = null;
		if (PermittedCharacters.ALPHABETS.equals(permittedCharacters)) {
			randomString = randomString(length);
		} else if (PermittedCharacters.NUMERIC.equals(permittedCharacters)) {
			randomString = randomInteger(length);
		} else if (PermittedCharacters.ALPHANUMERIC.equals(permittedCharacters)) {
			randomString = randomAlphanumeric(length);
		} else if (PermittedCharacters.EMAIL.equals(permittedCharacters)) {
			randomString = randomEmailAddress(length);
		}
		return randomString;
	}

	private static String randomInteger(Integer length) {
		return RandomStringUtils.randomNumeric(length);
	}

	/**
	 * Generates random String.
	 *
	 * @param length length of random characters to be generated
	 */
	private static String randomString(Integer length) {
		return RandomStringUtils.random(length, true, false);
	}

	/**
	 * Generates random alphanumeric String.
	 *
	 * @param length length of random alphanumeric characters to be generated
	 */
	private static String randomAlphanumeric(Integer length) {
		return RandomStringUtils.randomAlphanumeric(length);
	}

	/**
	 * Generates random alphabetic String.
	 *
	 * @param length length of random alphabetic characters to be generated
	 */
	public static String randomAlphabetic(Integer length) {
		return RandomStringUtils.randomAlphabetic(length);
	}

	/**
	 * Generates random emailaddress for @example.com domain  in lower case
	 *
	 * @param length length of random alphanumeric characters to be generated for the local part of email address
	 */

	public static String randomPermissionName() {
		String jobtitle = "Recruiter" +randomAlphabetic(2);
		return jobtitle;
	}
	public static String randomOrgName() {
		String jobtitle = "ATS" +randomAlphabetic(2)+randomInteger(3);
		return jobtitle;
	}
	public static String randomGroupName() {
		String jobtitle = "Human" +randomAlphabetic(2)+"Resource"+randomInteger(3);
		return jobtitle;
	}

	public static String randomRoleName() {
		String jobtitle = "Admini" +randomAlphabetic(2)+"strator"+randomInteger(3);
		return jobtitle;
	}
	public static String randomRoleNameUpdate() {
		String jobtitle = "Recrui" +randomAlphabetic(2)+"ter"+randomInteger(3);
		return jobtitle;
	}
	public static String randomAddressLine1() {
		String jobtitle = "D" +randomAlphabetic(2)+"Street" +" "+randomInteger(3);
		return jobtitle;
	}
	public static String randomAddressLine2() {
		String jobtitle = randomAlphabetic(2)+ "WR"+randomInteger(3);
		return jobtitle;
	}
	public static String randomStateName() {
		String name = "NY"+randomInteger(4)+ "Office" +" " +randomString(3);
		return name;
	}
	public static String randomZipCode() {
		String name = "492"+randomInteger(4);
		return name;
	}
	public static String randomTagName() {
		String jobtitle = "Tag" +randomAlphabetic(2);
		return jobtitle;
	}
	public static String randomEmailAddress(Integer length) {
		String email = randomAlphanumeric(length) + "@example.com";
		return email.toLowerCase();
	}
	public static String randomFirstName() {
		String name = "John"+randomInteger(4)+randomString(3);
		return name;
	}
	public static String randomLastName() {
		String name = "Doe"+randomInteger(4)+randomString(3);
		return name;
	}
	public static String randomfavicon() {
		String name = "http://asavd.w"+randomString(4);
		return name;
	}
	public static String randomDomainName() {
		String name = "ats"+randomInteger(4);
		return name;
	}
	public static String randomOrgEmail() {
		String name = "She"+"+"+randomInteger(2)+randomString(3)+"@example.com";
		return name;
	}
	public static String randomTemplateName() {
		String name = "Template"+randomInteger(4);
		return name;
	}
	public static String randomTemplateSubject() {
		String name = "TemplateSubject"+randomInteger(4);
		return name;
	}
	public static String randomText() {
		String name = "Test Comment"+randomInteger(4);
		return name;
	}
	public static String randomZoomname() {
		String name = "Zoom meeting"+randomInteger(4);
		return name;
	}
	public static String randomWebinarname() {
		String name = "Zoom Webinar"+randomInteger(4);
		return name;
	}
	public static String randomNotificationname() {
		String name = "General"+randomInteger(4);
		return name;
	}
	public static String randomUUIDGenerator() {
		UUID uuid2 = Generators.randomBasedGenerator().generate();
		System.out.println("UUID : "+uuid2);
		String id = uuid2.toString().replaceAll("-", "");
		return id;
	}
	
}
