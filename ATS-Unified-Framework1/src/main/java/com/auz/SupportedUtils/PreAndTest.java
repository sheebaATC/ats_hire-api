package com.auz.SupportedUtils;

import java.io.FileWriter;

import org.json.simple.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

public class PreAndTest extends Reporter {

	WebDriver driver;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("suiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);

	@BeforeSuite
	public void beforeSuite() throws Exception {
		startReport();
		getcookies();
		// getLinkedIncookie();
		// getcookies_dev();

	}

	@BeforeClass
	public void beforeClass() {
		startTestCase(testSuiteName, testSuiteDescription);
	}

	@AfterMethod
	public void afterMethod(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
			test.log(Status.FAIL, MarkupHelper.createLabel(result.getName() + " FAILED ", ExtentColor.RED));
			test.fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			test.log(Status.PASS, MarkupHelper.createLabel(result.getName() + " PASSED ", ExtentColor.GREEN));
		} else {
			test.log(Status.SKIP, MarkupHelper.createLabel(result.getName() + " SKIPPED ", ExtentColor.ORANGE));
			test.skip(result.getThrowable());
		}
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		endResult();
		System.out.println("Cookies:  " + driver.manage().getCookies());
		driver.quit();
	}

	@Override
	public long takeSnap() {
		return 0;
	}

	public void reportUpdate(String testname) {
		lib.testName.set(testname);
		svcTest = startTestModule(lib.testName.get());
		svcTest.assignAuthor("Harsha");
		svcTest.assignCategory("Smoke");
	}

	public void getcookies() {
		JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
		try {
			String environment = "STAGING";
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
			if (environment.equalsIgnoreCase("STAGING")) {
				driver.get(Constants.STAGING_APPURL);
				driver.manage().window().maximize();
				driver.findElement(By.id("email")).sendKeys((String) jsonsuitetestData.get("username"));
				driver.findElement(By.id("password")).sendKeys((String) jsonsuitetestData.get("password"));
				driver.findElement(By.id("signin-button")).click();
				driver.navigate().to(Constants.STAGING_REDIRECT_APPURL);
				Cookie cookie = driver.manage().getCookieNamed("connect.sid");
				String cookievalue = cookie.getValue();
				FileWriter fw = new FileWriter(Constants.RESOURCE_DIR + "cookie.txt");
				fw.write(cookievalue);
				fw.close();
			} else {
				driver.get(Constants.TESTING_APPURL);
				driver.manage().window().maximize();
				driver.findElement(By.id("email")).sendKeys((String) jsonsuitetestData.get("username"));
				driver.findElement(By.id("password")).sendKeys((String) jsonsuitetestData.get("password"));
				driver.findElement(By.id("signin-button")).click();
				driver.navigate().to(Constants.TESTING_REDIRECT_APPURL);
				Cookie cookie = driver.manage().getCookieNamed("connect.sid");
				String cookievalue = cookie.getValue();
				FileWriter fw = new FileWriter(Constants.RESOURCE_DIR + "cookie.txt");
				fw.write(cookievalue);
				fw.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void getLinkedIncookie() {
		try {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
			String url = "www.linkedin.com/in/sheeba-sasidharan-a405a677";
			driver.get("https://www.linkedin.com/in/sheeba-sasidharan-a405a677");
			Cookie cookie = driver.manage().getCookieNamed("connect.sid");
			String cookievalue = cookie.getValue();
			FileWriter fw = new FileWriter(Constants.RESOURCE_DIR + "cookie.txt");
			fw.write(cookievalue);
			fw.close();
			Cookie CSRFToken = driver.manage().getCookieNamed("JSESSIONID");
			String CSRFTokenvalue = CSRFToken.getValue();
			FileWriter fw1 = new FileWriter(Constants.RESOURCE_DIR + "CSRFToken.txt");
			fw1.write(CSRFTokenvalue);
			fw1.close();
			Cookie linkedincookie = driver.manage().getCookieNamed("li_sugr");
			String linkedincookievalue = linkedincookie.getValue();
			FileWriter fw2 = new FileWriter(Constants.RESOURCE_DIR + "LinkedIncookie.txt");
			fw2.write(linkedincookievalue);
			fw2.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void getcookies_dev() {
		try {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
			driver.get(Constants.DEV_APPURL);
			driver.manage().window().maximize();

			driver.findElement(By.id("email")).sendKeys(Constants.DEV_USERNAME);
			driver.findElement(By.id("password")).sendKeys(Constants.DEV_PWD);
			driver.findElement(By.id("signin-button")).click();
			driver.navigate().to(Constants.DEV_REDIRECT_APPURL);
			Cookie cookie = driver.manage().getCookieNamed("connect.sid");
			String cookievalue = cookie.getValue();
			FileWriter fw = new FileWriter(Constants.RESOURCE_DIR + "cookie.txt");
			fw.write(cookievalue);
			fw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getTestRailID(Object testrailObj) {
		String tesrailID = testrailObj.toString();
		String tesrailIDNumber = tesrailID.replaceAll("[^0-9]", "").trim();
		return tesrailIDNumber;
	}

}
