package com.auz.SupportedUtils;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.auz.selenium.CoreUtils.SeleniumBase;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

public class ProjectSpecificMethods extends SeleniumBase {

	@BeforeSuite
	public void beforeSuite() throws Exception {
		startReport();
	}

	@BeforeMethod
	public void beforeMethod() {
		driver = startApp("chrome", false);

	}

	@BeforeClass
	public void beforeClass() {
		startTestCase(testSuiteName, testSuiteDescription);
	}

	@AfterMethod
	public void getResult(ITestResult result) throws Exception {
		if (result.getStatus() == ITestResult.FAILURE) {
			test.log(Status.FAIL, MarkupHelper.createLabel(result.getName() + " - Test Case Failed", ExtentColor.RED));
			test.log(Status.FAIL,
					MarkupHelper.createLabel(result.getThrowable() + " - Test Case Failed", ExtentColor.RED));
			String screenshotPath = getScreenshotPath(result.getName(), driver);

			test.fail("Test Case Failed Snapshot is below " + test.addScreenCaptureFromPath(screenshotPath));
		} else if (result.getStatus() == ITestResult.SKIP) {
			test.log(Status.SKIP,
					MarkupHelper.createLabel(result.getName() + " - Test Case Skipped", ExtentColor.ORANGE));
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			test.log(Status.PASS, MarkupHelper.createLabel(result.getName() + " Test Case PASSED", ExtentColor.GREEN));
		}
		driver.quit();
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		endResult();
	}

	public void reportUpdate(String testname) {
		// setting Report data
		lib.testName.set(testname);
		svcTest = startTestModule(lib.testName.get());
		svcTest.assignAuthor("Abhinav");
		svcTest.assignCategory("Sanity");
	}

	/*
	 * public static String getTestRailID(Object testrailObj) { String tesrailID =
	 * testrailObj.toString(); String tesrailIDNumber =
	 * tesrailID.replaceAll("[^0-9]", "").trim(); return tesrailIDNumber; }
	 */

}
