package com.auz.SupportedUtils;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

import org.json.simple.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.util.JSON;


public final class MongoDBConnectionManager {
	
	
	@SuppressWarnings("deprecation")
	public Object MongoDatabaseDev(String key, String value){
			Object object= null;
		MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://10.59.253.147:27017"));
		DB database = mongoClient.getDB("integration-development");
		MongoDatabase databases = mongoClient.getDatabase("integration-development");
		System.out.println("- Database: " + databases);
		MongoIterable<String> collections = databases.listCollectionNames();
		for (String colName : collections) {
			System.out.println("\t + Collection: " + colName);
		}
		DBCollection collections2 = database.getCollection("organisations");
		System.out.println("\t + Collection2: " + collections2);
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put(key, value);
		DBCursor cursor = collections2.find(searchQuery);
		while (cursor.hasNext()) {
		    System.out.println(cursor.next());
		    object = cursor.one().get("_id");
		}
		mongoClient.close();
		return object;
		}
	
	@SuppressWarnings("deprecation")
	public String MongoDatabaseQA(BasicDBObject searchQuery){
		String result2 = null; 
		Object object= null;
		MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://10.59.253.147:27017"));
		DB database = mongoClient.getDB("integration-qa");
		MongoDatabase databases = mongoClient.getDatabase("integration-qa");
		MongoIterable<String> collections = databases.listCollectionNames();
		for (String colName : collections) {
			System.out.println("\t + Collection: " + colName);
		}
		DBCollection collections2 = database.getCollection("organisations");
		DBCursor cursor = collections2.find(searchQuery);
		while (cursor.hasNext()) {
			System.out.println(cursor.next());
			object = cursor.one().get("integrations");
			String string = object.toString();
		    result2 = (string.replaceAll("[{}]", " "));
		    result2.replaceAll(",", " ");
		}
		mongoClient.close();
		return result2;
		}
	
	@SuppressWarnings("deprecation")
	public String MongoDatabaseQAOrgID(BasicDBObject searchQuery){
		String result2 = null; 
		Object object= null;
		MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://10.59.253.147:27017"));
		DB database = mongoClient.getDB("integration-qa");
		MongoDatabase databases = mongoClient.getDatabase("integration-qa");
		MongoIterable<String> collections = databases.listCollectionNames();
//		for (String colName : collections) {
//			System.out.println("\t + Collection: " + colName);
//		}
		DBCollection collections2 = database.getCollection("organisations");
		DBCursor cursor = collections2.find(searchQuery);
		while (cursor.hasNext()) {
			System.out.println(cursor.next());
			object = cursor.one().get("_id");
			String string = object.toString();
		    result2 = (string.replaceAll("[{}]", " "));
		    result2.replaceAll(",", " ");
		}
		mongoClient.close();
		return result2;
		}
	
	
	@SuppressWarnings("deprecation")
	public String MongoDatabaseStaging(BasicDBObject searchQuery, String searchid){
		Object object= null;
		String result2 = null; 
		MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://10.59.253.147:27017"));
		DB database = mongoClient.getDB("integration-staging");
		MongoDatabase databases = mongoClient.getDatabase("integration-staging");
		System.out.println("- Database: " + databases);
		MongoIterable<String> collections = databases.listCollectionNames();
		for (String colName : collections) {
			System.out.println("\t + Collection: " + colName);
		}
		DBCollection collections2 = database.getCollection("organisations");
		DBCursor cursor = collections2.find(searchQuery);
		 
		while (cursor.hasNext()) {
			System.out.println(cursor.next());
			object = cursor.one().get(searchid);
			String string = object.toString();
		    result2 = (string.replaceAll("[{}]", " "));
		    result2.replaceAll(",", " ");
		}
		mongoClient.close();
		return result2;


		}
	
	@SuppressWarnings("deprecation")
	public String MongoDatabaseStagingwithTwoQueries(BasicDBObject searchQuery, String searchid, BasicDBObject fields){
		Object object= null;
		String result2 = null; 
		MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://10.59.253.147:27017"));
		DB database = mongoClient.getDB("integration-staging");
		MongoDatabase databases = mongoClient.getDatabase("integration-staging");
		System.out.println("- Database: " + databases);
		MongoIterable<String> collections = databases.listCollectionNames();
		for (String colName : collections) {
			System.out.println("\t + Collection: " + colName);
		}
		DBCollection collections2 = database.getCollection("organisations");
		DBCursor cursor = collections2.find(searchQuery, fields);
		 
		while (cursor.hasNext()) {
			System.out.println(cursor.next());
			object = cursor.one().get(searchid);
			String string = object.toString();
		    result2 = (string.replaceAll("[{}]", " "));
		    result2.replaceAll(",", " ");
		}
		mongoClient.close();
		return result2;


		}


}

