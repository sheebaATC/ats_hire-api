package com.auz.SupportedUtils;

public class Constants {
	public static final String RESOURCE_DIR = "./src/test/resources/";
	public static final String DATAINPUT_DIR="./src/test/resources/TestData/";
	public static final String JSONOBJECT_JOB_DIR="/src/test/resources/JsonObject/Job/";
	public static final String JSONOBJECT_SMS_DIR="/src/test/resources/JsonObject/SMS/";
	public static final String JSONOBJECT_EMPLOYEE_DIR="/src/test/resources/JsonObject/Employee/";
	public static final String JSONOBJECT_ZOOM_DIR="/src/test/resources/JsonObject/Zoom/";
	public static final String JSONOBJECT_JOBBOARDS_DIR="/src/test/resources/JsonObject/JobBoards/";
	public static final String JSONOBJECT_NOTIFICATION_DIR="/src/test/resources/JsonObject/NotificationSections/";
	public static final String JSONOBJECT_GOTOMEETING_DIR="/src/test/resources/JsonObject/GoToMeeting/";
	public static final String JSONOBJECT_NOTE_DIR="/src/test/resources/JsonObject/Notes/";
	public static final String JSONOBJECT_CANDIDATE_DIR="/src/test/resources/JsonObject/Candidate/";
	public static final String JSONOBJECT_ORGINTERVIEWSTAGES_DIR="/src/test/resources/JsonObject/OrgInterviewStages/";
	public static final String JSONOBJECT_GROUP_DIR="/src/test/resources/JsonObject/Group/";
	public static final String JSONOBJECT_FORM_DIR="/src/test/resources/JsonObject/Form/";
	public static final String JSONOBJECT_SSO_DIR="/src/test/resources/JsonObject/SSO/";
	public static final String JSONOBJECT_INTERVIEW_DIR="/src/test/resources/JsonObject/Interview/";
	public static final String JSONOBJECT_INTERVIEWKIT_DIR="/src/test/resources/JsonObject/InterviewKit/";
	public static final String JSONOBJECT_INTEGRATION_DIR="/src/test/resources/JsonObject/Integrations/";
	public static final String JSONOBJECT_CUSTOMFIELD_DIR="/src/test/resources/JsonObject/CustomField/";
	public static final String JSONOBJECT_TEMPLATE_DIR="/src/test/resources/JsonObject/Template/";
	public static final String JSONOBJECT_BULKUPLOAD_DIR="/src/test/resources/JsonObject/Bulk_Upload/";
	public static final String JSONOBJECT_FOLLOW_DIR="/src/test/resources/JsonObject/Follow/";
	public static final String JSONOBJECT_DOCUMENT_DIR="/src/test/resources/JsonObject/Document/";
	public static final String JSONOBJECT_PERMISSION_DIR="/src/test/resources/JsonObject/Permission/";
	public static final String JSONOBJECT_LOCATION_DIR="/src/test/resources/JsonObject/Location/";
	public static final String JSONOBJECT_WORKFLOWS_DIR="/src/test/resources/JsonObject/Workflows/";
	public static final String JSONOBJECT_ROLE_DIR="/src/test/resources/JsonObject/Role/";
	public static final String JSONOBJECT_EVALUATION_DIR="/src/test/resources/JsonObject/Evaluation/";
	public static final String JSONOBJECT_TAG_DIR="/src/test/resources/JsonObject/Tag/";
	public static final String JSONOBJECT_MAIL_DIR="/src/test/resources/JsonObject/Mail/";
	public static final String JSONOBJECT_EVENT_DIR="/src/test/resources/JsonObject/Event/";
	public static final String JSONOBJECT_ORGANIZATION_DIR="/src/test/resources/JsonObject/Organization/";
	public static final String AUTH_USERNAME="admin";
	public static final String AUTH_PASSWORD="admin";
	public static final String APIKEY="Eg37p/meiNA2EyhZukRlwg==";
	public static final String JSON_CONTENTTYPE="application/json";
	public static final String CREATE_JOB_POST_URL="https://hire-staging.api.auzmor.com/api/v1";
	public static final String NOTIFICATIONS_URL="https://notification-staging.api.auzmor.com/v1/";
	public static final String WORKFLOW_URL = "https://workflow-staging.api.auzmor.com/api";
	//public static final String CREATE_JOB_POST_URL="https://hire-qa.api.auzmor.com/api/v1";
	public static final String PLATFORM_DEV_URL="https://platform-dev.api.auzmor.com/api/v1";
	public static final String INTEGRATION_DEV_URL="https://integration-dev.api.auzmor.com/v1";
	public static final String INTEGRATION_QA_URL="https://integration-qa.api.auzmor.com/v1";
	public static final String INTEGRATION_STAGING_URL="https://integration-staging.api.auzmor.com/v1";
	public static final String HIRE_QA_URL = "https://hire-qa.api.auzmor.com/api/v1";
	public static final String HIRE_DEV_URL = "https://hire-dev.api.auzmor.com/api/v1";
	public static final String STAGING_APPURL1="https://identity-staging.api.auzmor.com/";
	
	public static final String DEV_URL= "https://hire-dev.api.auzmor.com/api/v1/notifications?offset=0";
	public static final String COMMONINPUT_JSONFILE ="CreateOrder_WithValidData";

	public static final String ERROR_TRANSACTION_RESPONSE_MESSAGE_JSONDATA="errorTransactionJSONData";
	public static final String SUCCESS_TRANSACTION_INPUT_AND_RESPONSE_JSONDATA="successTransactionJSONData";
	public static final String STAGING_APPURL="https://identity-staging.api.auzmor.com/login";
	public static final String STAGING_REDIRECT_APPURL="https://hire-staging.api.auzmor.com/";
	public static final String TESTING_APPURL="https://identity-qa.api.auzmor.com/login";
	public static final String TESTING_REDIRECT_APPURL="https://hire-qa.api.auzmor.com/";
	public static final String DEV_APPURL="https://identity-dev.api.auzmor.com/login";
	public static final String DEV_REDIRECT_APPURL="https://hire-dev.auzmor.com/";
	public static final String COMM_APPURL ="https://communication-dev.api.auzmor.com";
//	public static final String STAGING_USERNAME1="automation@mailinator.com";
//	public static final String STAGING_PWD2="Test!1234";
	public static final String STAGING_USERNAME="vinoth@american-technology.net";
	public static final String STAGING_PWD="Test@123";
//	public static final String STAGING_USERNAME="sheeba@american-technology.net";
//	public static final String STAGING_PWD="Test@123";
	public static final String TESTING_USERNAME="vinoth@american-technology.net";
	public static final String TESTING_PWD="Test@123";
	public static final String DEV_USERNAME="shiv+2@auzmor.com";
	public static final String DEV_PWD="amaramar";


	

	public static final String HUBURL="localhost";
	public static final String HUBPORT="4444";


	
}
