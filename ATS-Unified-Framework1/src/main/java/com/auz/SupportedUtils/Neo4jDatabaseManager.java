package com.auz.SupportedUtils;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.neo4j.driver.AuthTokens;
import org.neo4j.driver.Driver;
import org.neo4j.driver.GraphDatabase;
import org.neo4j.driver.Record;
import org.neo4j.driver.Result;
import org.neo4j.driver.Session;
import org.neo4j.driver.Transaction;

import static org.neo4j.driver.Values.parameters;

public class Neo4jDatabaseManager {	

	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws Exception {

		//JDBC driver name and database URL
		final String JDBC_DRIVER = "org.neo4j.jdbc.Driver";
		final String DB_URL = "jdbc:neo4j:bolt://10.59.250.57";

		//Database credentials
		final String user= "neo4j";
		final String pass = "tz!E3MEHE3u1G&H%u#oDmUFgQ";
		Connection con = DriverManager.getConnection("jdbc:neo4j:bolt://10.59.250.57", user, pass);
		//String query = "MATCH (note:Note) RETURN note.uuid";
		String query = "MATCH (note {uuid : '64278358c2f84b6d9bfeeb5657bcbc15'}) RETURN note, note.text";
	    try (Statement stmt = con.createStatement()) {
	        ResultSet rs = stmt.executeQuery(query);
	        while (rs.next()) {
	        	System.out.println("Result: " +rs);
	        	String uuid = rs.getString("note.text");
				System.out.println("Organization id is: " +uuid);
	        }
	    }
	    con.close();
	}


}






