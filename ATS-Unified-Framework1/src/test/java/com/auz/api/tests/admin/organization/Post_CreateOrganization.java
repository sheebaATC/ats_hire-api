package com.auz.api.tests.admin.organization;

import static org.testng.Assert.assertEquals;

import java.io.FileWriter;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.SupportedUtils.MySQLDBConnection;
import com.auz.SupportedUtils.RandomJsonGenerator;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class Post_CreateOrganization extends RestAssuredBase {

	public static HashMap<String, String> headers;
	public static String payLoadBody;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("adminSuiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	public static String createdid = "";
	public static String domainName = "";
	MySQLDBConnection db = new MySQLDBConnection();

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Post request to create/get/patch organization";
			testSuiteDescription = "Validating in passing the request to create/patch/get the new organization";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Admin_Verify_if_user_can_create_a_new_organization() throws Exception {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_if_user_can_create_a_new_organization", "id");
		JSONObject jsonObject = new JSONObject();
		String Dname = RandomJsonGenerator.randomDomainName();
		jsonObject.put("description", "Test Organization");
		jsonObject.put("domain", Dname);
		jsonObject.put("email", RandomJsonGenerator.randomOrgEmail());
		jsonObject.put("favicon", "string");
		jsonObject.put("firstName", "Sheeba");
		jsonObject.put("lastName", "S");
		jsonObject.put("logo", "string");
		jsonObject.put("organizationName", Dname);
		jsonObject.put("password", "Test@123");
		jsonObject.put("primaryColor", "{\"r\":0,\"g\":15,\"b\":61,\"a\":1}");
		jsonObject.put("secondaryColor", "{\"r\":38,\"g\":100,\"b\":196,\"a\":1}");

		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_ORGANIZATION_DIR + "addOrganization.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC001_Admin_Verify_if_user_can_create_a_new_organization";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_ORGANIZATION_DIR
					+ "addOrganization.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.STAGING_APPURL1 + "organization");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(388, 420);
			System.out.println("CreatedID: " + id);
			createdid = id;
			domainName = Dname;
			MySQLDBConnection db = new MySQLDBConnection();
			String valueFromDB = db.GetOrganisationUUID(Dname);
			String rcode = String.valueOf(responsecode);
			// Assert.assertEquals(true, jsonAsString.contains(Dname));
			assertEquals(id, valueFromDB);
			Assert.assertEquals(jsonsuitetestSuccessData.get("created_response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	public void TC002_Admin_Verify_if_user_can_get_organization_in_ATS() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_if_user_can_get_organization_in_ATS", "id");
		try {
			String testName = "TC002_Admin_Verify_if_user_can_get_organization_in_ATS";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.STAGING_APPURL1 + "organization/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			MySQLDBConnection db = new MySQLDBConnection();
			String valueFromDB = db.GetOrganisationName(createdid);
			assertEquals(domainName, valueFromDB);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC003_Admin_Verify_if_user_can_update_organization() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault("TC001_Admin_Verify_if_user_can_update_organization",
				"id");
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("description", "Test Organization");
		jsonObject.put("domain", domainName);
		jsonObject.put("email", RandomJsonGenerator.randomOrgEmail());
		jsonObject.put("firstName", "New");
		jsonObject.put("lastName", "Domain");
		jsonObject.put("favicon", "string");
		jsonObject.put("logo", "string");
		jsonObject.put("password", "Test@1234");
		jsonObject.put("organizationName", domainName);
		jsonObject.put("primaryColor", "{\"r\":0,\"g\":15,\"b\":61,\"a\":1}");
		jsonObject.put("secondaryColor", "{\"r\":38,\"g\":100,\"b\":196,\"a\":1}");
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_ORGANIZATION_DIR + "updateOrg.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC003_Admin_Verify_if_user_can_update_organization";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_ORGANIZATION_DIR + "updateOrg.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeaderwithToken();
			Response response = patchWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.STAGING_APPURL1 + "api/v1/organization/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			MySQLDBConnection db = new MySQLDBConnection();
			String valueFromDB = db.GetOrganisationName(createdid);
			assertEquals(domainName, valueFromDB);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC001_Admin_Verify_if_user_can_delete_dummy_data_of_organization_in_ATS() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_if_user_can_delete_dummy_data_of_organization_in_ATS", "id");
		try {
			String testName = "TC001_Admin_Verify_if_user_can_delete_dummy_data_of_organization_in_ATS";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.CREATE_JOB_POST_URL + "/deleteDummyData");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("deleted_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

}
