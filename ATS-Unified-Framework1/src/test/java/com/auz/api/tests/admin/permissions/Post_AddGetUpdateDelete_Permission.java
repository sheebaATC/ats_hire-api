package com.auz.api.tests.admin.permissions;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.SupportedUtils.RandomJsonGenerator;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class Post_AddGetUpdateDelete_Permission extends RestAssuredBase {

	public static HashMap<String, String> headers;
	public static String payLoadBody;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("adminSuiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	public static String createdid = "";

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Post request to create a new permission";
			testSuiteDescription = "Validating in passing the request to create a new permission";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Admin_Verify_if_user_can_create_a_new_permission() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_if_user_can_create_a_new_permission", "id");
		JSONObject jsonObject = new JSONObject();
		String name = RandomJsonGenerator.randomPermissionName();
		jsonObject.put("name", name);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_PERMISSION_DIR + "addPermission.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC001_Admin_Verify_if_user_can_create_a_new_permission";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_PERMISSION_DIR
					+ "addPermission.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/permission");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(15, 47);
			System.out.println("CreatedID: " + id);
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains(name));
			Assert.assertEquals(jsonsuitetestSuccessData.get("created_response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
			createdid = id;
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)

	public void TC002_Admin_Verify_if_user_can_get_a_permission_by_id() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault("TC002_Admin_Verify_if_user_can_get_a_permission_by_id",
				"id");

		try {
			String testName = "TC001_Admin_Verify_if_user_can_get_a_permission_by_id";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/permission/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains(createdid));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC003_Admin_Verify_if_user_can_update_permission() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault("TC003_Admin_Verify_if_user_can_update_permission",
				"id");
		JSONObject jsonObject = new JSONObject();
		String name = RandomJsonGenerator.randomPermissionName();
		jsonObject.put("name", name);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_PERMISSION_DIR + "updatePermission.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC001_Admin_Verify_if_user_can_update_permission";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_PERMISSION_DIR
					+ "updatePermission.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = patchWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/permission/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains(name));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	// @Test(priority=4)
	// This API is not used now ATS-1113
	public void TC004_Admin_Verify_if_user_can_get_all_the_permissions() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_if_user_can_get_all_the_permissions", "id");
		try {
			String testName = "TC004_Admin_Verify_if_user_can_get_all_the_permissions";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/permissions");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains("admin"));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 5)
	public void TC005_Admin_Verify_if_user_can_get_list_of_advanced_permissions() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC005_Admin_Verify_if_user_can_get_list_of_advanced_permissions", "id");

		try {
			String testName = "TC005_Admin_Verify_if_user_can_get_list_of_advanced_permissions";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/advanced-permissions");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 6)
	public void TC006_Admin_Verify_if_user_can_delete_permission_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC006_Admin_Verify_if_user_can_delete_permission_by_ID", "id");

		try {
			String testName = "TC006_Admin_Verify_if_user_can_delete_permission_by_ID";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.CREATE_JOB_POST_URL + "/permission/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("deleted_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	public String AddPermission() throws IOException {
		String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_PERMISSION_DIR + "addPermission.json";
		payLoadBody = JsonComponent.readTextFile(filePath);
		headers = generateHeader();
		Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/permission");
		System.out.println(response);
		System.out.println("Status code: " + response.statusCode());
		String jsonAsString = response.asString();
		String id = jsonAsString.substring(9, 41);
		System.out.println("ID: " + id);
		System.out.println("Response: " + jsonAsString);
		System.out.println("Body: " + response.getBody());
		return id;
	}

}
