package com.auz.api.tests.recruiter.interview;

import java.io.FileWriter;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.SupportedUtils.RandomJsonGenerator;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class AddGetUpdateDelete_ActionsForAnInterview extends RestAssuredBase {

	public static HashMap<String, String> headers;
	public static String payLoadBody;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("suiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	public static String createdid = "";

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Post request for schedule, update and delete actions in Interview";
			testSuiteDescription = "Validating in passing the request to schedule, update and delete actions in interviews";

		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Recruiter_Verify_if_user_can_add_action_for_an_interview() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Recruiter_Verify_if_user_can_add_action_for_an_interview", "id");
		JSONObject js = new JSONObject();
		JSONObject jsonObject = new JSONObject();
		JSONArray js1 = new JSONArray();
		js1.add("63e60e65e31f49d3bad457de749e9ab2");
		js.put("attachments", js1);
		js.put("body", "ngf54657yiuoijkn\ndrdf76t87y98oi[GHFGJH]");
		js.put("createdType", "SYSTEM");
		js.put("name", RandomJsonGenerator.randomTemplateName());
		js.put("subject", RandomJsonGenerator.randomTemplateSubject());
		js.put("type", "EMAIL");
		jsonObject.put("template", js);

		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_INTERVIEW_DIR + "actionForInterview.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC001_Recruiter_Verify_if_user_can_add_action_for_an_interview";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_INTERVIEW_DIR
					+ "actionForInterview.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			String interviewID = "dde12119d19d456fa748dab003bf8c28";
			Response response = putWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/interview/action/add/" + interviewID);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(285, 317);
			System.out.println("CreatedID: " + id);
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains(interviewID));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
			createdid = id;
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	public void TC002_Recruiter_Verify_if_user_can_get_action_for_an_interview() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC002_Recruiter_Verify_if_user_can_get_action_for_an_interview", "id");
		try {
			String testName = "TC002_Recruiter_Verify_if_user_can_get_action_for_an_interview";
			reportUpdate(testName);
			headers = generateHeader();
			String interviewID = "dde12119d19d456fa748dab003bf8c28";
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/interview/action/get/" + interviewID);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains(interviewID));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC003_Recruiter_Verify_if_user_can_update_action_for_an_interview() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Recruiter_Verify_if_user_can_update_action_for_an_interview", "id");
		JSONObject js = new JSONObject();
		JSONObject jsonObject = new JSONObject();
		JSONArray js1 = new JSONArray();
		js1.add("63e60e65e31f49d3bad457de749e9ab2");
		js.put("attachments", js1);
		js.put("body", "ngf54657yiuoijkn\ndrdf76t87y98oi[GHFGJH]");
		js.put("createdType", "SYSTEM");
		js.put("name", RandomJsonGenerator.randomTemplateName());
		js.put("subject", RandomJsonGenerator.randomTemplateSubject());
		js.put("type", "EMAIL");
		jsonObject.put("template", js);
		try {
			FileWriter file = new FileWriter(System.getProperty("user.dir") + Constants.JSONOBJECT_INTERVIEW_DIR
					+ "updateActionForInterview.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC003_Recruiter_Verify_if_user_can_update_action_for_an_interview";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_INTERVIEW_DIR
					+ "updateActionForInterview.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			String interviewID = "dde12119d19d456fa748dab003bf8c28";
			Response response = putWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/interview/action/update/" + interviewID);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains(interviewID));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC004_Recruiter_Verify_if_user_can_delete_action_for_an_interview() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC004_Recruiter_Verify_if_user_can_delete_action_for_an_interview", "id");
		try {
			String testName = "TC004_Recruiter_Verify_if_user_can_delete_action_for_an_interview";
			reportUpdate(testName);
			headers = generateHeader();
			String interviewID = "dde12119d19d456fa748dab003bf8c28";
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.CREATE_JOB_POST_URL + "/interview/action/delete/" + interviewID);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains(interviewID));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

}
