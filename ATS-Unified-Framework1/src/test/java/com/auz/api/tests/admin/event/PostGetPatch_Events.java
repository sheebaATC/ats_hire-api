package com.auz.api.tests.admin.event;

import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.SupportedUtils.Neo4jDBConnection;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class PostGetPatch_Events extends RestAssuredBase {

	public static HashMap<String, String> headers;
	public static String payLoadBody;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("adminSuiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	public static String createdid = "";
	public static String description = "";
	public static String newdescription = "";
	Neo4jDBConnection db = new Neo4jDBConnection();

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Requests for Event API's";
			testSuiteDescription = "Validating in passing the request to create, get, update and delete the event APIs";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Admin_Verify_if_user_can_create_a_new_event() throws Exception {
		Object testrailObj = jsonsuitetestrailData.getOrDefault("TC001_Admin_Verify_if_user_can_create_a_new_event",
				"id");
		try {
			description = "This event is to show case FY 2018 achievements and failure";
			String testName = "TC001_Admin_Verify_if_user_can_create_a_new_event";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_EVENT_DIR + "AddEvent.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/event");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(549, 581);
			System.out.println("CreatedID: " + id);
			createdid = id;
			String DBquery = "MATCH (event {uuid :" + "'" + id + "'" + "}) RETURN event, event.description";
			String getquery = "event.description";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals(description, databaseResult);
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonAsString.contains((String) jsonsuitetestSuccessData.get("superAdminID")), true);
			Assert.assertEquals(jsonsuitetestSuccessData.get("created_response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	public void TC002_Admin_Verify_if_user_can_get_an_event_by_ID() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault("TC002_Admin_Verify_if_user_can_get_an_event_by_ID",
				"id");
		try {
			String testName = "TC002_Admin_Verify_if_user_can_get_an_event_by_ID";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/event/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			String DBquery = "MATCH (event {uuid :" + "'" + createdid + "'" + "}) RETURN event, event.description";
			String getquery = "event.description";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals(description, databaseResult);
			Assert.assertEquals(true, jsonAsString.contains((String) jsonsuitetestSuccessData.get("superAdminID")));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC003_Admin_Verify_if_user_can_update_an_event_by_id() {
		newdescription = "Updated description for the event";
		Object testrailObj = jsonsuitetestrailData.getOrDefault("TC003_Admin_Verify_if_user_can_update_an_event_by_id",
				"id");
		try {
			String testName = "TC003_Admin_Verify_if_user_can_update_an_event_by_id";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_EVENT_DIR + "UpdateEvent.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = patchWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/event/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			String DBquery = "MATCH (event {uuid :" + "'" + createdid + "'" + "}) RETURN event, event.description";
			String getquery = "event.description";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals(newdescription, databaseResult);
			Assert.assertEquals(true, jsonAsString.contains((String) jsonsuitetestSuccessData.get("location")));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC004_Admin_Verify_if_user_can_get_candidate_details_involved_in_event() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC004_Admin_Verify_if_user_can_get_candidate_details_involved_in_event", "id");
		try {
			String testName = "TC004_Admin_Verify_if_user_can_get_candidate_details_involved_in_event";
			String interviewdescription = "You are invited for the interview";
			reportUpdate(testName);
			headers = generateHeader();
			String scheduleinterviewid = "edf5acea9af449188989bff6aeb49030";
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/event/candidate-details/" + scheduleinterviewid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			String DBquery = "MATCH (event {uuid :" + "'" + createdid + "'" + "}) RETURN event, event.description";
			String getquery = "event.description";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			// Assert.assertEquals(interviewdescription, databaseResult);
			Assert.assertEquals(true, jsonAsString.contains((String) jsonsuitetestSuccessData.get("superAdminID")));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 5)
	public void TC005_Admin_Verify_if_user_can_delete_the_event_by_id() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault("TC005_Admin_Verify_if_user_can_delete_the_event_by_id",
				"id");
		try {
			String testName = "TC005_Admin_Verify_if_user_can_delete_the_event_by_id";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.CREATE_JOB_POST_URL + "/event/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			String DBquery = "MATCH (event {uuid :" + "'" + createdid + "'" + "}) RETURN event, event.status";
			String getquery = "event.status";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals("Cancelled", databaseResult);
			Assert.assertEquals(false, jsonAsString.contains(createdid));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("deleted_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

}
