package com.auz.api.tests.admin.gotomeeting;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.json.simple.JSONObject;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class PostPutGetDelete_GoToMeetingConference extends RestAssuredBase {
	public static HashMap<String, String> headers;
	public static String payLoadBody;

	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("adminSuiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	public static String createdid = "";

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Request from start of creating , updating ,getting and deleting the Go To meeting";
			testSuiteDescription = "Validating in creating , updating ,getting and deleting the Go To meeting";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Admin_Verify_user_is_able_to_login_into_gotomeeting() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_user_is_able_to_login_into_gotomeeting", "id");
		try {
			String testName = "TC001_Admin_Verify_user_is_able_to_login_into_gotomeeting";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_GOTOMEETING_DIR
					+ "logingotomeeting.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeaderforMeetings();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.INTEGRATION_STAGING_URL + "/integrations/gotomeeting/login");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(40, 126);
			System.out.println("created id: " + id);
			String id1 = jsonAsString.substring(90, 126);
			System.out.println("Meeting id : " + id1);
			String rcode = String.valueOf(responsecode);
			createdid = id1;
			ChromeDriver driver = new ChromeDriver();
			driver.get(id);
			driver.findElementById("emailAddress").sendKeys("abhinav.dharmapuri@american-technology.net");
			driver.findElementById("next-button").click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElementById("password").sendKeys("@bh1Madhu");
			driver.findElementById("submit").click();
			driver.close();
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	public void TC002_Admin_Verify_user_is_able_to_enable_gotomeeting_integration() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC002_Admin_Verify_user_is_able_to_enable_gotomeeting_integration", "id");
		JSONObject js = new JSONObject();
		String Meetingid = createdid;
		js.put("id", Meetingid);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_GOTOMEETING_DIR + "enablegotomeeting.json");
			file.write(js.toJSONString());
			file.close();
			String testName = "TC002_Admin_Verify_user_is_able_to_enable_gotomeeting_integration";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_GOTOMEETING_DIR
					+ "enablegotomeeting.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeaderforMeetings();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.INTEGRATION_STAGING_URL + "/integrations/gotomeeting/enable");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(7, 22);
			String rcode = String.valueOf(responsecode);
			System.out.println("created id: " + id);
			Assert.assertEquals(true, jsonAsString.contains(id));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC003_Admin_Verify_if_you_user_is_able_to_get_gotomeeting_is_enabled() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Admin_Verify_if_you_user_is_able_to_get_gotomeeting_is_enabled", "id");
		try {
			String testName = "TC003_Admin_Verify_if_you_user_is_able_to_get_gotomeeting_is_enabled";
			reportUpdate(testName);
			headers = generateHeaderforMeetings();
			Response response = getWithHeader(headers,
					Constants.INTEGRATION_STAGING_URL + "/integrations/gotomeeting/is-enabled");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 10)
	public void TC004_Admin_Verify_user_is_able_to_disable_gotomeeting_integration() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC004_Admin_Verify_user_is_able_to_disable_gotomeeting_integration", "id");
		try {
			String testName = "TC004_Admin_Verify_user_is_able_to_disable_gotomeeting_integration";
			reportUpdate(testName);
			headers = generateHeaderforMeetings();
			Response response = postWithHeaderParam(headers,
					Constants.INTEGRATION_STAGING_URL + "/integrations/gotomeeting/disable");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(7, 23);
			String rcode = String.valueOf(responsecode);
			System.out.println("created id: " + id);
			Assert.assertEquals(true, jsonAsString.contains(id));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC005_Admin_Verify_user_is_able_to_post_gotomeeting() throws IOException {
		Object testrailObj = jsonsuitetestrailData.getOrDefault("TC005_Admin_Verify_user_is_able_to_post_gotomeeting",
				"id");
		try {
			String testName = "TC005_Admin_Verify_user_is_able_to_post_gotomeeting";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_GOTOMEETING_DIR
					+ "postMeeting.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeaderforMeetings();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.INTEGRATION_STAGING_URL + "/integrations/gotomeeting/meetings");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(96, 105);
			createdid = id;
			String rcode = String.valueOf(responsecode);
			System.out.println("created id: " + id);
			Assert.assertEquals(true, jsonAsString.contains(id));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 5)
	public void TC006_Admin_Verify_if_you_user_is_able_to_get_gotoMeeting_meetings() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC006_Admin_Verify_if_you_user_is_able_to_get_gotoMeeting_meetings", "id");
		try {
			String testName = "TC006_Admin_Verify_if_you_user_is_able_to_get_gotoMeeting_meetings";
			reportUpdate(testName);
			headers = generateHeaderforMeetings();
			Response response = getWithHeader(headers,
					Constants.INTEGRATION_STAGING_URL + "/integrations/gotomeeting/meetings");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 7)
	public void TC007_Admin_Verify_if_you_user_is_able_to_get_gotoMeeting_meeting_by_id() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC007_Admin_Verify_if_you_user_is_able_to_get_gotoMeeting_meeting_by_id", "id");
		try {
			String testName = "TC007_Admin_Verify_if_you_user_is_able_to_get_gotoMeeting_meeting_by_id";
			reportUpdate(testName);
			headers = generateHeaderforMeetings();
			Response response = getWithHeader(headers,
					Constants.INTEGRATION_STAGING_URL + "/integrations/gotomeeting/meetings/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 6)
	public void TC008_Admin_Verify_if_you_user_is_able_to_update_gotomeeting__meeting_by_id() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC008_Admin_Verify_if_you_user_is_able_to_update_gotomeeting__meeting_by_id", "id");
		try {
			String testName = "TC008_Admin_Verify_if_you_user_is_able_to_update_gotomeeting__meeting_by_id";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_GOTOMEETING_DIR
					+ "updateMeeting.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeaderforMeetings();
			Response response = putWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.INTEGRATION_STAGING_URL + "/integrations/gotomeeting/meetings/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 8)
	public void TC009_Admin_Verify_user_is_able_to_end_gotomeeting_meetings_by_id() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC009_Admin_Verify_user_is_able_to_end_gotomeeting_meetings_by_id", "id");
		try {
			String testName = "TC009_Admin_Verify_user_is_able_to_end_gotomeeting_meetings_by_id";
			reportUpdate(testName);
			headers = generateHeaderforMeetings();
			Response response = putWithHeader(headers,
					Constants.INTEGRATION_STAGING_URL + "/integrations/gotomeeting/meetings/" + createdid + "/end");
			// Response response = postWithHeaderParam(headers,
			// Constants.INTEGRATION_STAGING_URL +"/integrations/gotomeeting/meetings/"
			// +createdid + "/end");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
//			String id = jsonAsString.substring(33, 65);
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 9)
	public void TC0010_Admin_Verify_if_you_user_is_able_to_delete_gotomeeting_meetings_by_id() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0010_Admin_Verify_if_you_user_is_able_to_delete_gotomeeting_meetings_by_id", "id");
		try {
			String testName = "TC0010_Admin_Verify_if_you_user_is_able_to_delete_gotomeeting_meetings_by_id";
			reportUpdate(testName);
			headers = generateHeaderforMeetings();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.INTEGRATION_STAGING_URL + "/integrations/gotomeeting/meetings/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

}