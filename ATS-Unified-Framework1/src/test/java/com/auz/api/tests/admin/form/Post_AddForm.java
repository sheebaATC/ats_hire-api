package com.auz.api.tests.admin.form;

import java.io.FileWriter;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class Post_AddForm extends RestAssuredBase {

	public static HashMap<String, String> headers;
	public static String payLoadBody;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("adminSuiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	public static String createdid = "";
	public static String locid = "";
	public static String EntityId = "ca1036b53b73411b966a78bf7b2be34b";
	public static String createdentityid = "";
	public static String createdat = "";

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Post request to add/get/update/submit/delete forms";
			testSuiteDescription = "Validating in passing the request to add/get/update/submit/delete forms";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Admin_Verify_if_user_can_add_a_new_form_to_system() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_if_user_can_add_a_new_form_to_system", "id");

		try {
			String testName = "TC001_Admin_Verify_if_user_can_add_a_new_form_to_system";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_FORM_DIR + "addForm.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/form");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(15, 47);
			String LocID = jsonAsString.substring(229, 261);
			String CreatedAt = jsonAsString.substring(290, 317);
			String CreatedEntityID = jsonAsString.substring(756, 788);
			System.out.println("CreatedID: " + id);
			System.out.println("CreatedID2: " + LocID);
			System.out.println("CreatedID3: " + CreatedAt);
			System.out.println("CreatedID4: " + CreatedEntityID);
			createdid = id;
			locid = LocID;
			createdat = CreatedAt;
			createdentityid = CreatedEntityID;
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains((String) jsonsuitetestSuccessData.get("category")));
			Assert.assertEquals(jsonsuitetestSuccessData.get("created_response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	public void TC002_Admin_Verify_if_you_user_is_able_to_get_form_from_system_OPENAPI() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC002_Admin_Verify_if_you_user_is_able_to_get_form_from_system_OPENAPI", "id");
		try {
			String testName = "TC002_Admin_Verify_if_you_user_is_able_to_get_form_from_system_OPENAPI";
			reportUpdate(testName);
			headers = generateHeader();
			String orgDomain = "vinoth";
			Response response = getRequestWithQueryParams(headers, "category", "JobApplicationForm",
					Constants.CREATE_JOB_POST_URL + "/form/" + orgDomain + "/" + EntityId);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains(EntityId));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC003_Admin_Verify_if_you_user_is_able_to_get_formById_from_system() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Admin_Verify_if_you_user_is_able_to_get_formById_from_system", "id");
		try {
			String testName = "TC003_Admin_Verify_if_you_user_is_able_to_get_formById_from_system";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/formById/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains(createdid));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC004_Admin_Verify_if_you_user_is_able_to_get_formById_from_system_OPENAPI() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC004_Admin_Verify_if_you_user_is_able_to_get_formById_from_system_OPENAPI", "id");
		try {
			String testName = "TC004_Admin_Verify_if_you_user_is_able_to_get_formById_from_system_OPENAPI";
			reportUpdate(testName);
			headers = generateHeader();
			String orgDomain = "vinoth";
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/formById/" + orgDomain + "/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains(createdid));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 5)
	public void TC005_Admin_Verify_if_you_are_able_to_publish_form_to_system() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC005_Admin_Verify_if_you_are_able_to_publish_form_to_system", "id");
		try {
			String testName = "TC005_Admin_Verify_if_you_are_able_to_publish_form_to_system";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_FORM_DIR + "publishForm.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = putWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/form/" + createdid + "/publish");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains(createdid));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 6)
	public void TC006_Admin_Verify_if_you_user_is_able_to_get_form_from_system() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC006_Admin_Verify_if_you_user_is_able_to_get_form_from_system_OPENAPI", "id");
		try {
			String testName = "TC006_Admin_Verify_if_you_user_is_able_to_get_form_from_system_OPENAPI";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getRequestWithQueryParams(headers, "category", "JobApplicationForm",
					Constants.CREATE_JOB_POST_URL + "/form/" + EntityId);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains(EntityId));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 7)
	public void TC007_Admin_Verify_if_you_are_able_to_update_form_to_system() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC007_Admin_Verify_if_you_are_able_to_update_form_to_system", "id");
		JSONObject js = new JSONObject();
		js.put("category", "JobApplicationForm");
		js.put("createdEntityId", createdentityid);
		js.put("submitterType", "Candidate");
		JSONArray role = new JSONArray();
		JSONObject js1 = new JSONObject();
		js1.put("uuid", locid);
		js1.put("createdAt", createdat);
		js1.put("label", "currentLocation");
		js1.put("name", "currentLocation");
		js1.put("value", "vb");
		js1.put("type", "String");
		js1.put("isRequired", false);
		js1.put("position", 8);
		js1.put("autoGenerated", true);
		js1.put("singleOption", true);
		js1.put("isEnabled", true);
		js1.put("fieldLevel", "CUSTOM_FIELD");
		role.add(js1);
		js.put("customFields", role);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_FORM_DIR + "updateForm.json");
			file.write(js.toJSONString());
			file.close();
			String testName = "TC007_Admin_Verify_if_you_are_able_to_update_form_to_system";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_FORM_DIR + "updateForm.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = putWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/form/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains(createdentityid));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 8)
	public void TC008_Admin_Verify_if_you_user_is_able_to_get_formById_from_system() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC008_Admin_Verify_if_you_user_is_able_to_get_formById_from_system", "id");
		try {
			String testName = "TC008_Admin_Verify_if_you_user_is_able_to_get_formById_from_system";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/form-status/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains((String) jsonsuitetestSuccessData.get("status")));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 9)
	public void TC009_Admin_Verify_if_you_are_able_to_submit_form_to_system() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC009_Admin_Verify_if_you_are_able_to_submit_form_to_system", "id");
		String place = "NewYork";
		JSONObject js = new JSONObject();
		js.put(locid, place);
		JSONObject js1 = new JSONObject();
		js1.put("fields", js);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_FORM_DIR + "submitForm.json");
			file.write(js1.toJSONString());
			file.close();
			String testName = "TC009_Admin_Verify_if_you_are_able_to_submit_form_to_system";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_FORM_DIR + "submitForm.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			String orgDomain = "vinoth";
			Response response = putWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/form/submit/" + orgDomain + "/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains("ca1036b53b73411b966a78bf7b2be34b"));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 10)
	public void TC0010_Admin_Verify_if_you_are_able_to_delete_form_to_system() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0010_Admin_Verify_if_you_are_able_to_delete_form_to_system", "id");
		try {
			String testName = "TC0010_Admin_Verify_if_you_are_able_to_delete_form_to_system";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.CREATE_JOB_POST_URL + "/form/" + EntityId);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("deleted_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}
}
