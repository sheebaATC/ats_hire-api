package com.auz.api.tests.admin.workflowInterview;

import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.SupportedUtils.Neo4jDBConnection;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class InterviewWorkflow extends RestAssuredBase {
	public static HashMap<String, String> headers;
	public static String payLoadBody;

	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("adminSuiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	Neo4jDBConnection db = new Neo4jDBConnection();
	public static String createdid = "";
	public static String interviewid = "592ad5048aae45cd87a4cb2e9cf98388";

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Post Request for Interview Workflow";
			testSuiteDescription = "Validating the request for Interview Workflow";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Admin_Verify_if_the_user_is_able_to_post_workflow_to_an_interview() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_if_the_user_is_able_to_post_workflow_to_an_interview", "id");
		try {
			String testName = "TC001_Admin_Verify_if_the_user_is_able_to_post_workflow_to_an_interview";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_WORKFLOWS_DIR
					+ "PostWorkflowInterview.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/interview/" + interviewid + "/workflow");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(237, 269);
			System.out.println("CREATEDID: " + id);
			createdid = id;
			String rcode = String.valueOf(responsecode);
			String DBquery = "MATCH (workflow {uuid :" + "'" + id + "'" + "}) RETURN workflow, workflow.name";
			String getquery = "workflow.name";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals("TestRecipe", databaseResult);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	public void TC002_Admin_Verify_if_the_user_is_able_to_get_workflow_of_an_interview() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC002_Admin_Verify_if_the_user_is_able_to_get_workflow_of_an_interview", "id");
		try {
			String testName = "TC002_Admin_Verify_if_the_user_is_able_to_get_workflow_of_an_interview";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/interview/" + interviewid + "/workflows");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains((createdid)));
			String DBquery = "MATCH (workflow {uuid :" + "'" + createdid + "'" + "}) RETURN workflow, workflow.name";
			String getquery = "workflow.name";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals("TestRecipe", databaseResult);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC003_Admin_Verify_if_the_user_is_able_to_update_a_interview_workflow_of_an_interview() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC003_Admin_Verify_if_the_user_is_able_to_update_a_workflow_template_of_an_interview", "id");
		try {
			String testName = "TC003_Admin_Verify_if_the_user_is_able_to_update_a_interview_workflow_of_an_interview";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_WORKFLOWS_DIR
					+ "UpdateWorkflowInterview.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = putWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/interview/" + interviewid + "/workflow/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String DBquery = "MATCH (workflow {uuid :" + "'" + createdid + "'" + "}) RETURN workflow, workflow.name";
			String getquery = "workflow.name";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals("UpdateTestRecipe", databaseResult);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC004_Admin_Verify_if_the_user_is_able_to_delete_a_workflow_of_an_interview() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC004_Admin_Verify_if_the_user_is_able_to_delete_a_workflow_of_an_interview", "id");
		try {
			String testName = "TC004_Admin_Verify_if_the_user_is_able_to_update_a_workflow_template_by_ID";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.CREATE_JOB_POST_URL + "/interview/" + interviewid + "/workflow/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			String DBquery = "MATCH (_workflow {uuid :" + "'" + createdid + "'" + "}) RETURN _workflow, _workflow.name";
			String getquery = "_workflow.name";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals("UpdateTestRecipe", databaseResult);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 5)
	public void TC005_Admin_Verify_if_the_user_is_unable_to_get_workflow_of_an_interview_after_deletion() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC005_Admin_Verify_if_the_user_is_able_to_get_workflow_of_an_interview_after_deletion", "id");
		try {
			String testName = "TC005_Admin_Verify_if_the_user_is_able_to_get_workflow_of_an_interview_after_deletion";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/interview/" + interviewid + "/workflows");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(false, jsonAsString.contains((createdid)));
			String DBquery = "MATCH (_workflow {uuid :" + "'" + createdid + "'" + "}) RETURN _workflow, _workflow.name";
			String getquery = "_workflow.name";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals("UpdateTestRecipe", databaseResult);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

}
