package com.auz.api.tests.admin.mail;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.SupportedUtils.RandomJsonGenerator;
import com.auz.restassured.tests.candidates.testcases.Post_AddNewCandidate;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class Post_SendAnEmail extends RestAssuredBase {
	public static HashMap<String, String> headers;
	public static String payLoadBody;

	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("adminSuiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	// static String CandidateID = "b58fa4e244be4c46a975747c8d3a4638";

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Post Request for send an email ";
			testSuiteDescription = "Validating the request to send an email";
			// candidate();
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })

	public static void addMailJSONFile() throws Exception {
		Post_AddNewCandidate objAddCandidate = new Post_AddNewCandidate();
		JSONObject js = new JSONObject();
		js.put("organizationId", "b665ccd67abe4e8dba9d7ffcae488093");

		JSONArray toEmail1 = new JSONArray();
		JSONObject toEmailID = new JSONObject();
		toEmailID.put("email", "johndoe@email.com");
		toEmailID.put("name", RandomJsonGenerator.randomFirstName());
		toEmailID.put("entity", "candidate");
		toEmailID.put("entityId", "b58fa4e244be4c46a975747c8d3a4638");
		toEmail1.add(toEmailID);
		js.put("toEmail", toEmail1);

		JSONArray ccEmail = new JSONArray();
		JSONObject ccEmailId = new JSONObject();
		ccEmailId.put("email", "johndoe1@email.com");
		ccEmailId.put("name", RandomJsonGenerator.randomFirstName());
		ccEmailId.put("entity", "candidate");
		ccEmailId.put("entityId", "f05fdc177b314938aefd9af914b18d2e");
		ccEmail.add(ccEmailId);
		js.put("ccEmail", ccEmail);

		JSONArray bccEmail = new JSONArray();
		JSONObject bccEmailID = new JSONObject();
		bccEmailID.put("email", "johndoe2@email.com");
		bccEmailID.put("name", RandomJsonGenerator.randomFirstName());
		bccEmailID.put("entity", "candidate");
		bccEmailID.put("entityId", "8a0e4a935377426a89b08e52e847de81");
		bccEmail.add(bccEmailID);
		js.put("bccEmail", bccEmail);

		JSONArray replyToEmail = new JSONArray();
		JSONObject replyToEmailID = new JSONObject();
		replyToEmailID.put("email", "johndoe3@email.com");
		replyToEmailID.put("name", RandomJsonGenerator.randomFirstName());
		replyToEmailID.put("entity", "candidate");
		replyToEmailID.put("entityId", "5642bcf827414e96829bbb893ca1de4b");
		replyToEmail.add(replyToEmailID);
		js.put("replyToEmail", replyToEmail);

		js.put("replyToMessageId", "Good morning! Thanks for the email!");

		JSONObject fromEmailID = new JSONObject();
		fromEmailID.put("email", "sheeba@american-technology.net");
		fromEmailID.put("name", "Sheeba");
		fromEmailID.put("entity", "admin");
		fromEmailID.put("entityId", "191edfb1c54f4d1c985710ae33ba6699");
		js.put("fromEmail", fromEmailID);
		js.put("subject", "Thanks email");
		js.put("body", "Thank you for sending the email");
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_MAIL_DIR + "sendMail.json");
			file.write(js.toJSONString());
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	@Test()
	public void TC001_Admin_Verify_user_is_able_to_send_mails_for_candidate() {

		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_user_is_able_to_send_mails_for_candidate", "id");
		try {
			addMailJSONFile();
			String testName = "TC001_Admin_Verify_user_is_able_to_send_mails_for_candidate";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_MAIL_DIR + "sendMail.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/mail");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			// updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			// updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	public String SyncMail() throws IOException {
		String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_TAG_DIR + "CreateTag.json";
		payLoadBody = JsonComponent.readTextFile(filePath);
		headers = generateHeader();
		Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.COMM_APPURL + "SyncMail.json");
		System.out.println(response);
		System.out.println("Status code: " + response.statusCode());
		String jsonAsString = response.asString();
		String id = jsonAsString.substring(9, 41);
		System.out.println("ID: " + id);
		System.out.println("Response: " + jsonAsString);
		System.out.println("Body: " + response.getBody());
		return id;

	}

}
