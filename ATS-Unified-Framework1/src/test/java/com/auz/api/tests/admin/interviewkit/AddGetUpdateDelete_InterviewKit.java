package com.auz.api.tests.admin.interviewkit;

import java.io.FileWriter;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.SupportedUtils.Neo4jDBConnection;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class AddGetUpdateDelete_InterviewKit extends RestAssuredBase {

	public static HashMap<String, String> headers;
	public static String payLoadBody;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("adminSuiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	Neo4jDBConnection db = new Neo4jDBConnection();
	public static String createdid = "";
	public static String description = "";

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Post request for schedule, update and delete interview kit in interview";
			testSuiteDescription = "Validating in passing the request to schedule, update and delete interview kit in interview";

		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Admin_Verify_if_user_can_add_interview_kit_to_interview() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_if_user_can_add_interview_kit_to_interview", "id");
		JSONObject jsonObject = new JSONObject();
		description = "New Interview kit";
		jsonObject.put("description", description);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_INTERVIEWKIT_DIR + "addInterviewKit.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC001_Admin_Verify_if_user_can_add_interview_kit_to_interview";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_INTERVIEWKIT_DIR
					+ "addInterviewKit.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			String interviewID = "dde12119d19d456fa748dab003bf8c28";
			// String interviewID = "ae90b2614c8b404181046507ee3efded";
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/interview/add-kit/" + interviewID);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(253, 285);
			System.out.println("CreatedID: " + id);
			createdid = id;
			String rcode = String.valueOf(responsecode);
			String DBquery = "MATCH (interviewkit {uuid :" + "'" + id + "'"
					+ "}) RETURN interviewkit, interviewkit.description";
			String getquery = "interviewkit.description";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals(description, databaseResult);
			Assert.assertEquals(true, jsonAsString.contains(interviewID));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	public void TC002_Admin_Verify_if_user_can_get_interview_kit_for_interview() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC002_Admin_Verify_if_user_can_get_interview_kit_for_interview", "id");
		try {
			String testName = "TC002_Admin_Verify_if_user_can_get_interview_kit_for_interview";
			reportUpdate(testName);
			headers = generateHeader();
			String interviewID = "dde12119d19d456fa748dab003bf8c28";
			// String interviewID = "ae90b2614c8b404181046507ee3efded";
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/interview/get-kit/" + interviewID);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			String DBquery = "MATCH (interviewkit {uuid :" + "'" + createdid + "'"
					+ "}) RETURN interviewkit, interviewkit.description";
			String getquery = "interviewkit.description";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals(description, databaseResult);
			Assert.assertEquals(true, jsonAsString.contains(interviewID));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC003_Admin_Verify_if_user_can_update_interview_kit_for_interview() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Admin_Verify_if_user_can_update_interview_kit_for_interview", "id");
		JSONObject jsonObject = new JSONObject();
		description = "Updated Interview kit";
		jsonObject.put("description", description);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_INTERVIEWKIT_DIR + "updateInterviewKit.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC003_Admin_Verify_if_user_can_update_interview_kit_for_interview";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_INTERVIEWKIT_DIR
					+ "updateInterviewKit.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			String interviewID = "dde12119d19d456fa748dab003bf8c28";
			// String interviewID = "ae90b2614c8b404181046507ee3efded";
			Response response = putWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/interview/update-kit/" + interviewID);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			String DBquery = "MATCH (interviewkit {uuid :" + "'" + createdid + "'"
					+ "}) RETURN interviewkit, interviewkit.description";
			String getquery = "interviewkit.description";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals(description, databaseResult);
			Assert.assertEquals(true, jsonAsString.contains(interviewID));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC004_Admin_Verify_if_user_can_remove_interview_kit_for_interview() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC004_Admin_Verify_if_user_can_remove_interview_kit_for_interview", "id");
		try {
			String testName = "TC004_Admin_Verify_if_user_can_remove_interview_kit_for_interview";
			reportUpdate(testName);
			headers = generateHeader();
			String id = createdid;
			String interviewID = "dde12119d19d456fa748dab003bf8c28";
			// String interviewID = "ae90b2614c8b404181046507ee3efded";
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.CREATE_JOB_POST_URL + "/interview/delete-kit/" + interviewID);
			Thread.sleep(3000);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			String DBquery = "MATCH (interviewkit {uuid :" + "'" + createdid + "'"
					+ "}) RETURN interviewkit, interviewkit.description";
			String getquery = "interviewkit.description";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			System.out.println("S  :" + databaseResult);
			// Assert.assertEquals("", databaseResult);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

}
