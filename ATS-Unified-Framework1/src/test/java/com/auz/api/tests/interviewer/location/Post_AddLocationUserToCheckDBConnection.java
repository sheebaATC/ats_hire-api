package com.auz.api.tests.interviewer.location;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.SupportedUtils.RandomJsonGenerator;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class Post_AddLocationUserToCheckDBConnection extends RestAssuredBase {
	public static HashMap<String, String> headers;
	public static String payLoadBody;

	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("suiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Post Request for getting all locations for interviewer role ";
			testSuiteDescription = "Post Request for getting all locations for interviewer role";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test
	public void TC001_Interviewer_Verify_if_you_are_able_to_get_all_locations_of_organization() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Interviewer_Verify_if_you_are_able_to_get_all_locations_of_organization", "id");
		try {
			String testName = "TC001_Interviewer_Verify_if_you_are_able_to_get_all_locations_of_organization";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/locations");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	public String AddLocation() throws IOException {
		JSONObject jsonObject = new JSONObject();
		String name = RandomJsonGenerator.randomAddressLine1();
		jsonObject.put("isHq", "true");
		jsonObject.put("line1", name);
		jsonObject.put("line2", RandomJsonGenerator.randomAddressLine2());
		jsonObject.put("name", RandomJsonGenerator.randomStateName());
		jsonObject.put("zipCode", RandomJsonGenerator.randomZipCode());
		jsonObject.put("cityId", "07ce8ca540294b7786b94dc1e4cae2d6");
		FileWriter file = new FileWriter(
				System.getProperty("user.dir") + Constants.JSONOBJECT_LOCATION_DIR + "AddLocation.json");
		file.write(jsonObject.toJSONString());
		file.close();
		String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_LOCATION_DIR + "AddLocation.json";
		payLoadBody = JsonComponent.readTextFile(filePath);
		headers = generateHeader();
		Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/location");
		System.out.println(response);
		System.out.println("Status code: " + response.statusCode());
		String jsonAsString = response.asString();
		String id = jsonAsString.substring(9, 41);
		System.out.println("ID: " + id);
		System.out.println("Response: " + jsonAsString);
		System.out.println("Body: " + response.getBody());
		return id;
	}

}
