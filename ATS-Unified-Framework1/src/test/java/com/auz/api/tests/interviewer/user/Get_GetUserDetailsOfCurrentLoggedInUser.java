package com.auz.api.tests.interviewer.user;

import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class Get_GetUserDetailsOfCurrentLoggedInUser extends RestAssuredBase {

	public static HashMap<String, String> headers;
	public static String payLoadBody;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("suiteLevelData", strtesttestraildatafilename);

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Get Request to get the details of the current logged in user";
			testSuiteDescription = "Validating in passing the request to get the details of the current logged in user";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test()
	public void TC001_Interviewer_Verify_if_user_is_able_to_get_details_of_current_logged_in_user() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC001_Interviewer_Verify_if_user_is_able_to_get_details_of_current_logged_in_user", "id");
		try {
			String testName = "TC001_Interviewer_Verify_if_user_is_able_to_get_details_of_current_loggin_in_user";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/user");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(jsonAsString.contains((String) jsonsuitetestSuccessData.get("recruiterID")), true);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

}
