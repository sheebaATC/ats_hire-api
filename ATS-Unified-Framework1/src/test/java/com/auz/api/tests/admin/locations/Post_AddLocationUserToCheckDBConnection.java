package com.auz.api.tests.admin.locations;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.SupportedUtils.RandomJsonGenerator;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class Post_AddLocationUserToCheckDBConnection extends RestAssuredBase {
	public static HashMap<String, String> headers;
	public static String payLoadBody;

	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("adminSuiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	public static String createdid = "";

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Post Request for adding a new location ";
			testSuiteDescription = "Validating the request to add new location";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Admin_Verify_user_is_able_to_create_new_location_user_to_check_db_connection() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC001_Admin_Verify_user_is_able_to_create_new_location_user_to_check_db_connection", "id");
		JSONObject jsonObject = new JSONObject();
		String name = RandomJsonGenerator.randomAddressLine1();
		jsonObject.put("isHq", "true");
		jsonObject.put("line1", name);
		jsonObject.put("line2", RandomJsonGenerator.randomAddressLine2());
		jsonObject.put("name", RandomJsonGenerator.randomStateName());
		jsonObject.put("zipCode", RandomJsonGenerator.randomZipCode());
		jsonObject.put("cityId", "07ce8ca540294b7786b94dc1e4cae2d6");
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_LOCATION_DIR + "AddLocation.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC001_Admin_Verify_user_is_able_to_create_new_location_user_to_check_db_connection";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_LOCATION_DIR + "AddLocation.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.HIRE_QA_URL + "/location");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(15, 47);
			System.out.println("CREATEDID: " + id);
			createdid = id;
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains((name)));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	public void TC002_Admin_Verify_if_you_user_is_able_to_find_location_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_if_you_user_is_able_to_find_location_by_ID", "id");
		Post_AddLocationUserToCheckDBConnection objAddLocation = new Post_AddLocationUserToCheckDBConnection();
		try {
			String testName = "TC002_Admin_Verify_if_you_user_is_able_to_find_location_by_ID";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/location/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains((createdid)));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC003_Admin_Verify_if_you_are_able_to_update_a_location_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_if_you_are_able_to_update_a_location_by_ID", "id");
		JSONObject jsonObject = new JSONObject();
		String address1 = RandomJsonGenerator.randomAddressLine1();
		jsonObject.put("isHq", "true");
		jsonObject.put("line1", address1);
		jsonObject.put("line2", RandomJsonGenerator.randomAddressLine2());
		jsonObject.put("name", RandomJsonGenerator.randomStateName());
		jsonObject.put("zipCode", RandomJsonGenerator.randomZipCode());
		jsonObject.put("cityId", "07ce8ca540294b7786b94dc1e4cae2d6");
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_LOCATION_DIR + "UpdateLocation.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC003_Admin_Verify_if_you_are_able_to_update_a_location_by_ID";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_LOCATION_DIR
					+ "UpdateLocation.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = patchWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/location/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains((address1)));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC004_Admin_Verify_if_you_are_able_to_get_all_locations_of_organization() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_if_you_are_able_to_get_all_locations_of_organization", "id");
		try {
			String testName = "TC004_Admin_Verify_if_you_are_able_to_get_all_locations_of_organization";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/locations");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains((createdid)));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 5)
	public void TC005_Admin_Verify_if_you_are_able_to_delete_a_location_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_if_you_are_able_to_delete_a_location_by_ID", "id");
		try {
			String testName = "TC005_Admin_Verify_if_you_are_able_to_delete_a_location_by_ID";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.CREATE_JOB_POST_URL + "/location/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("deleted_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	public String AddLocation() throws IOException {
		JSONObject jsonObject = new JSONObject();
		String name = RandomJsonGenerator.randomAddressLine1();
		jsonObject.put("isHq", "true");
		jsonObject.put("line1", name);
		jsonObject.put("line2", RandomJsonGenerator.randomAddressLine2());
		jsonObject.put("name", RandomJsonGenerator.randomStateName());
		jsonObject.put("zipCode", RandomJsonGenerator.randomZipCode());
		jsonObject.put("cityId", "07ce8ca540294b7786b94dc1e4cae2d6");
		FileWriter file = new FileWriter(
				System.getProperty("user.dir") + Constants.JSONOBJECT_LOCATION_DIR + "AddLocation.json");
		file.write(jsonObject.toJSONString());
		file.close();
		String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_LOCATION_DIR + "AddLocation.json";
		payLoadBody = JsonComponent.readTextFile(filePath);
		headers = generateHeader();
		Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/location");
		System.out.println(response);
		System.out.println("Status code: " + response.statusCode());
		String jsonAsString = response.asString();
		String id = jsonAsString.substring(9, 41);
		System.out.println("ID: " + id);
		System.out.println("Response: " + jsonAsString);
		System.out.println("Body: " + response.getBody());
		return id;
	}

}
