package com.auz.api.tests.admin.WorkflowTemplate;

import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.SupportedUtils.Neo4jDBConnection;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class WorkflowTemplate extends RestAssuredBase {
	public static HashMap<String, String> headers;
	public static String payLoadBody;

	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("adminSuiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	Neo4jDBConnection db = new Neo4jDBConnection();
	public static String createdid = "";
	public static String name = "firstRecipe";
	public static String updatedname = "UpdatefirstRecipe";

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Post Request for Workflow template ";
			testSuiteDescription = "Validating the request for Workflow template";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Admin_Verify_if_the_user_is_able_to_post_workflow_template() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_if_the_user_is_able_to_post_workflow_template", "id");
		try {
			String testName = "TC001_Admin_Verify_if_the_user_is_able_to_post_workflow_template";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_WORKFLOWS_DIR
					+ "PostWorkflowTemplate.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.HIRE_QA_URL + "/workflow-template");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(15, 47);
			System.out.println("CREATEDID: " + id);
			createdid = id;
			String rcode = String.valueOf(responsecode);
			String DBquery = "MATCH (workflow {uuid :" + "'" + id + "'" + "}) RETURN workflow, workflow.name";
			String getquery = "workflow.name";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals(name, databaseResult);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)

	public void TC002_Admin_Verify_if_the_user_is_able_to_get_workflowtemplate() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC002_Admin_Verify_if_the_user_is_able_to_get_workflowtemplate", "id");
		try {
			String testName = "TC002_Admin_Verify_if_the_user_is_able_to_get_workflowtemplate";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.HIRE_QA_URL + "/workflow-template");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains((createdid)));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC003_Admin_Verify_if_the_user_is_able_to_get_workflow_template_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Admin_Verify_if_the_user_is_able_to_get_workflow_template_by_ID", "id");
		try {
			String testName = "TC003_Admin_Verify_if_the_user_is_able_to_get_workflow_template_by_ID";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.HIRE_QA_URL + "/workflow-template/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			String DBquery = "MATCH (workflow {uuid :" + "'" + createdid + "'" + "}) RETURN workflow, workflow.name";
			String getquery = "workflow.name";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals(name, databaseResult);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			Assert.assertEquals(true, jsonAsString.contains((createdid)));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC004_Admin_Verify_if_the_user_is_able_to_update_a_workflow_template_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC004_Admin_Verify_if_the_user_is_able_to_update_a_workflow_template_by_ID", "id");
		try {
			String testName = "TC004_Admin_Verify_if_the_user_is_able_to_update_a_workflow_template_by_ID";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_WORKFLOWS_DIR
					+ "UpdateWorkflowTemplate.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = patchWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.HIRE_QA_URL + "/workflow-template/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			String DBquery = "MATCH (workflow {uuid :" + "'" + createdid + "'" + "}) RETURN workflow, workflow.name";
			String getquery = "workflow.name";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals(updatedname, databaseResult);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 5)
	public void TC005_Admin_Verify_if_the_user_is_able_to_delete_a_workflow_template_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC005_Admin_Verify_if_the_user_is_able_to_delete_a_workflow_template_by_ID", "id");
		try {
			String testName = "TC005_Admin_Verify_if_the_user_is_able_to_delete_a_workflow_template_by_ID";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.HIRE_QA_URL + "/workflow-template/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			String DBquery = "MATCH (_workflow {uuid :" + "'" + createdid + "'" + "}) RETURN _workflow, _workflow.name";
			String getquery = "_workflow.name";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals(updatedname, databaseResult);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("deleted_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 6)
	public void TC006_Admin_Verify_if_the_user_is_able_to_get_workflow_template_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC006_Admin_Verify_if_the_user_is_able_to_get_workflow_template_by_ID", "id");
		try {
			String testName = "TC006_Admin_Verify_if_the_user_is_able_to_get_workflow_template_by_ID";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.HIRE_QA_URL + "/workflow-template/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			String DBquery = "MATCH (_workflow {uuid :" + "'" + createdid + "'" + "}) RETURN _workflow, _workflow.name";
			String getquery = "_workflow.name";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals(updatedname, databaseResult);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			Assert.assertEquals(false, jsonAsString.contains((createdid)));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 7)
	public void TC007_Admin_Verify_if_the_user_is_able_to_get_workflow_logs() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC007_Admin_Verify_if_the_user_is_able_to_get_workflow_logs", "id");
		try {
			String testName = "TC007_Admin_Verify_if_the_user_is_able_to_get_workflow_logs";
			reportUpdate(testName);
			headers = generateHeader();
			String candidateid = "689c9a1df9534713862e77a521a42878";
			Response response = getRequestWithQueryParams(headers, "entityId", "689c9a1df9534713862e77a521a42878",
					Constants.HIRE_QA_URL + "/workflow-logs");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
//			String DBquery = "MATCH (_workflow {uuid :"+"'" +createdid + "'" +"}) RETURN _workflow, _workflow.name";
//			String getquery = "_workflow.name";
//			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
//			Assert.assertEquals(updatedname, databaseResult);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			Assert.assertEquals(true, jsonAsString.contains((candidateid)));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

}
