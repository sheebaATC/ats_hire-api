package com.auz.api.tests.admin.candidates;

import java.io.FileWriter;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.SupportedUtils.Neo4jDBConnection;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class Post_AddNewCandidate extends RestAssuredBase {
	public static HashMap<String, String> headers;
	public static String payLoadBody;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("adminSuiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	public static String createdid = "";
	public static String otherDetails = "City Volunteer of the Year";
	public static String email = "johndoe4@email.com";
	Neo4jDBConnection db = new Neo4jDBConnection();

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Post Request for Creating Candidate ";
			testSuiteDescription = "Validating the Create candidate";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Admin_Verify_if_user_can_add_a_candidate() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault("TC001_Admin_Verify_if_user_can_add_a_candidate", "id");

		try {
			String testName = "TC001_Admin_Verify_if_user_can_add_a_candidate";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_CANDIDATE_DIR
					+ "postCandidate.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/candidate");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(15, 47);
			System.out.println("CreatedID: " + id);
			createdid = id;
			addCandidateToJob();
			String DBquery = "MATCH (candidate {uuid :" + "'" + id + "'"
					+ "}) RETURN candidate, candidate.otherDetails";
			String getquery = "candidate.otherDetails";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals(otherDetails, databaseResult);
			String DBquery2 = "MATCH (candidate {uuid :" + "'" + id + "'" + "}) RETURN candidate, candidate.email";
			String getquery2 = "candidate.email";
			String databaseResult2 = db.databaseConnectiviy(DBquery2, getquery2);
			Assert.assertEquals(email, databaseResult2);
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains(id));
			Assert.assertEquals(jsonsuitetestSuccessData.get("created_response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	public void TC002_Admin_Verify_if_you_are_able_to_find_candidate_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC002_Admin_Verify_if_you_are_able_to_find_candidate_by_ID", "id");
		try {
			String testName = "TC002_Admin_Verify_if_you_are_able_to_find_candidate_by_ID";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_CANDIDATE_DIR
					+ "postCandidate.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/candidate/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			String DBquery = "MATCH (candidate {uuid :" + "'" + createdid + "'"
					+ "}) RETURN candidate, candidate.otherDetails";
			String getquery = "candidate.otherDetails";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals(otherDetails, databaseResult);
			Assert.assertEquals(true, jsonAsString.contains(createdid));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC003_Admin_Verify_if_you_are_able_to_update_a_candidate_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Admin_Verify_if_you_are_able_to_update_candidate_by_ID", "id");
		String email = "johndoeeee@email.com";
		try {
			String testName = "TC003_Admin_Verify_if_you_are_able_to_patch_candidate_by_ID";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_CANDIDATE_DIR
					+ "updateCandidate.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = patchWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/candidate/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			String DBquery2 = "MATCH (candidate {uuid :" + "'" + createdid + "'"
					+ "}) RETURN candidate, candidate.email";
			String getquery2 = "candidate.email";
			String databaseResult2 = db.databaseConnectiviy(DBquery2, getquery2);
			Assert.assertEquals(email, databaseResult2);
			Assert.assertEquals(true, jsonAsString.contains(createdid));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC004_Admin_Verify_if_you_are_able_to_get_all_the_scheduled_interview_for_the_candidate() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC004_Admin_Verify_if_you_are_able_to_get_all_the_scheduled_interview_for_the_candidate", "id");
		try {
			String testName = "TC004_Admin_Verify_if_you_are_able_to_get_all_the_scheduled_interview_for_the_candidate";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/candidate/" + createdid + "/scheduledInterviews");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains(createdid));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 5)
	public void TC005_Admin_Verify_if_you_are_able_to_get_all_mails_for_candidate() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC005_Admin_Verify_if_you_are_able_to_get_all_mails_for_candidate", "id");
		try {
			String testName = "TC005_Admin_Verify_if_you_are_able_to_get_all_mails_for_candidate";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/candidate/" + createdid + "/mails");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			// Assert.assertEquals(true, jsonAsString.contains(createdid));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 6)
	public void TC006_Admin_Verify_if_you_are_able_to_give_feedback_to_a_candidate() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC006_Admin_Verify_if_you_are_able_to_give_feedback_to_a_candidate", "id");
		try {
			String testName = "TC006_Admin_Verify_if_you_are_able_to_give_feedback_to_a_candidate";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_CANDIDATE_DIR + "GiveFeedback.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			// String createdid = "b91d825298d24dbbad072ea52955bbe4";
			Response response = putWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/candidate/" + createdid + "/feedback");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains(createdid));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 7)
	public void TC007_Admin_Verify_if_you_are_able_to_move_candidate_to_Offer_stage() throws Exception {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC007_Admin_Verify_if_you_are_able_to_move_candidate_to_Offer_stage", "id");
		JSONObject jsonObject = new JSONObject();
		String interviewid = "951be493f58c4e54a671c8c7e2d3a868";
		jsonObject.put("interviewId", interviewid);
		jsonObject.put("jobId", "7d3bf62ac62f4c0f950fec090a1cdb57");
		JSONArray jsonArray = new JSONArray();
		jsonArray.add("dde12119d19d456fa748dab003bf8c28");
		jsonObject.put("stageActionIds", jsonArray);
		try {
			FileWriter file = new FileWriter(System.getProperty("user.dir") + Constants.JSONOBJECT_CANDIDATE_DIR
					+ "MoveCandidateBetweenStages.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC007_Admin_Verify_if_you_are_able_to_move_candidate_to_Offer_stage";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_CANDIDATE_DIR
					+ "MoveCandidateBetweenStages.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = putWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/candidate/" + createdid + "/move");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains(interviewid));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 8)
	public void TC008_Admin_Verify_if_user_can_get_all_feedback_for_a_candidate() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC008_Admin_Verify_if_user_can_get_all_feedback_for_a_candidate", "id");
		try {
			String testName = "TC008_Admin_Verify_if_user_can_get_all_feedback_for_a_candidate";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_CANDIDATE_DIR + "GiveFeedback.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/candidate/" + createdid + "/feedback");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains(createdid));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 9)
	public void TC009_Validate_if_we_are_able_to_sync_mails_for_a_candidate() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC009_Validate_if_we_are_able_to_sync_mails_for_a_candidate", "id");
		try {
			String testName = "TC009_Validate_if_we_are_able_to_sync_mails_for_a_candidate";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_CANDIDATE_DIR
					+ "SyncMailsForCandidate.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/candidate/" + createdid + "/syncMessages");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			// Assert.assertEquals(true, jsonAsString.contains(createdid));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 10)
	public void TC0010_Admin_Verify_if_user_can_get_all_the_screening_questions_for_the_candidate() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC0010_Admin_Verify_if_user_can_get_all_the_screening_questions_for_the_candidate", "id");
		try {
			String testName = "TC0010_Admin_Verify_if_user_can_get_all_the_screening_questions_for_the_candidate";
			reportUpdate(testName);
			headers = generateHeader();
			String createdid = "507e7682357145c582e461a6c6142165";
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/candidate/screeningQuestions/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 11)
	public void TC0011_Admin_Verify_if_user_can_update_the_screening_questions_for_the_candidate() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0011_Admin_Verify_if_user_can_update_the_screening_questions_for_the_candidate", "id");
		try {
			String testName = "TC0011_Admin_Verify_if_user_can_update_the_screening_questions_for_the_candidate";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_CANDIDATE_DIR
					+ "updateScreeningQuestions.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/candidate/screeningQuestions/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 12)
	public void TC0012_Admin_Verify_if_you_are_able_to_get_score_for_a_candidate() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0012_Admin_Verify_if_you_are_able_to_get_score_for_a_candidate", "id");
		try {
			String testName = "TC0012_Admin_Verify_if_you_are_able_to_get_score_for_a_candidate";
			reportUpdate(testName);
			headers = generateHeader();
			String jobid = "6bbe560fd1cb4d568f428444ef8a165f";
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/candidate/score/" + createdid + "/" + jobid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 13)
	public void TC0013_Admin_Verify_if_you_are_able_to_delete_a_candidate_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0013_Admin_Verify_if_you_are_able_to_delete_a_candidate_by_ID", "id");
		try {
			String testName = "TC0013_Admin_Verify_if_you_are_able_to_delete_a_candidate_by_ID";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.CREATE_JOB_POST_URL + "/candidate/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonsuitetestSuccessData.get("deleted_response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	public String addCandidate() throws Exception {
		String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_CANDIDATE_DIR + "postCandidate.json";
		payLoadBody = JsonComponent.readTextFile(filePath);
		headers = generateHeader();
		Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/candidate");
		System.out.println(response.body());
		System.out.println("Status code: " + response.statusCode());
		String jsonAsString = response.asString();
		System.out.println("Createtedddddd: " + jsonAsString.substring(8, 45));
		String id = jsonAsString.substring(9, 41);
		System.out.println("CreatedID: " + id);
		return id;
	}

	@SuppressWarnings("unchecked")
	public void addCandidateToJob() throws Exception {
		JSONObject js = new JSONObject();
		JSONArray Candidate = new JSONArray();
		Candidate.add(createdid);
		js.put("candidateIds", Candidate);
		js.put("interviewId", "09e10ceb34fc49e5a1b689c56c5d39e5");
		FileWriter file = new FileWriter(
				System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "AddJobToaCandidate.json");
		System.out.println(js.toJSONString());
		file.write(js.toJSONString());
		file.close();
		String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "AddJobToaCandidate.json";
		payLoadBody = JsonComponent.readTextFile(filePath);
		headers = generateHeader();
		String jobid = "6bbe560fd1cb4d568f428444ef8a165f";
		Response response = putWithHeaderAndBodyForm(headers, payLoadBody,
				Constants.CREATE_JOB_POST_URL + "/job/" + jobid + "/candidates");
		System.out.println("Status code: " + response.statusCode());

	}
}
