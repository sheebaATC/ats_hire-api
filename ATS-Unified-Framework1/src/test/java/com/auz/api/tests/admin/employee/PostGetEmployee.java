package com.auz.api.tests.admin.employee;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.SupportedUtils.RandomJsonGenerator;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class PostGetEmployee extends RestAssuredBase {
	public static HashMap<String, String> headers;
	public static String payLoadBody;

	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("adminSuiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	public static String createdid = "";

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Request from start of creating an employee, getting the employee by id, update and deleting the employee by id ";
			testSuiteDescription = "Validating in create,get,patch and delete employee";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })

	public static void addEmployeeJsonFile() throws Exception {

		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_EMPLOYEE_DIR + "AddEmployee.json");
			JSONObject js = new JSONObject();
			js.put("email", RandomJsonGenerator.randomEmailAddress(7));
			js.put("firstName", RandomJsonGenerator.randomFirstName());
			js.put("lastName", RandomJsonGenerator.randomLastName());
			js.put("organizationId", "02c3eda51b0146e1beecdcb52e2bfce8");
			JSONArray role = new JSONArray();
			role.add("c406fc4e087441b3b0ea3a1011adcf00");
			js.put("roleIds", role);
			System.out.println(js.toJSONString());
			file.write(js.toJSONString());
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Admin_Verify_user_is_able_to_create_new_employee_user_to_check_db_connection()
			throws IOException {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC001_Admin_Verify_user_is_able_to_create_new_employee_user_to_check_db_connection", "id");
		JSONObject js = new JSONObject();
		String email = RandomJsonGenerator.randomEmailAddress(7);
		js.put("email", email);
		js.put("firstName", RandomJsonGenerator.randomFirstName());
		js.put("lastName", RandomJsonGenerator.randomLastName());
		js.put("organizationId", "b665ccd67abe4e8dba9d7ffcae488093");
		JSONArray role = new JSONArray();
		role.add("012b18c05406467782aca76f2be6ba59");
		js.put("roleIds", role);
		JSONArray permission = new JSONArray();
		permission.add("f8e1b678a5584b0884774af4c0f9f5f4");
		js.put("permissionIds", permission);
		JSONArray job = new JSONArray();
		job.add("eeaf56204e774fe8abde4bb5f2c3967e");
		js.put("jobIds", job);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_EMPLOYEE_DIR + "AddEmployee.json");
			file.write(js.toJSONString());
			file.close();
			String testName = "TC001_Admin_Verify_user_is_able_to_create_new_employee_user_to_check_db_connection";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_EMPLOYEE_DIR + "AddEmployee.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/user");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(33, 65);
			String rcode = String.valueOf(responsecode);
			System.out.println("created id: " + id);
			createdid = id;
			Assert.assertEquals(true, jsonAsString.contains(id));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	public void TC002_Admin_Verify_if_you_user_is_able_to_find_an_user_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_if_you_user_is_able_to_find_an_user_by_ID", "id");
		try {
			String testName = "TC002_Admin_Verify_if_you_user_is_able_to_find_an_user_by_ID";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/user/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	// @Test(priority=3)
	// WE are not using this api anywhere in current implementation ATS-1114
	public void TC003_Admin_Verify_if_you_user_is_able_to_find_an_user_roles_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_if_you_user_is_able_to_find_an_user_roles_by_ID", "id");
		try {
			String testName = "TC003_Admin_Verify_if_you_user_is_able_to_find_an_user_roles_by_ID";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/user/" + createdid + "/roles");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC004_Admin_Verify_if_you_are_able_to_patch_user_to_checkDB_connection() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC004_Admin_Verify_if_you_are_able_to_patch_user_to_checkDB_connection", "id");
		PostGetEmployee objAddEmployee = new PostGetEmployee();
		JSONObject jsonObject = new JSONObject();
		String FName = RandomJsonGenerator.randomFirstName();
		jsonObject.put("email", RandomJsonGenerator.randomEmailAddress(7));
		jsonObject.put("firstName", FName);
		jsonObject.put("lastName", RandomJsonGenerator.randomLastName());
		JSONArray jsonArray = new JSONArray();
		jsonArray.add("012b18c05406467782aca76f2be6ba59");
		jsonObject.put("roleIds", jsonArray);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_EMPLOYEE_DIR + "UpdateEmployee.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC001_Admin_Verify_if_you_are_able_to_patch_user_to_checkDB_connection";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_EMPLOYEE_DIR
					+ "UpdateEmployee.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = patchWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/user/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains("012b18c05406467782aca76f2be6ba59"));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 5)
	public void TC005_Admin_Verify_if_you_are_able_to_update_role_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_if_you_are_able_to_update_role_by_ID", "id");
		try {
			PostGetEmployee objAddEmployee = new PostGetEmployee();
			String testName = "TC005_Admin_Verify_if_you_are_able_to_update_user_by_ID";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_EMPLOYEE_DIR
					+ "UpdateUserRole.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = patchWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/user/" + createdid + "/roles");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 6)
	public void TC006_Admin_Verify_user_is_able_to_resend_the_mail() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault("TC001_Admin_Verify_user_is_able_to_resend_the_mail",
				"id");
		try {
			String testName = "TC006_Admin_Verify_user_is_able_to_resend_the_mail";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = postWithHeaderParam(headers,
					Constants.CREATE_JOB_POST_URL + "/resendMail/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 7)
	public void TC007_Admin_Verify_if_you_are_able_to_delete_an_user_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_if_you_are_able_to_delete_an_user_by_ID", "id");
		try {
			String testName = "TC007_Admin_Verify_if_you_are_able_to_delete_an_user_by_ID";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.CREATE_JOB_POST_URL + "/user/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("deleted_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 8)
	public void TC008_Admin_Verify_if_you_user_is_able_to_find_all_events_of_users() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_if_you_user_is_able_to_find_all_events_of_users", "id");
		try {
			String testName = "TC008_Admin_Verify_if_you_user_is_able_to_find_all_events_of_users";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/fetch/user/events");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 9)
	public void TC009_Admin_Verify_if_you_user_is_able_to_find_all_users_in_organization() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_if_you_user_is_able_to_find_all_users_in_organization", "id");
		try {
			String testName = "TC009_Admin_Verify_if_you_user_is_able_to_find_all_users_in_organization";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getRequestWithQueryParams(headers, "organizationId", "2c3eda51b0146e1beecdcb52e2bfce8",
					Constants.CREATE_JOB_POST_URL + "/users");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 10)
	public void TC0010_Admin_Verify_if_you_user_is_able_to_find_email_existence_of_user() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_if_you_user_is_able_to_find_email_existence_of_user", "id");
		try {
			String testName = "TC0010_Admin_Verify_if_you_user_is_able_to_find_email_existence_of_user";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getRequestWithQueryParams(headers, "email", "abc@hotmail.com",
					Constants.CREATE_JOB_POST_URL + "/email-exists");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	public String addEmployee() throws Exception {
		addEmployeeJsonFile();
		String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_EMPLOYEE_DIR + "AddEmployee.json";
		payLoadBody = JsonComponent.readTextFile(filePath);
		headers = generateHeader();
		Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/user");
		System.out.println(response);
		System.out.println("Status code: " + response.statusCode());
		String jsonAsString = response.asString();
		String id = jsonAsString.substring(17, 49);
		System.out.println("ID: " + id);
		System.out.println("Response: " + jsonAsString);
		System.out.println("Body: " + response.getBody());
		return id;
	}
}
