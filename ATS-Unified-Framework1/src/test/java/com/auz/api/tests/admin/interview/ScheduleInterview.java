package com.auz.api.tests.admin.interview;

import java.io.FileWriter;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.restassured.tests.candidates.testcases.Post_AddNewCandidate;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class ScheduleInterview extends RestAssuredBase {

	public static HashMap<String, String> headers;
	public static String payLoadBody;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("adminSuiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	public static String createdid = "";
	public static String candidateid = "";

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Post request for schedule, update and delete Interview";
			testSuiteDescription = "Validating in passing the request to schedule, update and delete interviews";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 6)
	public void TC001_Admin_Verify_if_user_can_schedule_interview_by_id() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Admin_Verify_if_user_can_schedule_interview_by_id", "id");
		Post_AddNewCandidate objAddNewCandidate = new Post_AddNewCandidate();
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("jobId", "6bbe560fd1cb4d568f428444ef8a165f");
		jsonObject.put("interviewId", "09e10ceb34fc49e5a1b689c56c5d39e5");
		JSONArray js1 = new JSONArray();
		js1.add("sheeba@american-technology.net");
		jsonObject.put("guestEmails", js1);
		JSONArray js2 = new JSONArray();
		JSONObject js3 = new JSONObject();
		js3.put("email", "vinoth@american-technology.net");
		js3.put("id", "1a44b50f8dc24dff8433b46f16ffcd5b");
		js2.add(js3);
		jsonObject.put("inviteeIds", js2);
		jsonObject.put("start", "2019-10-15T10:30:25+05:30");
		jsonObject.put("description", "You are invited for the interview");
		jsonObject.put("isAvailabilityConfirmed", "false");
		jsonObject.put("end", "2019-10-15T11:30:25+05:30");
		jsonObject.put("location", "Office Campus");
		jsonObject.put("title", "StandUp");
		jsonObject.put("type", "VoiceCall");
		jsonObject.put("externalTitle", "ScheduleInterview");
		jsonArray.add(jsonObject);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_INTERVIEW_DIR + "postInteview.json");
			file.write(jsonArray.toJSONString());
			file.close();
			String testName = "TC001_Admin_Verify_if_user_can_schedule_interview_by_id";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_INTERVIEW_DIR + "postInteview.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			String candidateID = objAddNewCandidate.addCandidate();
			candidateid = candidateID;
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/candidate/" + candidateID + "/schedule");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(2060, 2092);
			System.out.println("CreatedID: " + id);
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains(candidateID));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
			createdid = id;
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 7)
	public void TC002_Admin_Verify_if_user_can_patch_an_inteview_by_id() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC002_Admin_Verify_if_user_can_patch_an_inteview_by_id", "id");
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = new JSONObject();
		JSONArray js1 = new JSONArray();
		js1.add("sheeba@american-technology.net");
		jsonObject.put("guestEmails", js1);
		jsonObject.put("candidateId", candidateid);
		jsonObject.put("title", "Candidate Interview: Voice Call (Senior Developement TestEngineer)");
		jsonObject.put("interviewId", "09e10ceb34fc49e5a1b689c56c5d39e5");
		jsonObject.put("location", "Office Campus2");
		jsonObject.put("type", "VoiceCall");
		jsonObject.put("externalTitle", "Candidate Interview: Voice Call (Senior Developement TestEngineer)");
		jsonObject.put("externalDescription", "UpdateEvents");
		JSONArray js2 = new JSONArray();
		JSONObject js3 = new JSONObject();
		js3.put("email", "vinoth@american-technology.net");
		js3.put("id", "1a44b50f8dc24dff8433b46f16ffcd5b");
		js2.add(js3);
		jsonObject.put("inviteeIds", js2);
		jsonObject.put("start", "2019-10-15T10:30:25+05:30");
		jsonObject.put("description", "You are invited for the interview");
		jsonObject.put("isAvailabilityConfirmed", false);
		jsonObject.put("end", "2019-10-15T11:30:25+05:30");
		jsonObject.put("jobId", "6bbe560fd1cb4d568f428444ef8a165f");
		jsonObject.put("scheduleId", createdid);
		jsonObject.put("interviewKit", null);
		jsonArray.add(jsonObject);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_INTERVIEW_DIR + "patchInteview.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC002_Admin_Verify_if_user_can_patch_an_inteview_by_id";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_INTERVIEW_DIR
					+ "patchInteview.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = patchWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/candidate/" + candidateid + "/schedule/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains(createdid));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 8)
	public void TC003_Admin_Verify_if_user_can_delete_schedule_interview_by_id() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Admin_Verify_if_user_can_delete_schedule_interview_by_id", "id");
		try {
			String testName = "TC003_Admin_Verify_if_user_can_delete_schedule_interview_by_id";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.CREATE_JOB_POST_URL + "/candidate/" + candidateid + "/schedule/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("deleted_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

}
