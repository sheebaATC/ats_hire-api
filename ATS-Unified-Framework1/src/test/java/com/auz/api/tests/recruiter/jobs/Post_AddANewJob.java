package com.auz.api.tests.recruiter.jobs;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.restassured.tests.candidates.testcases.Post_AddNewCandidate;
import com.auz.restassured.tests.location.testcases.Post_AddLocationUserToCheckDBConnection;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class Post_AddANewJob extends RestAssuredBase {
	public static HashMap<String, String> headers;
	public static String payLoadBody;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("suiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	static String locID = "";
	public static String createdid = "";

	public String location() throws IOException {
		Post_AddLocationUserToCheckDBConnection objAddLocation = new Post_AddLocationUserToCheckDBConnection();
		final String id = objAddLocation.AddLocation();
		locID = id;
		System.out.println("LOCATION: " + locID);
		return id;
	}

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Post Request for Create Job ,Get Job by ID ,Update Job by ID ,Delete Job & MapJob to Candidate ";
			testSuiteDescription = "Validating the request to  Create Job ,Get Job by ID ,Update Job by ID ,Delete Job & MapJob to Candidate ";
			location();
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })

	public static void addJobJSONFile() throws Exception {
		JSONObject js = new JSONObject();
		js.put("applicationEndDate", "2019-12-17T16:58:03.209657+05:30[Asia/Kolkata]");
		js.put("currency", "USD");
		js.put("description", "Sr. Quality Assurance Eng. Roles ... and Responsibilities ...");
		js.put("educationQualification", "BTech");
		js.put("educationQualificationType", "Bachelor_Degree");
		JSONArray EmpType = new JSONArray();
		EmpType.add("Contract");
		js.put("employmentTypes", EmpType);
		js.put("locationId", locID);
		js.put("maxBudget", 200000.5);
		js.put("maxExperience", 5);
		js.put("minBudget", 50000.5);
		js.put("minExperience", 1);
		js.put("noOfVacancies", 3);
		js.put("payType", "Monthly");
		js.put("role", "Sr. Qa Eng");
		js.put("title", "Sr. Quality Assurance Eng");
		JSONArray js3 = new JSONArray();
		js3.add("FreeJobBoards");
		js3.add("JobTarget");
		js.put("externalPublishTypes", js3);
		JSONObject interview1 = new JSONObject();
		interview1.put("title", "First Introduction Round");
		interview1.put("stage", 1);
		JSONObject interview2 = new JSONObject();
		interview2.put("title", "Second Introduction Round");
		interview2.put("stage", 2);
		JSONArray interviews = new JSONArray();
		interviews.add(interview1);
		interviews.add(interview2);
		js.put("interviews", interviews);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "TC001_postJob.json");
			file.write(js.toJSONString());
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Recruiter_Verify_user_is_able_to_create_a_new_job_with_employment_type_Contract()
			throws Exception {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC001_Recruiter_Verify_user_is_able_to_create_a_new_job_with_employment_type_Contract", "id");

		try {
			addJobJSONFile();
			String testName = "TC001_Recruiter_Verify_user_is_able_to_create_a_new_job_with_employment_type_Contract";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "TC001_postJob.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/job");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(15, 47);
			System.out.println("CreatedID: " + id);
			createdid = id;
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains(createdid));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")

	@Test(priority = 2)
	public void TC002_Recruiter_Verify_if_you_are_able_to_find_a_job_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Recruiter_Verify_if_you_are_able_to_find_a_job_by_ID", "id");
		try {
			String testName = "TC002_Recruiter_Verify_if_you_are_able_to_find_a_job_by_ID";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/job/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains(createdid));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC003_Recruiter_Verify_if_you_are_able_to_publish_job_internally_by_ID() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC001_Recruiter_Verify_user_is_able_to_add_candidate_successfully_without_mapping_job", "id");
		try {
			String testName = "TC003_Recruiter_Verify_if_you_are_able_to_publish_job_internally_by_ID";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "publishJob.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = putWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/job/" + createdid + "/publish");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains(createdid));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC004_Recruiter_Verify_if_you_are_able_to_update_a_job_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Recruiter_Verify_if_you_are_able_to_update_a_job_by_ID", "id");
		JSONObject js = new JSONObject();
		String paytype = "Hourly";
		js.put("applicationEndDate", "2019-12-17T16:58:03.209657+05:30[Asia/Kolkata]");
		js.put("currency", "USD");
		js.put("locationId", locID);
		js.put("description", "Sr. Quality Assurance Eng. Roles ... and Responsibilities ...");
		js.put("educationQualification", "BTech");
		js.put("educationQualificationType", "Bachelor_Degree");
		JSONArray EmpType = new JSONArray();
		EmpType.add("Contract");
		js.put("employmentTypes", EmpType);
		js.put("maxBudget", 250000.5);
		js.put("maxExperience", 5);
		js.put("minBudget", 55000.5);
		js.put("minExperience", 1);
		js.put("noOfVacancies", 3);
		js.put("payType", paytype);
		js.put("publishType", "Internal");
		js.put("publishUrl", "https://auzmor.com/jobs/backend-engineer");
		js.put("role", "Sr. Qa Eng");
		js.put("status", "OnHold");
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "updateJob.json");
			file.write(js.toJSONString());
			file.close();
			String testName = "TC004_Recruiter_Verify_if_you_are_able_to_update_a_job_by_ID";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "updateJob.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = patchWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/job/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains(paytype));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 5)
	public void TC005_Recruiter_Verify_if_user_is_able_to_get_add_candidate_to_job() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Recruiter_Verify_if_user_is_able_to_get_add_candidate_to_job", "id");
		try {
			String testName = "TC005_Recruiter_Verify_if_you_are_able_to_find_a_job_by_ID";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/job/stage-actions/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains("interviews"));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 6)
	public void TC006_Recruiter_Verify_if_you_are_able_to_add_a_candidate_to_job() throws Exception {
		Post_AddNewCandidate objAddCandidate = new Post_AddNewCandidate();
		JSONObject js = new JSONObject();
		JSONArray Candidate = new JSONArray();
		Candidate.add(objAddCandidate.addCandidate());
		js.put("candidateIds", Candidate);
		js.put("interviewId", "951be493f58c4e54a671c8c7e2d3a868");
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Recruiter_Verify_if_you_are_able_to_add_a_candidate_to_job", "id");
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "AddJobToaCandidate.json");
			file.write(js.toJSONString());
			file.close();
			String testName = "TC006_Recruiter_Verify_if_you_are_able_to_add_a_candidate_to_job";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "AddJobToaCandidate.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = putWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/job/" + createdid + "/candidates");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains(createdid));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 7)
	public void TC007_Recruiter_Verify_if_you_are_able_to_delete_a_job_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Recruiter_Verify_if_you_are_able_to_delete_a_job_by_ID", "id");
		try {
			String testName = "TC007_Recruiter_Verify_if_you_are_able_to_delete_a_job_by_ID";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.CREATE_JOB_POST_URL + "/job/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("deleted_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })

	public static void addJobJSONFile2() throws Exception {
		JSONObject js = new JSONObject();
		js.put("applicationEndDate", "2019-12-17T16:58:03.209657+05:30[Asia/Kolkata]");
		js.put("currency", "USD");
		js.put("description", "Sr. Quality Assurance Eng. Roles ... and Responsibilities ...");
		js.put("educationQualification", "BTech");
		js.put("educationQualificationType", "Bachelor_Degree");
		JSONArray EmpType = new JSONArray();
		EmpType.add("Full Time");
		js.put("employmentTypes", EmpType);
		js.put("locationId", locID);
		js.put("maxBudget", 200000.5);
		js.put("maxExperience", 5);
		js.put("minBudget", 50000.5);
		js.put("minExperience", 1);
		js.put("noOfVacancies", 3);
		js.put("payType", "Monthly");
		js.put("role", "Sr. Qa Eng");
		js.put("title", "Sr. Quality Assurance Eng");
		JSONObject interview1 = new JSONObject();
		interview1.put("title", "First Introduction Round");
		interview1.put("stage", 1);
		JSONObject interview2 = new JSONObject();
		interview2.put("title", "Second Introduction Round");
		interview2.put("stage", 2);
		JSONArray interviews = new JSONArray();
		interviews.add(interview1);
		interviews.add(interview2);
		js.put("interviews", interviews);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "TC002_postJob.json");
			file.write(js.toJSONString());
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "unchecked" })
	@Test(priority = 8)
	public void TC008_Recruiter_Verify_user_is_able_to_create_a_new_job_with_employment_type_Full_Time()
			throws Exception {
		String createdId = "";
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC002_Recruiter_Verify_user_is_able_to_create_a_new_job_with_employment_type_Full_Time", "id");
		try {
			addJobJSONFile2();
			String testName = "TC008_Recruiter_Verify_user_is_able_to_create_a_new_job_with_employment_type_Full_Time";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "TC001_postJob.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/job");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(15, 47);
			System.out.println("CreatedID: " + id);
			createdId = id;
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
		try {
			deleteJob(createdId);
		} catch (Exception e) {
			e.getMessage();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })

	public static void addJobJSONFile3() throws Exception {
		JSONObject js = new JSONObject();
		js.put("applicationEndDate", "2019-12-17T16:58:03.209657+05:30[Asia/Kolkata]");
		js.put("currency", "USD");
		js.put("description", "Sr. Quality Assurance Eng. Roles ... and Responsibilities ...");
		js.put("educationQualification", "BTech");
		js.put("educationQualificationType", "Bachelor_Degree");
		JSONArray EmpType = new JSONArray();
		EmpType.add("Part Time");
		js.put("employmentTypes", EmpType);
		js.put("locationId", locID);
		js.put("maxBudget", 200000.5);
		js.put("maxExperience", 5);
		js.put("minBudget", 50000.5);
		js.put("minExperience", 1);
		js.put("noOfVacancies", 3);
		js.put("payType", "Monthly");
		js.put("role", "Sr. Qa Eng");
		js.put("title", "Sr. Quality Assurance Eng");
		JSONObject interview1 = new JSONObject();
		interview1.put("title", "First Introduction Round");
		interview1.put("stage", 1);
		JSONObject interview2 = new JSONObject();
		interview2.put("title", "Second Introduction Round");
		interview2.put("stage", 2);
		JSONArray interviews = new JSONArray();
		interviews.add(interview1);
		interviews.add(interview2);
		js.put("interviews", interviews);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "TC003_postJob.json");
			file.write(js.toJSONString());
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 9)
	public void TC009_Recruiter_Verify_user_is_able_to_create_a_new_job_with_employment_type_Part_Time()
			throws Exception {
		String createdId = "";
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC003_Recruiter_Verify_user_is_able_to_create_a_new_job_with_employment_type_Part_Time", "id");
		try {
			addJobJSONFile3();
			String testName = "TC009_Recruiter_Verify_user_is_able_to_create_a_new_job_with_employment_type_Part_Time";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "TC003_postJob.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/job");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(15, 47);
			System.out.println("CreatedID: " + id);
			createdId = id;
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
		try {
			deleteJob(createdId);
		} catch (Exception e) {
			e.getMessage();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })

	public static void addJobJSONFile4() throws Exception {
		Post_AddLocationUserToCheckDBConnection objAddLocation = new Post_AddLocationUserToCheckDBConnection();
		JSONObject js = new JSONObject();
		js.put("applicationEndDate", "2019-12-17T16:58:03.209657+05:30[Asia/Kolkata]");
		js.put("currency", "USD");
		js.put("description", "Sr. Quality Assurance Eng. Roles ... and Responsibilities ...");
		js.put("educationQualification", "BTech");
		js.put("educationQualificationType", "Bachelor_Degree");
		JSONArray EmpType = new JSONArray();
		EmpType.add("Internship");
		js.put("employmentTypes", EmpType);
		js.put("locationId", locID);
		js.put("maxBudget", 200000.5);
		js.put("maxExperience", 5);
		js.put("minBudget", 50000.5);
		js.put("minExperience", 1);
		js.put("noOfVacancies", 3);
		js.put("payType", "Monthly");
		js.put("role", "Sr. Qa Eng");
		js.put("title", "Sr. Quality Assurance Eng");
		JSONObject interview1 = new JSONObject();
		interview1.put("title", "First Introduction Round");
		interview1.put("stage", 1);
		JSONObject interview2 = new JSONObject();
		interview2.put("title", "Second Introduction Round");
		interview2.put("stage", 2);
		JSONArray interviews = new JSONArray();
		interviews.add(interview1);
		interviews.add(interview2);
		js.put("interviews", interviews);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "TC004_postJob.json");
			file.write(js.toJSONString());
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 10)
	public void TC0010_Recruiter_Verify_user_is_able_to_create_a_new_job_with_employment_type_Internship()
			throws Exception {
		String createdId = "";
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC004_Recruiter_Verify_user_is_able_to_create_a_new_job_with_employment_type_Internship", "id");

		try {
			addJobJSONFile4();
			String testName = "TC0010_Recruiter_Verify_user_is_able_to_create_a_new_job_with_employment_type_Internship";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "TC004_postJob.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/job");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(15, 47);
			System.out.println("CreatedID: " + id);
			createdId = id;
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
		try {
			deleteJob(createdId);
		} catch (Exception e) {
			e.getMessage();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })

	public static void addJobJSONFile5() throws Exception {
		JSONObject js = new JSONObject();
		js.put("applicationEndDate", "2019-12-17T16:58:03.209657+05:30[Asia/Kolkata]");
		js.put("currency", "USD");
		js.put("description", "Sr. Quality Assurance Eng. Roles ... and Responsibilities ...");
		js.put("educationQualification", "BTech");
		js.put("educationQualificationType", "Bachelor_Degree");
		JSONArray EmpType = new JSONArray();
		EmpType.add("Temporary");
		js.put("employmentTypes", EmpType);
		js.put("locationId", locID);
		js.put("maxBudget", 200000.5);
		js.put("maxExperience", 5);
		js.put("minBudget", 50000.5);
		js.put("minExperience", 1);
		js.put("noOfVacancies", 3);
		js.put("payType", "Monthly");
		js.put("role", "Sr. Qa Eng");
		js.put("title", "Sr. Quality Assurance Eng");
		JSONObject interview1 = new JSONObject();
		interview1.put("title", "First Introduction Round");
		interview1.put("stage", 1);
		JSONObject interview2 = new JSONObject();
		interview2.put("title", "Second Introduction Round");
		interview2.put("stage", 2);
		JSONArray interviews = new JSONArray();
		interviews.add(interview1);
		interviews.add(interview2);
		js.put("interviews", interviews);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "TC005_postJob.json");
			file.write(js.toJSONString());
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 11)
	public void TC0011_Recruiter_Verify_user_is_able_to_create_a_new_job_with_employment_type_Temporary()
			throws Exception {
		String createdId = "";
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC005_Recruiter_Verify_user_is_able_to_create_a_new_job_with_employment_type_Temporary", "id");

		try {
			addJobJSONFile5();
			String testName = "TC0011_Recruiter_Verify_user_is_able_to_create_a_new_job_with_employment_type_Temporary";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "TC005_postJob.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/job");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(15, 47);
			System.out.println("CreatedID: " + id);
			createdId = id;
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
		try {
			deleteJob(createdId);
		} catch (Exception e) {
			e.getMessage();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })

	public static void addJobJSONFile6() throws Exception {
		JSONObject js = new JSONObject();
		js.put("applicationEndDate", "2019-12-17T16:58:03.209657+05:30[Asia/Kolkata]");
		js.put("currency", "USD");
		js.put("description", "Sr. Quality Assurance Eng. Roles ... and Responsibilities ...");
		js.put("educationQualification", "BTech");
		js.put("educationQualificationType", "Bachelor_Degree");
		JSONArray EmpType = new JSONArray();
		EmpType.add("Temporary");
		js.put("employmentTypes", EmpType);
		js.put("locationId", locID);
		js.put("maxBudget", 200000.5);
		js.put("maxExperience", 5);
		js.put("minBudget", 50000.5);
		js.put("minExperience", 1);
		js.put("noOfVacancies", 3);
		js.put("payType", "Hourly");
		js.put("role", "Sr. Qa Eng");
		js.put("title", "Sr. Quality Assurance Eng");
		JSONObject interview1 = new JSONObject();
		interview1.put("title", "First Introduction Round");
		interview1.put("stage", 1);
		JSONObject interview2 = new JSONObject();
		interview2.put("title", "Second Introduction Round");
		interview2.put("stage", 2);
		JSONArray interviews = new JSONArray();
		interviews.add(interview1);
		interviews.add(interview2);
		js.put("interviews", interviews);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "TC006_postJob.json");
			file.write(js.toJSONString());
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 12)
	public void TC0012_Recruiter_Verify_user_is_able_to_create_a_new_job_with_paytype_hourly() throws Exception {
		String createdId = "";
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC006_Recruiter_Verify_user_is_able_to_create_a_new_job_with_paytype_hourly", "id");

		try {
			addJobJSONFile6();
			String testName = "TC0012_Recruiter_Verify_user_is_able_to_create_a_new_job_with_paytype_hourly";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "TC006_postJob.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/job");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(15, 47);
			System.out.println("CreatedID: " + id);
			createdId = id;
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
		try {
			deleteJob(createdId);
		} catch (Exception e) {
			e.getMessage();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })

	public static void addJobJSONFile7() throws Exception {
		JSONObject js = new JSONObject();
		js.put("applicationEndDate", "2019-12-17T16:58:03.209657+05:30[Asia/Kolkata]");
		js.put("currency", "USD");
		js.put("description", "Sr. Quality Assurance Eng. Roles ... and Responsibilities ...");
		js.put("educationQualification", "BTech");
		js.put("educationQualificationType", "Bachelor_Degree");
		JSONArray EmpType = new JSONArray();
		EmpType.add("Temporary");
		js.put("employmentTypes", EmpType);
		js.put("locationId", locID);
		js.put("maxBudget", 200000.5);
		js.put("maxExperience", 5);
		js.put("minBudget", 50000.5);
		js.put("minExperience", 1);
		js.put("noOfVacancies", 3);
		js.put("payType", "Daily");
		js.put("role", "Sr. Qa Eng");
		js.put("title", "Sr. Quality Assurance Eng");
		JSONObject interview1 = new JSONObject();
		interview1.put("title", "First Introduction Round");
		interview1.put("stage", 1);
		JSONObject interview2 = new JSONObject();
		interview2.put("title", "Second Introduction Round");
		interview2.put("stage", 2);
		JSONArray interviews = new JSONArray();
		interviews.add(interview1);
		interviews.add(interview2);
		js.put("interviews", interviews);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "TC007_postJob.json");
			file.write(js.toJSONString());
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 13)
	public void TC0013_Recruiter_Verify_user_is_able_to_create_a_new_job_with_paytype_daily() throws Exception {
		String createdId = "";
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC007_Recruiter_Verify_user_is_able_to_create_a_new_job_with_paytype_daily", "id");

		try {
			addJobJSONFile7();
			String testName = "TC0013_Recruiter_Verify_user_is_able_to_create_a_new_job_with_paytype_daily";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "TC007_postJob.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/job");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(15, 47);
			System.out.println("CreatedID: " + id);
			createdId = id;
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
		try {
			deleteJob(createdId);
		} catch (Exception e) {
			e.getMessage();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })

	public static void addJobJSONFile8() throws Exception {
		JSONObject js = new JSONObject();
		js.put("applicationEndDate", "2019-12-17T16:58:03.209657+05:30[Asia/Kolkata]");
		js.put("currency", "USD");
		js.put("description", "Sr. Quality Assurance Eng. Roles ... and Responsibilities ...");
		js.put("educationQualification", "BTech");
		js.put("educationQualificationType", "Bachelor_Degree");
		JSONArray EmpType = new JSONArray();
		EmpType.add("Temporary");
		js.put("employmentTypes", EmpType);
		js.put("locationId", locID);
		js.put("maxBudget", 200000.5);
		js.put("maxExperience", 5);
		js.put("minBudget", 50000.5);
		js.put("minExperience", 1);
		js.put("noOfVacancies", 3);
		js.put("payType", "Weekly");
		js.put("role", "Sr. Qa Eng");
		js.put("title", "Sr. Quality Assurance Eng");
		JSONObject interview1 = new JSONObject();
		interview1.put("title", "First Introduction Round");
		interview1.put("stage", 1);
		JSONObject interview2 = new JSONObject();
		interview2.put("title", "Second Introduction Round");
		interview2.put("stage", 2);
		JSONArray interviews = new JSONArray();
		interviews.add(interview1);
		interviews.add(interview2);
		js.put("interviews", interviews);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "TC008_postJob.json");
			file.write(js.toJSONString());
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 14)
	public void TC0014_Recruiter_Verify_user_is_able_to_create_a_new_job_with_paytype_weekly() throws Exception {
		String createdId = "";
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC008_Recruiter_Verify_user_is_able_to_create_a_new_job_with_paytype_weekly", "id");

		try {
			addJobJSONFile8();
			String testName = "TC0014_Recruiter_Verify_user_is_able_to_create_a_new_job_with_paytype_weekly";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "TC008_postJob.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/job");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(15, 47);
			System.out.println("CreatedID: " + id);
			createdId = id;
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
		try {
			deleteJob(createdId);
		} catch (Exception e) {
			e.getMessage();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })

	public static void addJobJSONFile9() throws Exception {
		JSONObject js = new JSONObject();
		js.put("applicationEndDate", "2019-12-17T16:58:03.209657+05:30[Asia/Kolkata]");
		js.put("currency", "USD");
		js.put("description", "Sr. Quality Assurance Eng. Roles ... and Responsibilities ...");
		js.put("educationQualification", "BTech");
		js.put("educationQualificationType", "Bachelor_Degree");
		JSONArray EmpType = new JSONArray();
		EmpType.add("Temporary");
		js.put("employmentTypes", EmpType);
		js.put("locationId", locID);
		js.put("maxBudget", 200000.5);
		js.put("maxExperience", 5);
		js.put("minBudget", 50000.5);
		js.put("minExperience", 1);
		js.put("noOfVacancies", 3);
		js.put("payType", "ByMonthly");
		js.put("role", "Sr. Qa Eng");
		js.put("title", "Sr. Quality Assurance Eng");
		JSONObject interview1 = new JSONObject();
		interview1.put("title", "First Introduction Round");
		interview1.put("stage", 1);
		JSONObject interview2 = new JSONObject();
		interview2.put("title", "Second Introduction Round");
		interview2.put("stage", 2);
		JSONArray interviews = new JSONArray();
		interviews.add(interview1);
		interviews.add(interview2);
		js.put("interviews", interviews);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "TC009_postJob.json");
			file.write(js.toJSONString());
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 15)
	public void TC0015_Recruiter_Verify_user_is_able_to_create_a_new_job_with_employment_type_bymonthly()
			throws Exception {
		String createdId = "";
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC009_Recruiter_Verify_user_is_able_to_create_a_new_job_with_employment_type_bymonthly", "id");
		try {
			addJobJSONFile9();
			String testName = "TC0015_Recruiter_Verify_user_is_able_to_create_a_new_job_with_employment_type_bymonthly";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "TC009_postJob.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/job");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(15, 47);
			System.out.println("CreatedID: " + id);
			createdId = id;
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
		try {
			deleteJob(createdId);
		} catch (Exception e) {
			e.getMessage();
		}
	}

	@SuppressWarnings("unchecked")
	public String addJob() throws Exception {

		Post_AddLocationUserToCheckDBConnection objAddLocation = new Post_AddLocationUserToCheckDBConnection();
		JSONObject js = new JSONObject();
		js.put("applicationEndDate", "2019-12-17T16:58:03.209657+05:30[Asia/Kolkata]");
		js.put("currency", "USD");
		js.put("description", "Sr. Quality Assurance Eng. Roles ... and Responsibilities ...");
		js.put("educationQualification", "BTech");
		js.put("educationQualificationType", "Bachelor_Degree");
		JSONArray EmpType = new JSONArray();
		EmpType.add("Contract");
		js.put("employmentTypes", EmpType);
		js.put("locationId", objAddLocation.AddLocation());
		js.put("maxBudget", 200000.5);
		js.put("maxExperience", 5);
		js.put("minBudget", 50000.5);
		js.put("minExperience", 1);
		js.put("noOfVacancies", 3);
		js.put("payType", "Monthly");
		js.put("role", "Sr. Qa Eng");
		js.put("title", "Sr. Quality Assurance Eng");
		JSONObject interview1 = new JSONObject();
		interview1.put("title", "First Introduction Round");
		interview1.put("stage", 1);
		JSONObject interview2 = new JSONObject();
		interview2.put("title", "Second Introduction Round");
		interview2.put("stage", 2);
		JSONArray interviews = new JSONArray();
		interviews.add(interview1);
		interviews.add(interview2);
		js.put("interviews", interviews);
		FileWriter file = new FileWriter(
				System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "TC001_postJob.json");
		file.write(js.toJSONString());
		file.close();
		String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "TC001_postJob.json";
		payLoadBody = JsonComponent.readTextFile(filePath);
		headers = generateHeader();
		Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/job");
		logResponseInReport(response);
		System.out.println("Status code: " + response.statusCode());
		String jsonAsString = response.asString();
		String id = jsonAsString.substring(15, 47);
		System.out.println("CreatedID: " + id);
		return id;
	}

	public void deleteJob(String id) throws Exception {
		headers = generateHeader();
		Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
				Constants.CREATE_JOB_POST_URL + "/job/" + id);
		logResponseInReport(response);
		System.out.println("Status code: " + response.statusCode());
		verifyResponseCode(response, "204");
		verifyContentType(response, Constants.JSON_CONTENTTYPE);
		verifyResponseTime(response, 1000);
	}

}
