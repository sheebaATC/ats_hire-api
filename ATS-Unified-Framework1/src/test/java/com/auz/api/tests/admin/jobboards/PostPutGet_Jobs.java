package com.auz.api.tests.admin.jobboards;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.restassured.tests.jobs.testcases.Post_AddANewJob;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class PostPutGet_Jobs extends RestAssuredBase {
	public static HashMap<String, String> headers;
	public static String payLoadBody;

	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("adminSuiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	public static String createdid = "";
	Post_AddANewJob objAddjob = new Post_AddANewJob();

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Request from start of creating , updating and getting the job integrations";
			testSuiteDescription = "Validating in creating , updating and getting the job integrations";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Admin_Verify_user_is_able_to_get_the_jobs() throws IOException {
		Object testrailObj = jsonsuitetestrailData.getOrDefault("TC001_Admin_Verify_user_is_able_to_get_the_jobs",
				"id");
		try {
			String testName = "TC001_Admin_Verify_user_is_able_to_get_the_jobs";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.HIRE_QA_URL + "/integrations/job-boards/jobs");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			// System.out.println("created id: "+id);
			// MongoDBConnectionManager db = new MongoDBConnectionManager();
			// BasicDBObject searchQuery = new BasicDBObject();
			// searchQuery.put("_id", orgID);
			// Object mongoDatabaseQA = db.MongoDatabaseQAOrgID(searchQuery);
			// String DBvalue = mongoDatabaseQA.toString();
			// Assert.assertEquals(true, (DBvalue.contains(orgID)));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	public void TC002_Admin_Verify_user_is_able_to_publish_jobs_by_id() throws IOException {
		Object testrailObj = jsonsuitetestrailData.getOrDefault("TC002_Admin_Verify_user_is_able_to_publish_jobs_by_id",
				"id");
		try {
			// createdid = objAddjob.addJob();
			createdid = "b0606cb2eedf4a7eac0eff7cb3ef90c3";
			JSONObject js = new JSONObject();
			js.put("title", "Sr. Quality Assurance Eng");
			js.put("description", "Sr. Quality Assurance Eng. Roles ... and Responsibilities ...");
			js.put("jobViewUrl", "https://hire-qa.auzmor.com/american-technology/careers/" + createdid);
			js.put("applyUrl", "https://hire-qa.auzmor.com/american-technology/careers/" + createdid);
			js.put("maxBudget", 200000.5);
			js.put("minBudget", 50000.5);
			js.put("minExperience", 1);
			js.put("educationQualificationType", "Bachelor_Degree");
			js.put("publishOnFreeJobBoards", true);
			js.put("publishOnJobTarget", true);
			JSONObject js1 = new JSONObject();
			js1.put("city", "New York City");
			js1.put("state", "New York");
			js1.put("country", "United States");
			js1.put("name", "New York City, New York, United States");
			js1.put("zipCode", "100002");
			js.put("location", js1);
			JSONArray js2 = new JSONArray();
			js2.add("Contract");
			js.put("employmentTypes", js2);
			JSONArray js3 = new JSONArray();
			js3.add("FreeJobBoards");
			js3.add("JobTarget");
			js.put("externalPublishTypes", js3);
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_JOBBOARDS_DIR + "publishjob.json");
			file.write(js.toJSONString());
			file.close();
			String testName = "TC002_Admin_Verify_user_is_able_to_publish_jobs_by_id";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_JOBBOARDS_DIR + "publishjob.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.HIRE_QA_URL + "/integrations/job-boards/jobs/" + createdid + "/publish");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains("\"published\": true"));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC003_Admin_Verify_if_you_user_is_able_to_get_jobs_by_id() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Admin_Verify_if_you_user_is_able_to_get_jobs_by_id", "id");
		try {
			String testName = "TC003_Admin_Verify_if_you_user_is_able_to_get_jobs_by_id";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers,
					Constants.HIRE_QA_URL + "/integrations/job-boards/jobs/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC004_Admin_Verify_if_you_user_is_able_to_put_jobs_by_id() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC004_Admin_Verify_if_you_user_is_able_to_put_jobs_by_id", "id");
		try {
			JSONObject js = new JSONObject();
			js.put("title", "Sr. Quality Assurance Eng");
			js.put("description", "Sr. Quality Assurance Eng. Roles ... and Responsibilities ...");
			js.put("jobViewUrl", "https://hire-qa.auzmor.com/american-technology/careers/" + createdid);
			js.put("applyUrl", "https://hire-qa.auzmor.com/american-technology/careers/" + createdid);
			js.put("maxBudget", 250000.5);
			js.put("minBudget", 51000.5);
			js.put("minExperience", 5);
			js.put("educationQualificationType", "Bachelor_Degree");
			js.put("publishOnFreeJobBoards", true);
			JSONObject js1 = new JSONObject();
			js1.put("city", "New York City");
			js1.put("state", "New York");
			js1.put("country", "United States");
			js1.put("name", "New York City, New York, United States");
			js1.put("zipCode", "100002");
			js.put("location", js1);
			JSONArray js2 = new JSONArray();
			js2.add("Contract");
			js.put("employmentTypes", js2);
			JSONArray js3 = new JSONArray();
			js3.add("FreeJobBoards");
			js.put("externalPublishTypes", js3);
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_JOBBOARDS_DIR + "putJobs.json");
			file.write(js.toJSONString());
			file.close();
			String testName = "TC004_Admin_Verify_if_you_user_is_able_to_put_jobs_by_id";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_JOBBOARDS_DIR + "putJobs.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = putWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.HIRE_QA_URL + "/integrations/job-boards/jobs/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 5)
	public void TC005_Admin_Verify_user_is_able_to_stop_job_by_id() throws IOException {
		Object testrailObj = jsonsuitetestrailData.getOrDefault("TC005_Admin_Verify_user_is_able_to_stop_job_by_id",
				"id");
		try {
			String testName = "TC005_Admin_Verify_user_is_able_to_stop_job_by_id";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = postWithHeaderParam(headers,
					Constants.HIRE_QA_URL + "/integrations/job-boards/jobs/" + createdid + "+/stop");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 6)
	public void TC006_Admin_Verify_if_you_user_is_able_to_get_jobs_by_id_after_stopping_job() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC006_Admin_Verify_if_you_user_is_able_to_get_jobs_by_id_after_stopping_job", "id");
		try {
			String testName = "TC006_Admin_Verify_if_you_user_is_able_to_get_jobs_by_id_after_stopping_job";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers,
					Constants.HIRE_QA_URL + "/integrations/job-boards/jobs/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 7)
	public void TC007_Admin_Verify_user_is_able_to_get_the_google_jobs_by_id() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC007_Admin_Verify_user_is_able_to_get_the_google_jobs_by_id", "id");
		try {
			String testName = "TC007_Admin_Verify_user_is_able_to_get_the_google_jobs_by_id";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers,
					Constants.HIRE_QA_URL + "/integrations/job-boards/jobs/" + createdid + "/google-jobs");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 8)
	public void TC008_Admin_Verify_user_is_able_to_get_the_monster_jobs_by_id() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC009_Admin_Verify_user_is_able_to_get_the_monster_jobs_by_id", "id");
		try {
			String testName = "TC009_Admin_Verify_user_is_able_to_get_the_monster_jobs_by_id";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers,
					Constants.HIRE_QA_URL + "/integrations/job-boards/jobs/all/monster-jobs");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

}
