package com.auz.api.tests.admin.jobs;

import java.util.HashMap;

import org.json.simple.JSONObject;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.restassuredbase.coreutils.RestAssuredBase;

import io.restassured.response.Response;

public class Put_PublishJobByID extends RestAssuredBase {

	public static HashMap<String, String> headers;
	public static String payLoadBody;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("adminSuiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);

	public String publishJob() throws Exception {
		Post_AddANewJob objAddJob = new Post_AddANewJob();
		String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_JOB_DIR + "publishJob.json";
		payLoadBody = JsonComponent.readTextFile(filePath);
		headers = generateHeader();
		String jobId = objAddJob.addJob();
		Response response = putWithHeaderAndBodyForm(headers, payLoadBody,
				Constants.CREATE_JOB_POST_URL + "/job/" + jobId + "/publish");
		logResponseInReport(response);
		System.out.println("Status code: " + response.statusCode());
		String jsonAsString = response.asString();
		String id = jsonAsString.substring(15, 47);
		System.out.println("CreatedID: " + id);
		return id;
	}

}
