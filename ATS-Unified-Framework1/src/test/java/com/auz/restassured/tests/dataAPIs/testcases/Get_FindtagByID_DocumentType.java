package com.auz.restassured.tests.dataAPIs.testcases;

import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class Get_FindtagByID_DocumentType extends RestAssuredBase {

	public static HashMap<String, String> headers;
	public static String payLoadBody;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("suiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Get Request to get data by passing the datatype as Document Type";
			testSuiteDescription = "Validating in passing the request to get data by passing the datatype as Document Type";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test()
	public void TC001_Superadmin_Verify_if_user_can_get_the_datatype_for_DocumentType_Resume() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_if_user_can_get_the_datatype_for_DocumentType_Resume", "id");
		try {
			String testName = "TC001_Superadmin_Verify_if_user_can_get_the_datatype_for_DocumentType_Resume";
			reportUpdate(testName);
			headers = generateHeader();
			String datatype = "DocumentType";
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/data/" + datatype);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			String jsonAsString = response.asString();
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonAsString.contains((String) jsonsuitetestSuccessData.get("documenttype1")), true);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test()
	public void TC002_Superadmin_Verify_if_user_can_get_the_datatype_for_DocumentType_PaySlip() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC002_Superadmin_Verify_if_user_can_get_the_datatype_for_DocumentType_PaySlip", "id");
		try {
			String testName = "TC002_Superadmin_Verify_if_user_can_get_the_datatype_for_DocumentType_PaySlip";
			reportUpdate(testName);
			headers = generateHeader();
			String datatype = "DocumentType";
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/data/" + datatype);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			String jsonAsString = response.asString();
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonAsString.contains((String) jsonsuitetestSuccessData.get("documenttype2")), true);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test()
	public void TC003_Superadmin_Verify_if_user_can_get_the_datatype_for_DocumentType_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Superadmin_Verify_if_user_can_get_the_datatype_for_DocumentType_ID", "id");
		try {
			String testName = "TC003_Superadmin_Verify_if_user_can_get_the_datatype_for_CandidateStatus_Shortlisted";
			reportUpdate(testName);
			headers = generateHeader();
			String datatype = "DocumentType";
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/data/" + datatype);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			String jsonAsString = response.asString();
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonAsString.contains((String) jsonsuitetestSuccessData.get("documenttype3")), true);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test()
	public void TC004_Superadmin_Verify_if_user_can_get_the_datatype_for_DocumentType_BankAcStatement() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC004_Superadmin_Verify_if_user_can_get_the_datatype_for_DocumentType_BankAcStatement", "id");
		try {
			String testName = "TC004_Superadmin_Verify_if_user_can_get_the_datatype_for_DocumentType_BankAcStatement";
			reportUpdate(testName);
			headers = generateHeader();
			String datatype = "DocumentType";
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/data/" + datatype);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			String jsonAsString = response.asString();
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonAsString.contains((String) jsonsuitetestSuccessData.get("documenttype4")), true);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test()
	public void TC005_Superadmin_Verify_if_user_can_get_the_datatype_for_DocumentType_Other() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC005_Superadmin_Verify_if_user_can_get_the_datatype_for_DocumentType_Other", "id");
		try {
			String testName = "TC005_Superadmin_Verify_if_user_can_get_the_datatype_for_DocumentType_Other";
			reportUpdate(testName);
			headers = generateHeader();
			String datatype = "DocumentType";
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/data/" + datatype);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			String jsonAsString = response.asString();
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonAsString.contains((String) jsonsuitetestSuccessData.get("documenttype5")), true);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test()
	public void TC006_Superadmin_Verify_if_user_can_get_the_datatype_for_DocumentType_Static() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC006_Superadmin_Verify_if_user_can_get_the_datatype_for_DocumentType_Static", "id");
		try {
			String testName = "TC006_Superadmin_Verify_if_user_can_get_the_datatype_for_DocumentType_Static";
			reportUpdate(testName);
			headers = generateHeader();
			String datatype = "DocumentType";
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/data/" + datatype);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			String jsonAsString = response.asString();
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonAsString.contains((String) jsonsuitetestSuccessData.get("documenttype6")), true);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

}