package com.auz.restassured.tests.group.testcases;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.SupportedUtils.RandomJsonGenerator;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class Post_AddGroupUserToCheckDBConnection extends RestAssuredBase {

	public static HashMap<String, String> headers;
	public static String payLoadBody;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("suiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	String name = RandomJsonGenerator.randomGroupName();
	public static String createdid1 = "";
	public static String createdid2 = "";
	public static String createdid3 = "";
	public static String createdid4 = "";

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Post request to create/update/get/delete new group";
			testSuiteDescription = "Validating in passing the request to create/update/get/delete roles";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Superadmin_Verify_if_user_can_add_a_new_group_with_type_department() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_if_user_can_add_a_new_group_with_type_department", "id");
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("name", name);
		jsonObject.put("type", "Department");
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_GROUP_DIR + "addGroup.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC001_Superadmin_Verify_if_user_can_add_a_new_group_with_type_department";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_GROUP_DIR + "addGroup.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/group/");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(15, 47);
			System.out.println("CreatedID: " + id);
			createdid1 = id;
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains(name));
			Assert.assertEquals(jsonsuitetestSuccessData.get("created_response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	public void TC002_Superadmin_Verify_if_user_can_find_the_group_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_if_user_can_find_the_group_by_ID", "id");
		try {
			String testName = "TC002_Superadmin_Verify_if_user_can_find_the_group_by_ID";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/group/" + createdid1);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(15, 47);
			System.out.println("CreatedID: " + id);
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains(createdid1));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC003_Superadmin_Verify_if_user_can_update_group_to_project() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC002_Superadmin_Verify_if_user_can_update_group_to_project", "id");
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("name", name);
		jsonObject.put("type", "Project");
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_GROUP_DIR + "updateGroup.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC003_Superadmin_Verify_if_user_can_update_group_to_project";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_GROUP_DIR + "updateGroup.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = patchWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/group/" + createdid1);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains("Project"));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC004_Superadmin_Verify_if_user_can_update_group_to_organization() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Superadmin_Verify_if_user_can_update_group_to_organization", "id");
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("name", name);
		jsonObject.put("type", "Organization");
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_GROUP_DIR + "updateGroup.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC004_Superadmin_Verify_if_user_can_update_group_to_organization";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_GROUP_DIR + "updateGroup.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = patchWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/group/" + createdid1);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains("Organization"));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 5)
	public void TC005_Superadmin_Verify_if_user_can_update_group_to_team() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC004_Superadmin_Verify_if_user_can_update_group_to_team", "id");
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("name", name);
		jsonObject.put("type", "Team");
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_GROUP_DIR + "updateGroup.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC005_Superadmin_Verify_if_user_can_update_group_to_team";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_GROUP_DIR + "updateGroup.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = patchWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/group/" + createdid1);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains("Team"));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 6)
	public void TC006_Superadmin_Verify_if_user_can_delete_group_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_if_user_can_delete_group_by_ID", "id");
		try {
			String testName = "TC006_Superadmin_Verify_if_user_can_delete_group_by_ID";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.CREATE_JOB_POST_URL + "/group/" + createdid1);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("deleted_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 7)
	public void TC007_Superadmin_Verify_if_user_can_add_a_new_group_with_type_project() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC002_Superadmin_Verify_if_user_can_add_a_new_group_with_type_project", "id");
		JSONObject jsonObject = new JSONObject();
		String name = RandomJsonGenerator.randomGroupName();
		jsonObject.put("name", name);
		jsonObject.put("type", "Project");
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_GROUP_DIR + "addGroup.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC007_Superadmin_Verify_if_user_can_add_a_new_group_with_type_project";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_GROUP_DIR + "addGroup.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/group/");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(15, 47);
			System.out.println("CreatedID: " + id);
			createdid2 = id;
			Assert.assertEquals(true, jsonAsString.contains(name));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 8)
	public void TC008_Superadmin_Verify_if_user_can_delete_group_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_if_user_can_delete_group_by_ID", "id");
		try {
			String testName = "TC008_Superadmin_Verify_if_user_can_delete_group_by_ID";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.CREATE_JOB_POST_URL + "/group/" + createdid2);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("deleted_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 9)
	public void TC009_Superadmin_Verify_if_user_can_add_a_new_group_with_type_organization() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Superadmin_Verify_if_user_can_add_a_new_group_with_type_organization", "id");
		JSONObject jsonObject = new JSONObject();
		String name = RandomJsonGenerator.randomGroupName();
		jsonObject.put("name", name);
		jsonObject.put("type", "Organization");
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_GROUP_DIR + "addGroup.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC009_Superadmin_Verify_if_user_can_add_a_new_group_with_type_organization";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_GROUP_DIR + "addGroup.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/group/");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(15, 47);
			System.out.println("CreatedID: " + id);
			createdid3 = id;
			Assert.assertEquals(true, jsonAsString.contains(name));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 10)
	public void TC0010_Superadmin_Verify_if_user_can_delete_group_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_if_user_can_delete_group_by_ID", "id");
		try {
			String testName = "TC0010_Superadmin_Verify_if_user_can_delete_group_by_ID";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.CREATE_JOB_POST_URL + "/group/" + createdid3);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("deleted_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 11)
	public void TC0011_Superadmin_Verify_if_user_can_add_a_new_group_with_type_team() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC004_Superadmin_Verify_if_user_can_add_a_new_group_with_type_team", "id");
		JSONObject jsonObject = new JSONObject();
		String name = RandomJsonGenerator.randomGroupName();
		jsonObject.put("name", name);
		jsonObject.put("type", "Team");
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_GROUP_DIR + "addGroup.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC0011_Superadmin_Verify_if_user_can_add_a_new_group_with_type_team";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_GROUP_DIR + "addGroup.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/group/");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(15, 47);
			System.out.println("CreatedID: " + id);
			createdid4 = id;
			Assert.assertEquals(true, jsonAsString.contains(name));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 12)
	public void TC0012_Superadmin_Verify_if_user_can_delete_group_by_ID() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_if_user_can_delete_group_by_ID", "id");
		try {
			String testName = "TC0012_Superadmin_Verify_if_user_can_delete_group_by_ID";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.CREATE_JOB_POST_URL + "/group/" + createdid4);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("deleted_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 13)
	public void TC0013_Superadmin_Verify_if_user_can_get_all_the_groups_of_organization() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_if_user_can_get_all_the_groups_of_organization", "id");
		try {
			String testName = "TC0013_Superadmin_Verify_if_user_can_get_all_the_groups_of_organization";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/groups");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains((String) jsonsuitetestSuccessData.get("type")));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	public String AddDepartmentGroup() throws IOException {
		String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_GROUP_DIR + "addGroup.json";
		payLoadBody = JsonComponent.readTextFile(filePath);
		headers = generateHeader();
		Response response = postWithHeaderAndForm(headers, payLoadBody, Constants.CREATE_JOB_POST_URL + "/group/");
		System.out.println(response);
		System.out.println("Status code: " + response.statusCode());
		String jsonAsString = response.asString();
		String id = jsonAsString.substring(9, 41);
		System.out.println("ID: " + id);
		System.out.println("Response: " + jsonAsString);
		System.out.println("Body: " + response.getBody());
		return id;
	}

}
