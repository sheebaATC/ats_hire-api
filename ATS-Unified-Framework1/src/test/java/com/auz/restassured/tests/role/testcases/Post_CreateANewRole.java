package com.auz.restassured.tests.role.testcases;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.SupportedUtils.RandomJsonGenerator;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class Post_CreateANewRole extends RestAssuredBase {
	public static HashMap<String, String> headers;
	public static String payLoadBody;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("suiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	public static String createdid = "";

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Post Request for create/update/get/delete roles ";
			testSuiteDescription = "Validating the request for create/update/get/delete roles";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")

	public static void addRoleJsonFile() {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("name", RandomJsonGenerator.randomRoleName());
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_ROLE_DIR + "NewRole.json");
			file.write(jsonObject.toJSONString());
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Superadmin_Verify_user_is_able_to_create_a_new_role() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_user_is_able_to_create_a_new_role", "id");
		JSONObject jsonObject = new JSONObject();
		String name = RandomJsonGenerator.randomRoleName();
		jsonObject.put("name", name);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_ROLE_DIR + "NewRole.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC001_Superadmin_Verify_user_is_able_to_create_a_new_role";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_ROLE_DIR + "NewRole.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			String organizationID = "02c3eda51b0146e1beecdcb52e2bfce8";
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/role/" + organizationID);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(15, 47);
			System.out.println("CreatedID: " + id);
			createdid = id;
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains(name));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	public void TC002_Superadmin_Verify_if_user_is_able_to_get_a_role() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault("TC001_Superadmin_Verify_if_user_is_able_to_get_a_role",
				"id");
		try {
			String testName = "TC002_Superadmin_Verify_if_user_is_able_to_get_a_role";
			reportUpdate(testName);
			headers = generateHeader();
			String orgID = "02c3eda51b0146e1beecdcb52e2bfce8";
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/role/" + createdid + "/" + orgID);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC003_Superadmin_Verify_if_user_is_able_to_get_list_of_permissions_for_role() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_if_user_is_able_to_get_list_of_permissions_for_role", "id");
		try {
			String testName = "TC003_Superadmin_Verify_if_user_is_able_to_get_list_of_permissions_for_role";
			reportUpdate(testName);
			headers = generateHeader();
			String createdid = "2acb67711fa8433f9124f079117ce0cc";
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/role/" + createdid + "/permissions");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC004_Superadmin_Verify_if_you_are_able_to_update_a_role() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_if_you_are_able_to_update_a_role", "id");
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("name", RandomJsonGenerator.randomRoleNameUpdate());
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_ROLE_DIR + "updateRole.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC004_Superadmin_Verify_if_you_are_able_to_update_a_role";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_ROLE_DIR + "updateRole.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = patchWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/role/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 5)
	public void TC005_Superadmin_Verify_if_you_are_able_to_update_permission_for_role() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_if_you_are_able_to_update_permission_for_role", "id");
		try {
			String testName = "TC005_Superadmin_Verify_if_you_are_able_to_update_permission_for_role";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_ROLE_DIR + "updatePermission.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = patchWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/role/" + createdid + "/permissions");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 6)
	public void TC006_Superadmin_Verify_if_user_is_able_to_get_all_roles() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_if_user_is_able_to_get_all_roles", "id");
		try {
			String testName = "TC006_Superadmin_Verify_if_user_is_able_to_get_all_roles";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getRequestWithQueryParams(headers, "organizationId", "02c3eda51b0146e1beecdcb52e2bfce8",
					Constants.CREATE_JOB_POST_URL + "/roles");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 7)
	public void TC007_Superadmin_Verify_if_you_are_able_to_delete_a_role() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_if_you_are_able_to_delete_a_role", "id");
		try {
			String testName = "TC007_Superadmin_Verify_if_you_are_able_to_delete_a_role";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.CREATE_JOB_POST_URL + "/role/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("deleted_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 8)
	public void TC008_Superadmin_Verify_if_user_is_able_to_see_manage_tags_permission_for_recruiter_role() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC008_Superadmin_Verify_if_user_is_able_to_see_manage_tags_permission_for_recruiter_role", "id");
		try {
			String testName = "TC008_Superadmin_Verify_if_user_is_able_to_see_manage_tags_permission_for_recruiter_role";
			reportUpdate(testName);
			headers = generateHeader();
			String recruiterid = "012b18c05406467782aca76f2be6ba59";
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/role/" + recruiterid + "/permissions");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, (jsonAsString.contains("manage_tags")));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 9)
	public void TC009_Superadmin_Verify_if_user_is_able_to_see_view_all_tags_permission_for_recruiter_role() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC009_Superadmin_Verify_if_user_is_able_to_see_view_all_tags_permission_for_recruiter_role", "id");
		try {
			String testName = "TC009_Superadmin_Verify_if_user_is_able_to_see_view_all_tags_permission_for_recruiter_role";
			reportUpdate(testName);
			headers = generateHeader();
			String recruiterid = "012b18c05406467782aca76f2be6ba59";
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/role/" + recruiterid + "/permissions");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, (jsonAsString.contains("view_all_tags")));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 10)
	public void TC0010_Superadmin_Verify_if_user_is_able_to_see_view_tag_permission_for_recruiter_role() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC0010_Superadmin_Verify_if_user_is_able_to_see_view_tags_permission_for_recruiter_role", "id");
		try {
			String testName = "TC0010_Superadmin_Verify_if_user_is_able_to_see_view_tags_permission_for_recruiter_role";
			reportUpdate(testName);
			headers = generateHeader();
			String recruiterid = "012b18c05406467782aca76f2be6ba59";
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/role/" + recruiterid + "/permissions");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, (jsonAsString.contains("view_tag")));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

}
