package com.auz.restassured.tests.zoomConference.testcases;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.SupportedUtils.RandomJsonGenerator;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class PostPutGetDelete_ZoomConference extends RestAssuredBase {
	public static HashMap<String, String> headers;
	public static String payLoadBody;

	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("suiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	public static String createdid = "";
	public static String createdid2 = "";
	public static String name = "";
	public static String webinarname = "";
	public static String orgID = "f487380b79b94f469e128d16b7923b30";

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Request from start of creating , updating ,getting and deleting the zoom meeting";
			testSuiteDescription = "Validating in creating , updating ,getting and deleting the zoom meeting";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Superadmin_Verify_user_is_able_to_post_zoom_integration() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_user_is_able_to_post_zoom_integration", "id");
		try {
			String testName = "TC001_Superadmin_Verify_user_is_able_to_post_zoom_integration";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_ZOOM_DIR + "tokeninfo.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeaderforMeetings();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.INTEGRATION_QA_URL + "/integrations/zoom/token-info");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(134, 156);
			String rcode = String.valueOf(responsecode);
			System.out.println("created id: " + id);
//			MongoDBConnectionManager db = new MongoDBConnectionManager();
//			BasicDBObject searchQuery = new BasicDBObject();
//			searchQuery.put("_id", orgID);
//			 Object mongoDatabaseQA = db.MongoDatabaseQAOrgID(searchQuery);
//			 String DBvalue = mongoDatabaseQA.toString(); 
//			Assert.assertEquals(true, (DBvalue.contains(orgID)));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	public void TC002_Superadmin_Verify_user_is_able_to_enable_zoom_integration() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC002_Superadmin_Verify_user_is_able_to_enable_zoom_integration", "id");
		try {
			String testName = "TC002_Superadmin_Verify_user_is_able_to_enable_zoom_integrationss";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_ZOOM_DIR + "enablezoom.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeaderforMeetings();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.INTEGRATION_QA_URL + "/integrations/zoom/enable");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(6, 21);
			System.out.println("createdid: " + id);
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains("\"enabled\": true"));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC003_Superadmin_Verify_if_you_user_is_able_to_find_if_zoom_is_enabled() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Superadmin_Verify_if_you_user_is_able_to_find_if_zoom_is_enabled", "id");
		try {
			String testName = "TC003_Superadmin_Verify_if_you_user_is_able_to_find_if_zoom_is_enabled";
			reportUpdate(testName);
			headers = generateHeaderforMeetings();
			Response response = getWithHeader(headers, Constants.INTEGRATION_QA_URL + "/integrations/zoom/is-enabled");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(6, 21);
			System.out.println("createdid: " + id);
			Assert.assertEquals(true, jsonAsString.contains("\"enabled\": true"));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 18)
	public void TC004_Superadmin_Verify_user_is_able_to_disable_zoom_integration() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC004_Superadmin_Verify_user_is_able_to_disable_zoom_integration", "id");
		try {
			String testName = "TC004_Superadmin_Verify_user_is_able_to_disable_zoom_integration";
			reportUpdate(testName);
			headers = generateHeaderforMeetings();
			Response response = postWithHeaderParam(headers,
					Constants.INTEGRATION_QA_URL + "/integrations/zoom/disable");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			String id = jsonAsString.substring(6, 22);
			System.out.println("createdid: " + id);
			Assert.assertEquals(true, jsonAsString.contains("\"enabled\": false"));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC005_Superadmin_Verify_user_is_able_to_post_zoom_meetings() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC005_Superadmin_Verify_user_is_able_to_post_zoom_meetings", "id");
		JSONObject js = new JSONObject();
		name = RandomJsonGenerator.randomZoomname();
		js.put("clientId", "vbadLvcYC86ZY5iavYJzkD5MGABhYHp5");
		js.put("eventId", "ee235766841c4cb285ecc526f3fc1338");
		js.put("configName", "aapRG6OqG4m2GUd5");
		js.put("name", name);
		js.put("start", "2020-06-26T17:00:00Z");
		js.put("end", "2020-06-26T17:00:00Z");
		js.put("description", "New Meeting");
		js.put("timezone", "America/Chicago");
		js.put("firstName", "Rajesh");
		js.put("lastName", "Soni");
		js.put("email", "rajesh@auzmor.com");
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_ZOOM_DIR + "addMeeting.json");
			file.write(js.toJSONString());
			file.close();
			String testName = "TC005_Superadmin_Verify_user_is_able_to_post_zoom_meetings";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_ZOOM_DIR + "addMeeting.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeaderforMeetings();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.INTEGRATION_QA_URL + "/integrations/zoom/meetings");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(52, 63);
			String rcode = String.valueOf(responsecode);
			System.out.println("created id: " + id);
			createdid = id;
			Assert.assertEquals(true, jsonAsString.contains(name));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 5)
	public void TC006_Superadmin_Verify_if_you_user_is_able_to_get_zoom_meetings() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC006_Superadmin_Verify_if_you_user_is_able_to_get_zoom_meetings", "id");
		try {
			String testName = "TC006_Superadmin_Verify_if_you_user_is_able_to_get_zoom_meetings";
			reportUpdate(testName);
			headers = generateHeaderforMeetings();
			Response response = getWithHeader(headers, Constants.INTEGRATION_QA_URL + "/integrations/zoom/meetings");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			// Assert.assertEquals(true, jsonAsString.contains(name));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 6)
	public void TC007_Superadmin_Verify_if_you_user_is_able_to_get_zoom_meeting_by_id() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC007_Superadmin_Verify_if_you_user_is_able_to_get_zoom_meeting_by_id", "id");
		try {
			String testName = "TC007_Superadmin_Verify_if_you_user_is_able_to_get_zoom_meeting_by_id";
			reportUpdate(testName);
			headers = generateHeaderforMeetings();
			Response response = getWithHeader(headers,
					Constants.INTEGRATION_QA_URL + "/integrations/zoom/meetings/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			// Assert.assertEquals(true, jsonAsString.contains(name));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 7)
	public void TC008_Superadmin_Verify_if_you_user_is_able_to_update_zoom_meeting_by_id() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC008_Superadmin_Verify_if_you_user_is_able_to_update_zoom_meeting_by_id", "id");
		JSONObject js = new JSONObject();
		String name = RandomJsonGenerator.randomZoomname();
		js.put("clientId", "vbadLvcYC86ZY5iavYJzkD5MGABhYHp5");
		js.put("configName", "aapRG6OqG4m2GUd5");
		js.put("name", name);
		js.put("start", "2020-06-04T07:00:00Z");
		js.put("end", "2020-06-04T07:00:00Z");
		js.put("description", "Updated Meeting");
		js.put("timezone", "America/Chicago");
		js.put("firstName", "Rajesh");
		js.put("lastName", "Soni");
		js.put("email", "rajesh@auzmor.com");
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_ZOOM_DIR + "updateMeeting.json");
			file.write(js.toJSONString());
			file.close();
			String testName = "TC008_Superadmin_Verify_if_you_user_is_able_to_update_zoom_meeting_by_id";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_ZOOM_DIR + "updateMeeting.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeaderforMeetings();
			Response response = putWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.INTEGRATION_QA_URL + "/integrations/zoom/meetings/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 9)
	public void TC009_Superadmin_Verify_if_you_user_is_able_to_delete_zoom_meeting_by_id() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC009_Superadmin_Verify_if_you_user_is_able_to_delete_zoom_meeting_by_id", "id");
		try {
			String testName = "TC009_Superadmin_Verify_if_you_user_is_able_to_delete_zoom_meeting_by_id";
			reportUpdate(testName);
			headers = generateHeaderforMeetings();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.INTEGRATION_QA_URL + "/integrations/zoom/meetings/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("deleted_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 8)
	public void TC0010_Superadmin_Verify_if_you_user_is_able_to_end_zoom_meeting_by_id() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0010_Superadmin_Verify_if_you_user_is_able_to_end_zoom_meeting_by_id", "id");
		try {
			String testName = "TC0010_Superadmin_Verify_if_you_user_is_able_to_end_zoom_meeting_by_id";
			reportUpdate(testName);
			headers = generateHeaderforMeetings();
			Response response = putWithHeader(headers,
					Constants.INTEGRATION_QA_URL + "/integrations/zoom/meetings/" + createdid + "/end");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 10)
	public void TC0011_Superadmin_Verify_if_you_user_is_able_to_post_zoom_webinars() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0011_Superadmin_Verify_if_you_user_is_able_to_post_zoom_webinars", "id");
		JSONObject js = new JSONObject();
		webinarname = RandomJsonGenerator.randomWebinarname();
		js.put("name", webinarname);
		js.put("start", "2020-06-26T17:00:00Z");
		js.put("end", "2020-06-26T17:00:00Z");
		js.put("description", "New Webinar");
		js.put("timezone", "America/Chicago");
		js.put("firstName", "Rajesh");
		js.put("lastName", "Soni");
		js.put("email", "rajesh@auzmor.com");
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_ZOOM_DIR + "addzoomwebinar.json");
			file.write(js.toJSONString());
			file.close();
			String testName = "TC0011_Superadmin_Verify_if_you_user_is_able_to_post_zoom_webinar";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_ZOOM_DIR + "addzoomwebinar.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeaderforMeetings();
			Response response = postWithHeaderAndJsonBody(headers, payLoadBody,
					Constants.INTEGRATION_QA_URL + "/integrations/zoom/webinars");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(52, 63);
			String rcode = String.valueOf(responsecode);
			System.out.println("created id: " + id);
			createdid2 = id;
			Assert.assertEquals(true, jsonAsString.contains(webinarname));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 11)
	public void TC0012_Superadmin_Verify_if_you_user_is_able_to_get_zoom_webinars() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0012_Superadmin_Verify_if_you_user_is_able_to_get_zoom_webinars", "id");
		try {
			String testName = "TC0012_Superadmin_Verify_if_you_user_is_able_to_get_zoom_webinars";
			reportUpdate(testName);
			headers = generateHeaderforMeetings();
			Response response = getWithHeader(headers, Constants.INTEGRATION_QA_URL + "/integrations/zoom/webinars");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 12)
	public void TC0013_Superadmin_Verify_if_you_user_is_able_to_get_zoom_webinar_by_id() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0013_Superadmin_Verify_if_you_user_is_able_to_get_zoom_webinar_by_id", "id");
		try {
			String testName = "TC0013_Superadmin_Verify_if_you_user_is_able_to_get_zoom_webinar_by_id";
			reportUpdate(testName);
			headers = generateHeaderforMeetings();
			Response response = getWithHeader(headers,
					Constants.INTEGRATION_QA_URL + "/integrations/zoom/webinars/" + createdid2);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains(webinarname));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 13)
	public void TC0014_Superadmin_Verify_if_you_user_is_able_to_update_zoom_webinar_by_id() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0014_Superadmin_Verify_if_you_user_is_able_to_update_zoom_webinar_by_id", "id");
		JSONObject js = new JSONObject();
		webinarname = RandomJsonGenerator.randomWebinarname();
		js.put("name", webinarname);
		js.put("start", "2020-06-04T07:00:00Z");
		js.put("end", "2020-06-04T07:00:00Z");
		js.put("description", "Updated Webinar");
		js.put("timezone", "America/Chicago");
		js.put("firstName", "Rajesh");
		js.put("lastName", "Soni");
		js.put("email", "rajesh@auzmor.com");
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_ZOOM_DIR + "updateZoomwebinar.json");
			file.write(js.toJSONString());
			file.close();
			String testName = "TC0014_Superadmin_Verify_if_you_user_is_able_to_update_zoom_webinar_by_id";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_ZOOM_DIR + "updateZoomwebinar.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeaderforMeetings();
			Response response = putWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.INTEGRATION_QA_URL + "/integrations/zoom/webinars/" + createdid2);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 17)
	public void TC0015_Superadmin_Verify_if_you_user_is_able_to_delete_zoom_webinar_by_id() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0015_Superadmin_Verify_if_you_user_is_able_to_delete_zoom_webinar_by_id", "id");
		try {
			String testName = "TC0015_Superadmin_Verify_if_you_user_is_able_to_delete_zoom_webinar_by_id";
			reportUpdate(testName);
			headers = generateHeaderforMeetings();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.INTEGRATION_QA_URL + "/integrations/zoom/webinars/" + createdid2);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("deleted_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 16)
	public void TC0016_Superadmin_Verify_if_you_user_is_able_to_end_zoom_webinar_by_id() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0016_Superadmin_Verify_if_you_user_is_able_to_end_zoom_webinar_by_id", "id");
		try {
			String testName = "TC0016_Superadmin_Verify_if_you_user_is_able_to_end_zoom_webinar_by_id";
			reportUpdate(testName);
			headers = generateHeaderforMeetings();
			Response response = putWithHeader(headers,
					Constants.INTEGRATION_QA_URL + "/integrations/zoom/webinars/" + createdid2 + "/end");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 14)
	public void TC0017_Superadmin_Verify_if_you_user_is_able_to_get_zoom_webinars_attendees() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0017_Superadmin_Verify_if_you_user_is_able_to_get_zoom_webinars_attendees", "id");
		try {
			String testName = "TC0017_Superadmin_Verify_if_you_user_is_able_to_get_zoom_webinars_attendees";
			reportUpdate(testName);
			headers = generateHeaderforMeetings();
			String createdid2 = "81994107179";
			Response response = getWithHeader(headers,
					Constants.INTEGRATION_QA_URL + "/integrations/zoom/webinars/" + createdid2 + "/attendees");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			Response response1 = getWithHeader(headers,
					Constants.INTEGRATION_QA_URL + "/integrations/zoom/webinars/" + createdid2);
			logResponseInReport(response1);
			System.out.println("Status code: " + response1.statusCode());
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 15)
	public void TC0018_Superadmin_Verify_if_you_user_is_able_to_get_zoom_webinar_b() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0013_Superadmin_Verify_if_you_user_is_able_to_get_zoom_webinar_by_id", "id");
		try {
			String testName = "TC0013_Superadmin_Verify_if_you_user_is_able_to_get_zoom_webinar_by_id";
			reportUpdate(testName);
			headers = generateHeaderforMeetings();

			Response response = getWithHeader(headers,
					Constants.INTEGRATION_QA_URL + "/integrations/zoom/webinars/" + createdid2);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

}
