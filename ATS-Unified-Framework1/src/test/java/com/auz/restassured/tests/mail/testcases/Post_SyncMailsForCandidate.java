package com.auz.restassured.tests.mail.testcases;

import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class Post_SyncMailsForCandidate extends RestAssuredBase {
	public static HashMap<String, String> headers;
	public static String payLoadBody;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("suiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Post Request for sync mails for candidate ";
			testSuiteDescription = "Validating the request for syncing mails for candidate ";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")

	@Test()
	public void TC001_Verify_user_is_able_to_sync_mails_for_candidate() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault("TC001_Verify_user_is_able_to_sync_mails_for_candidate",
				"id");
		try {
			String testName = "TC001_Verify_user_is_able_to_sync_mails_for_candidate";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_MAIL_DIR + "SyncMail.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			String candidateid = "11761cd3b55b4e54b60020f039387f45";
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/candidate/" + candidateid + "/syncMessages");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	public String SyncMail() throws IOException {
		String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_TAG_DIR + "CreateTag.json";
		payLoadBody = JsonComponent.readTextFile(filePath);
		headers = generateHeader();
		Response response = postWithHeaderAndForm(headers, payLoadBody,
				Constants.JSONOBJECT_MAIL_DIR + "SyncMail.json");
		System.out.println(response);
		System.out.println("Status code: " + response.statusCode());
		String jsonAsString = response.asString();
		String id = jsonAsString.substring(9, 41);
		System.out.println("ID: " + id);
		System.out.println("Response: " + jsonAsString);
		System.out.println("Body: " + response.getBody());
		return id;

	}

}
