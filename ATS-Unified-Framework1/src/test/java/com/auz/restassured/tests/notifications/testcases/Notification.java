package com.auz.restassured.tests.notifications.testcases;

import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.SupportedUtils.MongoDBConnectionManager;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class Notification extends RestAssuredBase {
	public static HashMap<String, String> headers;
	public static String payLoadBody;

	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("suiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	MongoDBConnectionManager db = new MongoDBConnectionManager();
	public static String orgID = "b665ccd67abe4e8dba9d7ffcae488093";
	public static String createdid = "";
	public static String createdid2 = "";

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Test cases for notifications API";
			testSuiteDescription = "Validating in creating , updating, getting and deleting the notification";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Superadmin_Verify_user_is_able_to_get_the_unread_notifications_count() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_user_is_able_to_get_the_unread_notifications_count", "id");
		try {
			String testName = "TC001_Superadmin_Verify_user_is_able_to_get_the_unread_notifications_count";
			reportUpdate(testName);
			headers = generateHeaderwithToken();
			Response response = getWithHeader(headers, Constants.NOTIFICATIONS_URL + "notifications/unread/count");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC002_Superadmin_Verify_user_is_able_to_get_notifications_by_id() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC002_Superadmin_Verify_user_is_able_to_get_notifications_by_id", "id");
		try {
			String testName = "TC002_Superadmin_Verify_user_is_able_to_get_notifications_by_id";
			reportUpdate(testName);
			headers = generateHeaderwithToken();
			Response response = getWithHeader(headers, Constants.NOTIFICATIONS_URL + "notifications/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC003_Superadmin_Verify_user_is_able_to_put_notifications_by_id() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Superadmin_Verify_user_is_able_to_put_notifications_by_id", "id");
		try {
			String testName = "TC003_Superadmin_Verify_user_is_able_to_put_notifications_by_id";
			reportUpdate(testName);
			headers = generateHeaderwithToken();
			Response response = putWithHeader(headers, Constants.NOTIFICATIONS_URL + "notifications/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 5)
	public void TC004_Superadmin_Verify_user_is_able_to_delete_notifications_by_id() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Superadmin_Verify_user_is_able_to_update_notifications_by_id", "id");
		try {
			String testName = "TC003_Superadmin_Verify_user_is_able_to_update_notifications_by_id";
			reportUpdate(testName);
			headers = generateHeaderwithToken();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.NOTIFICATIONS_URL + "notifications/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("deleted_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	public void TC005_Superadmin_Verify_user_is_able_to_post_notification() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC005_Superadmin_Verify_user_is_able_to_post_notification", "id");
		try {
			String testName = "TC005_Superadmin_Verify_user_is_able_to_post_notification";
			reportUpdate(testName);
			headers = generateHeaderwithToken();
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_NOTIFICATION_DIR
					+ "addNotification.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.NOTIFICATIONS_URL + "notifications");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(468, 492);
			String rcode = String.valueOf(responsecode);
			System.out.println("created id: " + id);
			createdid = id;
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 7)
	public void TC006_Superadmin_Verify_user_is_able_to_get_all_notifications() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC006_Superadmin_Verify_user_is_able_to_get_all_notifications", "id");
		try {
			String testName = "TC006_Superadmin_Verify_user_is_able_to_get_all_notifications";
			reportUpdate(testName);
			headers = generateHeaderwithToken();
			Response response = getWithHeader(headers, Constants.NOTIFICATIONS_URL + "notifications");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			// Assert.assertEquals(true, (jsonAsString.contains(SectionName)));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 8)
	public void TC007_Superadmin_Verify_user_is_able_to_put_all_notifications() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC007_Superadmin_Verify_user_is_able_to_put_all_notifications", "id");
		try {
			String testName = "TC007_Superadmin_Verify_user_is_able_to_put_all_notifications";
			reportUpdate(testName);
			headers = generateHeaderwithToken();
			Response response = putWithHeader(headers, Constants.NOTIFICATIONS_URL + "notifications/all");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 9)
	public void TC008_Superadmin_Verify_user_is_able_to_delete_all_notifications() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC008_Superadmin_Verify_user_is_able_to_delete_all_notifications", "id");
		try {
			String testName = "TC008_Superadmin_Verify_user_is_able_to_delete_all_notifications";
			reportUpdate(testName);
			headers = generateHeaderwithToken();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.NOTIFICATIONS_URL + "notifications/all");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("deleted_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 6)
	public void TC009_Superadmin_Verify_user_is_able_to_post_bulk_notification() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC009_Superadmin_Verify_user_is_able_to_post_bulk_notification", "id");
		try {
			String testName = "TC009_Superadmin_Verify_user_is_able_to_post_bulk_notification";
			reportUpdate(testName);
			headers = generateHeaderwithToken();
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_NOTIFICATION_DIR
					+ "addBulkNotification.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.NOTIFICATIONS_URL + "notifications/bulk");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 10)
	public void TC0010_Superadmin_Verify_user_is_able_to_post_notification_user_settings_to_system()
			throws IOException {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC0010_Superadmin_Verify_user_is_able_to_post_notification_user_settings_to_system", "id");
		try {
			String testName = "TC0010_Superadmin_Verify_user_is_able_to_post_notification_user_settings_to_system";
			reportUpdate(testName);
			headers = generateHeaderwithToken();
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_NOTIFICATION_DIR
					+ "addNotificationusersettings.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.NOTIFICATIONS_URL + "user-settings");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 11)
	public void TC0011_Superadmin_Verify_user_is_able_to_get_notification_user_settings() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0011_Superadmin_Verify_user_is_able_to_get_notification_user_settings", "id");
		try {
			String testName = "TC0011_Superadmin_Verify_user_is_able_to_get_notification_user_settings";
			reportUpdate(testName);
			headers = generateHeaderwithToken();
			Response response = getWithHeader(headers, Constants.NOTIFICATIONS_URL + "user-settings");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			// Assert.assertEquals(true, (jsonAsString.contains(SectionName)));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 12)
	public void TC0012_Superadmin_Verify_user_is_able_to_post_notification_engine_serialize() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0012_Superadmin_Verify_user_is_able_to_post_notification_engine_serialize", "id");
		try {
			String testName = "TC0012_Superadmin_Verify_user_is_able_to_post_notification_engine_serialize";
			reportUpdate(testName);
			headers = generateHeaderwithToken();
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_NOTIFICATION_DIR
					+ "PostNotificationEnginerSerialize.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.NOTIFICATIONS_URL + "notification-engine/serialize");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(468, 492);
			String rcode = String.valueOf(responsecode);
			System.out.println("created id: " + id);
			createdid = id;
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 13)
	public void TC0013_Superadmin_Verify_user_is_able_to_post_notification_engine() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0013_Superadmin_Verify_user_is_able_to_post_notification_engine", "id");
		try {
			String testName = "TC0013_Superadmin_Verify_user_is_able_to_post_notification_engine";
			reportUpdate(testName);
			headers = generateHeaderwithToken();
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_NOTIFICATION_DIR
					+ "PostNotificationEngine.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.NOTIFICATIONS_URL + "notification-engine");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			// String id = jsonAsString.substring(468,492);
			String rcode = String.valueOf(responsecode);
			// System.out.println("created id: "+id);
			// createdid = id ;
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 14)
	public void TC0014_Superadmin_Verify_user_is_able_to_get_all_the_notifications() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0014_Superadmin_Verify_user_is_able_to_get_all_the_notifications", "id");
		try {
			String testName = "TC0014_Superadmin_Verify_user_is_able_to_get_all_the_notifications";
			reportUpdate(testName);
			headers = generateHeaderwithToken();
			Response response = getWithHeader(headers, Constants.NOTIFICATIONS_URL + "notifications/all");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			// Assert.assertEquals(true, (jsonAsString.contains(SectionName)));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 15)
	public void TC0015_Superadmin_Verify_user_is_able_to_updates_all_the_notifications() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0015_Superadmin_Verify_user_is_able_to_updates_all_the_notifications", "id");
		try {
			String testName = "TC0015_Superadmin_Verify_user_is_able_to_updates_all_the_notifications";
			reportUpdate(testName);
			headers = generateHeaderwithToken();
			Response response = putWithHeader(headers, Constants.NOTIFICATIONS_URL + "notifications/all");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	// @Test(priority=16)
	public void TC0016_Superadmin_Verify_user_is_able_to_delete_all_the_notifications() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0016_Superadmin_Verify_user_is_able_to_delete_all_the_notifications", "id");
		try {
			String testName = "TC0016_Superadmin_Verify_user_is_able_to_delete_all_the_notifications";
			reportUpdate(testName);
			headers = generateHeaderwithToken();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.NOTIFICATIONS_URL + "notifications/all");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("deleted_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

}
