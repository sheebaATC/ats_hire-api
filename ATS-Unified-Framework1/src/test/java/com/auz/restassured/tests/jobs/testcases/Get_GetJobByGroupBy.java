package com.auz.restassured.tests.jobs.testcases;

import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class Get_GetJobByGroupBy extends RestAssuredBase {

	public static HashMap<String, String> headers;
	public static String payLoadBody;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("suiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Get Request to get the jobs through Group By";
			testSuiteDescription = "Validating in passing the request to get jobs through Group By";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")

	@Test()
	public void TC001_Superadmin_Verify_if_you_are_able_to_get_job_by_groupBy_Department_and_myJobs() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC001_Superadmin_Verify_if_you_are_able_to_get_job_by_groupBy_Department_and_myJobs", "id");
		try {
			String testName = "TC001_Superadmin_Verify_if_you_are_able_to_get_job_by_groupBy_Department_and_myJobs";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getRequestWithTwoQueryParams(headers, "myJobs", "true", "groupBy", "Department",
					Constants.CREATE_JOB_POST_URL + "/jobs/groupBy");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")

	@Test()
	public void TC002_Superadmin_Verify_if_you_are_able_to_get_job_by_groupBy_Department_and_myJobs_as_false() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC002_Superadmin_Verify_if_you_are_able_to_get_job_by_groupBy_Department_and_myJobs_as_false", "id");
		try {
			String testName = "TC002_Superadmin_Verify_if_you_are_able_to_get_job_by_groupBy_Department_and_myJobs_as_false";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getRequestWithTwoQueryParams(headers, "myJobs", "false", "groupBy", "Department",
					Constants.CREATE_JOB_POST_URL + "/jobs/groupBy");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")

	@Test()
	public void TC003_Superadmin_Verify_if_you_are_able_to_get_job_by_groupBy_Location_and_myJobs() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC003_Superadmin_Verify_if_you_are_able_to_get_job_by_groupBy_Location_and_myJobs", "id");
		try {
			String testName = "TC003_Superadmin_Verify_if_you_are_able_to_get_job_by_groupBy_Location_and_myJobs";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getRequestWithTwoQueryParams(headers, "myJobs", "true", "groupBy", "Location",
					Constants.CREATE_JOB_POST_URL + "/jobs/groupBy");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")

	@Test()
	public void TC004_Superadmin_Verify_if_you_are_able_to_get_job_by_groupBy_Location_and_myJobs_as_false() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC004_Superadmin_Verify_if_you_are_able_to_get_job_by_groupBy_Location_and_myJobs_as_false", "id");
		try {
			String testName = "TC004_Superadmin_Verify_if_you_are_able_to_get_job_by_groupBy_Location_and_myJobs_as_false";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getRequestWithTwoQueryParams(headers, "myJobs", "false", "groupBy", "Location",
					Constants.CREATE_JOB_POST_URL + "/jobs/groupBy");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

}
