package com.auz.restassured.tests.evaluation.testcases;

import java.io.FileWriter;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.SupportedUtils.Neo4jDBConnection;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class Post_EvaluationOfCandidate extends RestAssuredBase {
	public static HashMap<String, String> headers;
	public static String payLoadBody;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("suiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	public static String createdid = "";
	public static String candidateid = "756687352e8841a0afff760716ca4dfb";
	Neo4jDBConnection db = new Neo4jDBConnection();
	String comment = "";

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Post Request for evaluating candidates ";
			testSuiteDescription = "Validating the request for evaluating candidates";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Superadmin_Verify_request_evaluation_for_a_candidate() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_request_evaluation_for_a_candidate", "id");
		JSONObject jsonObject = new JSONObject();
		comment = "he is a good candidate";
		jsonObject.put("candidateId", candidateid);
		jsonObject.put("stageId", "9c4bcb81e71a477d8e86fdb46d464874");
		jsonObject.put("jobId", "134bacdc12d74794a83d0028121fb688");
		JSONArray jsonArray = new JSONArray();
		jsonArray.add("5b9d68cbe04746ba9b3e4b80ea8ae0a8");
		jsonObject.put("userIds", jsonArray);
		try {
			FileWriter file = new FileWriter(System.getProperty("user.dir") + Constants.JSONOBJECT_EVALUATION_DIR
					+ "postEvaluationCandidate.json");
			file.write(jsonObject.toJSONString());
			file.close();

			String testName = "TC001_Superadmin_Verify_post_request_to_evaluate_a_candidate";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_EVALUATION_DIR
					+ "postEvaluationCandidate.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/evaluation/request");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(38, 70);
			System.out.println("CreatedID: " + id);

//			String DBquery = "MATCH (evaluation {uuid :"+"'" +id + "'" +"}) RETURN evaluation, evaluation.comment";
//			String getquery = "evaluation.comment";
//			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
//			Assert.assertEquals(comment, databaseResult);
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains(candidateid));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	public void TC002_Superadmin_Verify_post_request_to_evaluate_a_candidate() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC002_Superadmin_Verify_post_request_to_evaluate_a_candidate", "id");
		JSONObject jsonObject = new JSONObject();
		comment = "he is a good candidate";
		jsonObject.put("evaluateeId", candidateid);
		jsonObject.put("score", "55");
		jsonObject.put("comment", comment);
		jsonObject.put("evaluationId", "09e10ceb34fc49e5a1b689c56c5d39e5");
		jsonObject.put("evaluatorId", "1a44b50f8dc24dff8433b46f16ffcd5b");
		try {
			FileWriter file = new FileWriter(System.getProperty("user.dir") + Constants.JSONOBJECT_EVALUATION_DIR
					+ "postEvaluateCandidate.json");
			file.write(jsonObject.toJSONString());
			file.close();

			String testName = "TC002_Superadmin_Verify_post_request_to_evaluate_a_candidate";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_EVALUATION_DIR
					+ "postEvaluateCandidate.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/evaluation");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(39, 71);
			System.out.println("CreatedID: " + id);
			createdid = id;
			String DBquery = "MATCH (evaluation {uuid :" + "'" + id + "'" + "}) RETURN evaluation, evaluation.comment";
			String getquery = "evaluation.comment";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals(comment, databaseResult);
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains(candidateid));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC003_Superadmin_Verify_to_get_request_to_evaluate_candidate() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Superadmin_Verify_to_get_request_to_evaluate_candidate", "id");
		try {
			String testName = "TC003_Superadmin_Verify_to_get_request_to_evaluate_candidate";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/evaluation/" + candidateid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			String DBquery = "MATCH (evaluation {uuid :" + "'" + createdid + "'"
					+ "}) RETURN evaluation, evaluation.comment";
			String getquery = "evaluation.comment";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals(comment, databaseResult);
			Assert.assertEquals(true, jsonAsString.contains(candidateid));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC004_Superadmin_Verify_to_update_request_to_evaluate_candidate() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC004_Superadmin_Verify_to_update_request_to_evaluate_candidate", "id");
		JSONObject jsonObject = new JSONObject();
		String score = "95";
		jsonObject.put("score", score);
		jsonObject.put("comment", "he is a good candidate");
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_ROLE_DIR + "updateEvaluationCandidate.json");
			file.write(jsonObject.toJSONString());
			file.close();
			String testName = "TC004_Superadmin_Verify_to_update_request_to_evaluate_candidate";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_ROLE_DIR
					+ "updateEvaluationCandidate.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = patchWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/evaluation/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			String DBquery = "MATCH (evaluation {uuid :" + "'" + createdid + "'"
					+ "}) RETURN evaluation, evaluation.score";
			String getquery = "evaluation.score";
			String databaseResult = db.databaseConnectiviy(DBquery, getquery);
			Assert.assertEquals(score, databaseResult);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

}
