package com.auz.restassured.tests.gotomeetingConference.testcases;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.json.simple.JSONObject;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.SupportedUtils.MongoDBConnectionManager;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;
import com.mongodb.BasicDBObject;

import io.restassured.response.Response;

public class QA_PostPutGetDelete_GoToMeetingConference extends RestAssuredBase {
	public static HashMap<String, String> headers;
	public static String payLoadBody;

	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("suiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	public static String createdid = "";
	MongoDBConnectionManager db = new MongoDBConnectionManager();
	public static String orgID = "b665ccd67abe4e8dba9d7ffcae488093";
	public static String createdByID = "1a44b50f8dc24dff8433b46f16ffcd5b";

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Request from start of creating , updating ,getting and deleting the Go To meeting";
			testSuiteDescription = "Validating in creating , updating ,getting and deleting the Go To meeting";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Superadmin_Verify_user_is_able_to_login_into_gotomeeting() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_user_is_able_to_login_into_gotomeeting_hire", "id");
		try {
			String testName = "TC001_Superadmin_Verify_user_is_able_to_login_into_gotomeeting";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_GOTOMEETING_DIR
					+ "logingotomeeting.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeaderforMeetings();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/gotomeeting/login");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(40, 126);
			String id1 = jsonAsString.substring(90, 126);
			String rcode = String.valueOf(responsecode);
			System.out.println("created id: " + id);
			System.out.println("Meeting id : " + id1);
			createdid = id1;
			ChromeDriver driver = new ChromeDriver();
			driver.get(id);
			driver.findElementById("emailAddress").sendKeys("sheeba@american-technology.net");
			driver.findElementById("next-button").click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElementById("password").sendKeys("Test@123");
			driver.findElementById("submit").click();
			driver.close();
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("_id", orgID);
			BasicDBObject fields = new BasicDBObject();
			fields.put("integrations.gotoMeeting.enabledBy", orgID);
			Object mongoDatabaseQA = db.MongoDatabaseStagingwithTwoQueries(searchQuery, "integrations", fields);
			String convertedToString = mongoDatabaseQA.toString();
			Assert.assertEquals(true, (convertedToString.contains(createdByID)));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	public void TC002_Superadmin_Verify_user_is_able_to_enable_gotomeeting_integration() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC002_Superadmin_Verify_user_is_able_to_enable_gotomeeting_integration_hire", "id");
		JSONObject js = new JSONObject();
		String Meetingid = createdid;
		js.put("id", Meetingid);
		try {
			FileWriter file = new FileWriter(
					System.getProperty("user.dir") + Constants.JSONOBJECT_GOTOMEETING_DIR + "enablegotomeeting.json");
			file.write(js.toJSONString());
			file.close();
			String testName = "TC002_Superadmin_Verify_user_is_able_to_enable_gotomeeting_integration";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_GOTOMEETING_DIR
					+ "enablegotomeeting.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeaderforMeetings();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/gotomeeting/enable");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(6, 21);
			String rcode = String.valueOf(responsecode);
			System.out.println("created id: " + id);
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("_id", orgID);
			BasicDBObject fields = new BasicDBObject();
			fields.put("integrations.gotoMeeting.enabled", "true");
			Object mongoDatabaseQA = db.MongoDatabaseStagingwithTwoQueries(searchQuery, "integrations", fields);
			String convertedToString = mongoDatabaseQA.toString();
			Assert.assertEquals(true, (convertedToString.contains(id)));
			Assert.assertEquals(true, jsonAsString.contains(id));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC003_Superadmin_Verify_if_you_user_is_able_to_get_gotomeeting_is_enabled() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Superadmin_Verify_if_you_user_is_able_to_get_gotomeeting_is_enabled_hire", "id");
		try {
			String testName = "TC003_Superadmin_Verify_if_you_user_is_able_to_get_gotomeeting_is_enabled";
			reportUpdate(testName);
			headers = generateHeaderforMeetings();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/gotomeeting/is-enabled");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(6, 21);
			String rcode = String.valueOf(responsecode);
			System.out.println("created id: " + id);
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("_id", orgID);
			BasicDBObject fields = new BasicDBObject();
			fields.put("integrations.gotoMeeting.enabled", "true");
			Object mongoDatabaseQA = db.MongoDatabaseStagingwithTwoQueries(searchQuery, "integrations", fields);
			String convertedToString = mongoDatabaseQA.toString();
			Assert.assertEquals(true, (convertedToString.contains(id)));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 6)
	public void TC004_Superadmin_Verify_user_is_able_to_disable_gotomeeting_integration() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC004_Superadmin_Verify_user_is_able_to_disable_gotomeeting_integration_hire", "id");
		try {
			String testName = "TC004_Superadmin_Verify_user_is_able_to_disable_gotomeeting_integration";
			reportUpdate(testName);
			headers = generateHeaderforMeetings();
			Response response = postWithHeaderParam(headers, Constants.CREATE_JOB_POST_URL + "/gotomeeting/disable");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(6, 22);
			String rcode = String.valueOf(responsecode);
			System.out.println("created id: " + id);
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("_id", orgID);
			BasicDBObject fields = new BasicDBObject();
			fields.put("integrations.gotoMeeting.enabled", "false");
			Object mongoDatabaseQA = db.MongoDatabaseStagingwithTwoQueries(searchQuery, "integrations", fields);
			String convertedToString = mongoDatabaseQA.toString();
			Assert.assertEquals(true, (convertedToString.contains(id)));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC005_Superadmin_Verify_user_is_able_to_post_gotomeeting() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC005_Superadmin_Verify_user_is_able_to_post_gotomeeting_hire", "id");
		try {
			String testName = "TC005_Superadmin_Verify_user_is_able_to_post_gotomeeting";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_GOTOMEETING_DIR
					+ "postMeeting.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeaderforMeetings();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/gotomeeting/meetings");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(96, 104);
			createdid = id;
			String rcode = String.valueOf(responsecode);
			System.out.println("created id: " + id);
			Assert.assertEquals(true, jsonAsString.contains(id));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 5)
	public void TC006_Superadmin_Verify_if_you_user_is_able_to_get_gotoMeeting_meetings() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC006_Superadmin_Verify_if_you_user_is_able_to_get_gotoMeeting_meetings_hire", "id");
		try {
			String testName = "TC006_Superadmin_Verify_if_you_user_is_able_to_get_gotoMeeting_meetings";
			reportUpdate(testName);
			headers = generateHeaderforMeetings();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/gotomeeting/meetings");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

}