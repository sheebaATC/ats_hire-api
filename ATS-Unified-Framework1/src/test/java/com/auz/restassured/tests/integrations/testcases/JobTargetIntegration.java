package com.auz.restassured.tests.integrations.testcases;

import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class JobTargetIntegration extends RestAssuredBase {

	public static HashMap<String, String> headers;
	public static String payLoadBody;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("suiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	public static String createdid = "";
	public static String jobid = "";

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Requeset for job target integrations";
			testSuiteDescription = "Validating in passing the request for job integrations";

		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Superadmin_Verify_if_user_can_enable_job_target_integrations() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_if_user_can_enable_job_target_integrations", "id");
		try {
			String testName = "TC001_Superadmin_Verify_if_user_can_enable_job_target_integrations";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = postWithHeaderParam(headers,
					Constants.CREATE_JOB_POST_URL + "/integrations/job-target/enable");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(476, 508);
			System.out.println("CreatedID: " + id);
			createdid = id;
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains("true"));
			Assert.assertEquals(jsonsuitetestSuccessData.get("created_response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	public void TC002_Superadmin_Verify_if_user_can_get_job_target_integration_enabled_jobs_is_true() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC002_Superadmin_Verify_if_user_can_get_job_target_integration_enabled_jobs_is_true", "id");
		try {
			String testName = "TC002_Superadmin_Verify_if_user_can_get_job_target_integration_enabled_jobs_is_true";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/integrations/job-target/is-enabled");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains("true"));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 11)
	public void TC003_Superadmin_Verify_if_user_can_disable_job_target_integrations() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Superadmin_Verify_if_user_can_disable_job_target_integrations", "id");
		try {
			String testName = "TC003_Superadmin_Verify_if_user_can_disable_job_target_integrations";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = postWithHeaderParam(headers,
					Constants.CREATE_JOB_POST_URL + "/integrations/job-target/disable");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(285, 317);
			System.out.println("CreatedID: " + id);
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains("false"));
			Assert.assertEquals(jsonsuitetestSuccessData.get("created_response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
			createdid = id;
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 12)
	public void TC004_Superadmin_Verify_if_user_can_get_job_target_integration_enabled_jobs_is_false() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC004_Superadmin_Verify_if_user_can_get_job_target_integration_enabled_jobs_is_false", "id");
		try {
			String testName = "TC004_Superadmin_Verify_if_user_can_get_job_target_integration_enabled_jobs_is_false";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/integrations/job-target/is-enabled");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains("false"));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC005_Superadmin_Verify_if_user_can_get_job_target_integrated_jobs() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC005_Superadmin_Verify_if_user_can_get_job_target_integrated_jobs", "id");
		try {
			String testName = "TC005_Superadmin_Verify_if_user_can_get_job_target_integrated_jobs";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/integrations/job-target/jobs");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(1121, 1153);
			System.out.println("CreatedID: " + id);
			jobid = id;
			Assert.assertEquals(true, jsonAsString.contains((String) jsonsuitetestSuccessData.get("orgID")));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC006_Superadmin_Verify_if_user_can_get_job_target_integration_job_by_id() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC006_Superadmin_Verify_if_user_can_get_job_target_integration_job_by_id", "id");
		try {
			String testName = "TC006_Superadmin_Verify_if_user_can_get_job_target_integration_job_by_id";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/integrations/job-target/jobs/" + jobid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains((String) jsonsuitetestSuccessData.get("orgID")));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 5)
	public void TC007_Superadmin_Verify_if_user_can_update_job_target_integration() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC007_Superadmin_Verify_if_user_can_update_job_target_integration", "id");
		try {
			String testName = "TC007_Superadmin_Verify_if_user_can_update_job_target_integration";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_INTEGRATION_DIR
					+ "updateIntegration.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			String id = "";
			Response response = putWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/integrations/job-target/jobs/" + jobid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			// Assert.assertEquals(true, jsonAsString.contains(interviewID));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 6)
	public void TC008_Superadmin_Verify_if_user_can_get_job_target_integration_company_info_for_job_id() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC008_Superadmin_Verify_if_user_can_get_job_target_integration_company_info_for_job_id", "id");
		try {
			String testName = "TC006_Superadmin_Verify_if_user_can_get_job_target_integration_job_by_id";
			reportUpdate(testName);
			headers = generateHeader();
			String id = "87bdb75ddd2f491da72fafaa733ecb1b";
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/integrations/job-target/jobs/" + jobid + "/company-info");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			// Assert.assertEquals(true,
			// jsonAsString.contains((String)jsonsuitetestSuccessData.get("orgId")));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 7)
	public void TC009_Superadmin_Verify_if_user_can_publish_job_target_integration_job() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC009_Superadmin_Verify_if_user_can_publish_job_target_integration_job", "id");
		try {
			String testName = "TC009_Superadmin_Verify_if_user_can_publish_job_target_integration_job";
			reportUpdate(testName);
			headers = generateHeader();
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_INTEGRATION_DIR
					+ "publishIntegration.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/integrations/job-target/jobs/" + jobid + "/publish");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, jsonAsString.contains("true"));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 8)
	public void TC0010_Superadmin_Verify_if_user_can_stop_job_target_integration_job() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0010_Superadmin_Verify_if_user_can_stop_job_target_integration_job", "id");
		try {
			String testName = "TC0010_Superadmin_Verify_if_user_can_stop_job_target_integration_job";
			reportUpdate(testName);
			headers = generateHeader();
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_INTEGRATION_DIR
					+ "stopIntegration.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/integrations/job-target/jobs/" + jobid + "/stop");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			// Assert.assertEquals(true, jsonAsString.contains("true"));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 9)
	public void TC0011_Superadmin_Verify_if_user_can_get_organizations_target_integration() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0011_Superadmin_Verify_if_user_can_get_organizations_target_integration", "id");
		try {
			String testName = "TC0011_Superadmin_Verify_if_user_can_get_organizations_target_integration";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/integrations/organisations");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains("integrations"));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 10)
	public void TC0012_Superadmin_Verify_if_user_can_get_organizations_target_integration_by_id() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC0012_Superadmin_Verify_if_user_can_get_organizations_target_integration_by_ids", "id");
		try {
			String testName = "TC0012_Superadmin_Verify_if_user_can_get_organizations_target_integration_by_id";
			reportUpdate(testName);
			headers = generateHeader();
			String orgID = "b665ccd67abe4e8dba9d7ffcae488093";
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/integrations/organisations/" + orgID);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			Assert.assertEquals(true, jsonAsString.contains(orgID));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

}
