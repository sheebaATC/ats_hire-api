package com.auz.restassured.tests.linkedin.testcases;

import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.SupportedUtils.MongoDBConnectionManager;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;
import com.mongodb.BasicDBObject;

import io.restassured.response.Response;

public class LinkedInIntegration extends RestAssuredBase {

	public static HashMap<String, String> headers;
	public static String payLoadBody;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("suiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	MongoDBConnectionManager db = new MongoDBConnectionManager();
	BasicDBObject searchQuery = new BasicDBObject();
	public static String orgID = "b665ccd67abe4e8dba9d7ffcae488093";
	// public static String createdByID = "1a44b50f8dc24dff8433b46f16ffcd5b";

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Request for LinkedIn integrations";
			testSuiteDescription = "Validating in passing the request for LinkedIn integrations";

		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Superadmin_Verify_if_user_can_enable_linked_in_integration() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_if_user_can_enable_linked_in_integration", "id");
		try {
			String testName = "TC001_Superadmin_Verify_if_user_can_enable_linked_in_integration";
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_INTEGRATION_DIR
					+ "enableLinkedInIntegration.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			reportUpdate(testName);
			headers = generateHeaderwithToken();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/integrations/linkedin/enable");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			searchQuery.put("_id", orgID);
			BasicDBObject fields = new BasicDBObject();
			fields.put("integrations.linkedin.enabled", orgID);
			Object mongoDatabaseQA = db.MongoDatabaseStagingwithTwoQueries(searchQuery, "integrations", fields);
			String convertedToString = mongoDatabaseQA.toString();
			Assert.assertEquals(true, convertedToString.contains("\"enabled\": true"));
			Assert.assertEquals(true, jsonAsString.contains("\"enabled\": true"));
			Assert.assertEquals(jsonsuitetestSuccessData.get("created_response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	public void TC002_Superadmin_Verify_if_user_can_get_linkedin_integration_enabled_is_true() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC002_Superadmin_Verify_if_user_can_get_linkedin_integration_enabled_is_true", "id");
		try {
			String testName = "TC002_Superadmin_Verify_if_user_can_get_linkedin_integration_enabled_is_true";
			reportUpdate(testName);
			headers = generateHeaderwithToken();
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/integrations/linkedin/is-enabled");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			searchQuery.put("_id", orgID);
			BasicDBObject fields = new BasicDBObject();
			fields.put("integrations.linkedin.enabled", orgID);
			Object mongoDatabaseQA = db.MongoDatabaseStagingwithTwoQueries(searchQuery, "integrations", fields);
			String convertedToString = mongoDatabaseQA.toString();
			Assert.assertEquals(true, convertedToString.contains("\"enabled\": true"));
			Assert.assertEquals(true, jsonAsString.contains("\"enabled\": true"));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC009_Superadmin_Verify_user_is_able_to_get_all_jobs_in_linkedin_feed() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC009_Superadmin_Verify_user_is_able_to_get_all_jobs_in_linkedin_feed", "id");
		try {
			String testName = "TC009_Superadmin_Verify_user_is_able_to_get_all_jobs_in_linkedin_feed";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/integrations/job-boards/jobs/all/linkedin-feed");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC003_Superadmin_Verify_if_user_can_disable_linkedin_integration() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Superadmin_Verify_if_user_can_disable_linkedin_integration", "id");
		try {
			String testName = "TC003_Superadmin_Verify_if_user_can_disable_linkedin_integration";
			reportUpdate(testName);
			headers = generateHeaderwithToken();
			Response response = postWithHeaderParam(headers,
					Constants.CREATE_JOB_POST_URL + "/integrations/linkedin/disable");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			searchQuery.put("_id", orgID);
			BasicDBObject fields = new BasicDBObject();
			fields.put("integrations.linkedin.enabled", orgID);
			Object mongoDatabaseQA = db.MongoDatabaseStagingwithTwoQueries(searchQuery, "integrations", fields);
			String convertedToString = mongoDatabaseQA.toString();
			Assert.assertEquals(true, convertedToString.contains("\"enabled\": false"));
			Assert.assertEquals(true, jsonAsString.contains("\"enabled\": false"));
			Assert.assertEquals(jsonsuitetestSuccessData.get("created_response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
			// createdid = id ;
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 5)
	public void TC004_Superadmin_Verify_if_user_can_get_linkedin_integration_enabled_is_false() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC004_Superadmin_Verify_if_user_can_get_linkedin_integration_enabled_is_false", "id");
		try {
			String testName = "TC004_Superadmin_Verify_if_user_can_get_linkedin_integration_enabled_is_false";
			reportUpdate(testName);
			headers = generateHeaderwithToken();
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/integrations/linkedin/is-enabled");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			searchQuery.put("_id", orgID);
			BasicDBObject fields = new BasicDBObject();
			fields.put("integrations.linkedin.enabled", orgID);
			Object mongoDatabaseQA = db.MongoDatabaseStagingwithTwoQueries(searchQuery, "integrations", fields);
			String convertedToString = mongoDatabaseQA.toString();
			Assert.assertEquals(true, convertedToString.contains("\"enabled\": false"));
			Assert.assertEquals(true, jsonAsString.contains("\"enabled\": false"));
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}

	}

	@SuppressWarnings("unchecked")
	@Test(priority = 6)
	public void TC006_Superadmin_Verify_user_is_able_to_get_all_jobs_in_linkedin_feed() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC009_Superadmin_Verify_user_is_able_to_get_all_jobs_in_linkedin_feed", "id");
		try {
			String testName = "TC009_Superadmin_Verify_user_is_able_to_get_all_jobs_in_linkedin_feed";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/integrations/job-boards/jobs/all/linkedin-feed");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

}
