package com.auz.restassured.tests.zoomConference.testcases;

import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.SupportedUtils.MongoDBConnectionManager;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;
import com.mongodb.BasicDBObject;

import io.restassured.response.Response;

public class QA_PostPutGetDelete_ZoomConference extends RestAssuredBase {
	public static HashMap<String, String> headers;
	public static String payLoadBody;

	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("suiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	MongoDBConnectionManager db = new MongoDBConnectionManager();
	public static String orgID = "b665ccd67abe4e8dba9d7ffcae488093";
	public static String createdByID = "1a44b50f8dc24dff8433b46f16ffcd5b";

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Request from start of creating , updating ,getting and deleting the zoom meeting";
			testSuiteDescription = "Validating in creating , updating ,getting and deleting the zoom meeting";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Superadmin_Verify_user_is_able_to_post_zoom_integration() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_user_is_able_to_post_zoom_integration_hire", "id");
		try {
			String testName = "TC001_Superadmin_Verify_user_is_able_to_post_zoom_integration";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_ZOOM_DIR + "tokeninfo.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/zoom/token-info");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(134, 156);
			String rcode = String.valueOf(responsecode);
			System.out.println("created id: " + id);
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("_id", orgID);
			BasicDBObject fields = new BasicDBObject();
			fields.put("integrations.zoom.enabledBy", orgID);
			Object mongoDatabaseQA = db.MongoDatabaseStagingwithTwoQueries(searchQuery, "integrations", fields);
			String convertedToString = mongoDatabaseQA.toString();
			Assert.assertEquals(true, (convertedToString.contains(createdByID)));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	public void TC002_Superadmin_Verify_user_is_able_to_enable_zoom_integration() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC002_Superadmin_Verify_user_is_able_to_enable_zoom_integration_hire", "id");
		try {
			String testName = "TC002_Superadmin_Verify_user_is_able_to_enable_zoom_integrationss";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_ZOOM_DIR + "enablezoom.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/zoom/enable");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(6, 21);
			System.out.println("createdid: " + id);
			String rcode = String.valueOf(responsecode);
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("_id", orgID);
			BasicDBObject fields = new BasicDBObject();
			fields.put("integrations.zoom.enabled", "true");
			Object mongoDatabaseQA = db.MongoDatabaseStagingwithTwoQueries(searchQuery, "integrations", fields);
			System.out.println("AAA: " + mongoDatabaseQA);
			String convertedToString = mongoDatabaseQA.toString();
			System.out.println("BBB: " + convertedToString);
			Assert.assertEquals(true, (convertedToString.contains(id)));
			Assert.assertEquals(true, (jsonAsString.contains("true")));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC003_Superadmin_Verify_if_you_user_is_able_to_find_if_zoom_is_enabled() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Superadmin_Verify_if_you_user_is_able_to_find_if_zoom_is_enabled_hire", "id");
		try {
			String testName = "TC003_Superadmin_Verify_if_you_user_is_able_to_find_if_zoom_is_enabled";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/zoom/is-enabled");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(6, 21);
			System.out.println("createdid: " + id);
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("_id", orgID);
			BasicDBObject fields = new BasicDBObject();
			fields.put("integrations.zoom.enabled", "true");
			Object mongoDatabaseQA = db.MongoDatabaseStagingwithTwoQueries(searchQuery, "integrations", fields);
			String convertedToString = mongoDatabaseQA.toString();
			Assert.assertEquals(true, (convertedToString.contains(id)));
			Assert.assertEquals(true, (jsonAsString.contains("true")));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test(priority = 4)
	public void TC004_Superadmin_Verify_user_is_able_to_disable_zoom_integration() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC004_Superadmin_Verify_user_is_able_to_disable_zoom_integration_hire", "id");
		try {
			String testName = "TC004_Superadmin_Verify_user_is_able_to_disable_zoom_integration";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = postWithHeaderParam(headers, Constants.CREATE_JOB_POST_URL + "/zoom/disable");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(6, 22);
			System.out.println("createdid: " + id);
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("_id", orgID);
			BasicDBObject fields = new BasicDBObject();
			fields.put("integrations.zoom.enabled", "false");
			Object mongoDatabaseQA = db.MongoDatabaseStagingwithTwoQueries(searchQuery, "integrations", fields);
			String convertedToString = mongoDatabaseQA.toString();
			Assert.assertEquals(true, (convertedToString.contains(id)));
			Assert.assertEquals(true, (jsonAsString.contains("false")));
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

}
