package com.auz.restassured.tests.dataAPIs.testcases;

import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class Get_FindtagByID_FieldType extends RestAssuredBase {

	public static HashMap<String, String> headers;
	public static String payLoadBody;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("suiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Get Request to get data by passing the datatype as Field Type";
			testSuiteDescription = "Validating in passing the request to get data by passing the datatype as Field Type";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test()
	public void TC001_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_Integer() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_Integer", "id");
		try {
			String testName = "TC001_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_Integer";
			reportUpdate(testName);
			headers = generateHeader();
			String datatype = "FieldType";
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/data/" + datatype);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			String jsonAsString = response.asString();
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonAsString.contains((String) jsonsuitetestSuccessData.get("FieldType1")), true);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test()
	public void TC002_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_String() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC002_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_String", "id");
		try {
			String testName = "TC002_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_String";
			reportUpdate(testName);
			headers = generateHeader();
			String datatype = "FieldType";
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/data/" + datatype);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			String jsonAsString = response.asString();
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonAsString.contains((String) jsonsuitetestSuccessData.get("FieldType2")), true);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test()
	public void TC003_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_Date() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_Date", "id");
		try {
			String testName = "TC003_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_Date";
			reportUpdate(testName);
			headers = generateHeader();
			String datatype = "FieldType";
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/data/" + datatype);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			String jsonAsString = response.asString();
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonAsString.contains((String) jsonsuitetestSuccessData.get("FieldType3")), true);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test()
	public void TC004_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_Boolean() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC004_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_Boolean", "id");
		try {
			String testName = "TC004_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_Boolean";
			reportUpdate(testName);
			headers = generateHeader();
			String datatype = "FieldType";
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/data/" + datatype);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			String jsonAsString = response.asString();
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonAsString.contains((String) jsonsuitetestSuccessData.get("FieldType4")), true);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test()
	public void TC005_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_File() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC005_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_File", "id");
		try {
			String testName = "TC005_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_File";
			reportUpdate(testName);
			headers = generateHeader();
			String datatype = "FieldType";
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/data/" + datatype);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			String jsonAsString = response.asString();
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonAsString.contains((String) jsonsuitetestSuccessData.get("FieldType5")), true);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test()
	public void TC006_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_TextArea() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC006_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_TextArea", "id");
		try {
			String testName = "TC006_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_TextArea";
			reportUpdate(testName);
			headers = generateHeader();
			String datatype = "FieldType";
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/data/" + datatype);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			String jsonAsString = response.asString();
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonAsString.contains((String) jsonsuitetestSuccessData.get("FieldType6")), true);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test()
	public void TC007_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_Radio() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC007_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_Radio", "id");
		try {
			String testName = "TC007_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_Radio";
			reportUpdate(testName);
			headers = generateHeader();
			String datatype = "FieldType";
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/data/" + datatype);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			String jsonAsString = response.asString();
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonAsString.contains((String) jsonsuitetestSuccessData.get("FieldType7")), true);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test()
	public void TC008_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_Checkbox() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC008_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_Checkbox", "id");
		try {
			String testName = "TC008_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_Checkbox";
			reportUpdate(testName);
			headers = generateHeader();
			String datatype = "FieldType";
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/data/" + datatype);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			String jsonAsString = response.asString();
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonAsString.contains((String) jsonsuitetestSuccessData.get("FieldType8")), true);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test()
	public void TC009_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_Video() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC009_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_Video", "id");
		try {
			String testName = "TC009_Superadmin_Verify_if_user_can_get_the_datatype_for_FieldType_Video";
			reportUpdate(testName);
			headers = generateHeader();
			String datatype = "FieldType";
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/data/" + datatype);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			String jsonAsString = response.asString();
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonAsString.contains((String) jsonsuitetestSuccessData.get("FieldType9")), true);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

}
