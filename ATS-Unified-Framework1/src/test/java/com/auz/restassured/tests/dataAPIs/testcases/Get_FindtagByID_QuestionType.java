package com.auz.restassured.tests.dataAPIs.testcases;

import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class Get_FindtagByID_QuestionType extends RestAssuredBase {

	public static HashMap<String, String> headers;
	public static String payLoadBody;
	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("suiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Get Request to get data by passing the datatype as Job Status";
			testSuiteDescription = "Validating in passing the request to get data by passing the datatype as Job Status";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test()
	public void TC001_Superadmin_Verify_if_user_can_get_the_datatype_for_QuestionType_SINGLE_SELECT() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC001_Superadmin_Verify_if_user_can_get_the_datatype_for_QuestionType_SINGLE_SELECT", "id");
		try {
			String testName = "TC001_Superadmin_Verify_if_user_can_get_the_datatype_for_QuestionType_SINGLE_SELECT";
			reportUpdate(testName);
			headers = generateHeader();
			String datatype = "QuestionType";
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/data/" + datatype);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			String jsonAsString = response.asString();
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonAsString.contains((String) jsonsuitetestSuccessData.get("QuestionType1")), true);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test()
	public void TC002_Superadmin_Verify_if_user_can_get_the_datatype_for_QuestionType_MULTI_SELECT() {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC002_Superadmin_Verify_if_user_can_get_the_datatype_for_QuestionType_MULTI_SELECT", "id");
		try {
			String testName = "TC002_Superadmin_Verify_if_user_can_get_the_datatype_for_QuestionType_MULTI_SELECT";
			reportUpdate(testName);
			headers = generateHeader();
			String datatype = "QuestionType";
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/data/" + datatype);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			String jsonAsString = response.asString();
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonAsString.contains((String) jsonsuitetestSuccessData.get("QuestionType2")), true);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test()
	public void TC003_Superadmin_Verify_if_user_can_get_the_datatype_for_QuestionType_ORDERING() {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Superadmin_Verify_if_user_can_get_the_datatype_for_QuestionType_ORDERING", "id");
		try {
			String testName = "TC003_Superadmin_Verify_if_user_can_get_the_datatype_for_QuestionType_ORDERING";
			reportUpdate(testName);
			headers = generateHeader();
			String datatype = "QuestionType";
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/data/" + datatype);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			String jsonAsString = response.asString();
			int responsecode = response.statusCode();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(jsonAsString.contains((String) jsonsuitetestSuccessData.get("QuestionType3")), true);
			Assert.assertEquals(jsonsuitetestSuccessData.get("response_code"), rcode);
			updateTestRailResultAsPass(getTestRailID(testrailObj));
		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

}