package com.auz.restassured.tests.notifications.testcases;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.auz.SupportedUtils.Constants;
import com.auz.SupportedUtils.JsonComponent;
import com.auz.SupportedUtils.MongoDBConnectionManager;
import com.auz.SupportedUtils.RandomJsonGenerator;
import com.auz.restassuredbase.coreutils.RestAssuredBase;
import com.aventstack.extentreports.Status;

import io.restassured.response.Response;

public class PostPutGetDelete_NotificationSettings extends RestAssuredBase {
	public static HashMap<String, String> headers;
	public static String payLoadBody;

	public String strtestdatafilename = "ApiTestData";
	public String strtesttestraildatafilename = "TestRailData";
	JSONObject jsonsuitetestrailData = JsonComponent.getJsonData("suiteLevelData", strtesttestraildatafilename);
	JSONObject jsonsuitetestData = JsonComponent.getJsonData("suiteLevelData", strtestdatafilename);
	JSONObject jsonsuitetestSuccessData = JsonComponent.getJsonData("successTransactionJSONData", strtestdatafilename);
	JSONObject jsonsuitetestErrorData = JsonComponent.getJsonData("errorTransactionJSONData", strtestdatafilename);
	MongoDBConnectionManager db = new MongoDBConnectionManager();
	public static String orgID = "b665ccd67abe4e8dba9d7ffcae488093";
	public static String createdid = "";
	public static String EntityName = "";

	@BeforeTest
	public void setValues() {
		try {
			testSuiteName = "Post request to notification settings";
			testSuiteDescription = "Validating in creating , updating, getting and deleting the notification settings";
		} catch (Exception e) {
			e.printStackTrace();
			reportRequest("Exception occured in @BeforeTest Method while reading the TestDescription", "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void TC001_Superadmin_Verify_user_is_able_to_add_notification_settings_to_system() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC001_Superadmin_Verify_user_is_able_to_add_notification_settings_to_system", "id");
		JSONObject js = new JSONObject();
		EntityName = "CANDIDATEENTITY";
		js.put("event", "event string");
		js.put("entity", EntityName);
		js.put("section", "5f5b4f842fc7db001cbdcf1a");
		js.put("title", "title string");
		try {
			FileWriter file = new FileWriter(System.getProperty("user.dir") + Constants.JSONOBJECT_NOTIFICATION_DIR
					+ "addNotificationSettings.json");
			file.write(js.toJSONString());
			file.close();
			String testName = "TC001_Superadmin_Verify_user_is_able_to_add_notification_settings_to_system";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_NOTIFICATION_DIR
					+ "addNotificationSettings.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/notifications-settings");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(14, 38);
			String rcode = String.valueOf(responsecode);
			System.out.println("created id: " + id);
			createdid = id;
			Assert.assertEquals(true, (jsonAsString.contains(EntityName)));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	public void TC002_Superadmin_Verify_user_is_able_to_get_all_the_template_settings_of_organization()
			throws IOException {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC002_Superadmin_Verify_user_is_able_to_get_all_the_template_settings_of_organization", "id");
		try {
			String testName = "TC002_Superadmin_Verify_user_is_able_to_get_all_template_settings_of_organization";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers, Constants.CREATE_JOB_POST_URL + "/notifications-settings");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, (jsonAsString.contains(EntityName)));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");

		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	public void TC003_Superadmin_Verify_user_is_able_to_add_notification_settings_bulk_to_system() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC003_Superadmin_Verify_user_is_able_to_add_notification_settings_bulk_to_system", "id");
		JSONObject js = new JSONObject();
		JSONObject js1 = new JSONObject();
		JSONArray jsonarray = new JSONArray();
		String updatedEntityName = "BULK CANDIDATE ENTITY";
		js1.put("event", "event string");
		js1.put("entity", updatedEntityName);
		js1.put("section", "5f55ed5a314f95001c824f7d");
		js1.put("title", "title string");
		jsonarray.add(js1);
		js.put("settings", jsonarray);

		try {
			FileWriter file = new FileWriter(System.getProperty("user.dir") + Constants.JSONOBJECT_NOTIFICATION_DIR
					+ "addBulkNotificationSettings.json");
			file.write(js.toJSONString());
			file.close();
			String testName = "TC003_Superadmin_Verify_user_is_able_to_add_notification_settings_bulk_to_system";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_NOTIFICATION_DIR
					+ "addBulkNotificationSettings.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			Response response = postWithHeaderAndForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/notifications-settings/bulk");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "201");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String id = jsonAsString.substring(13, 40);
			String rcode = String.valueOf(responsecode);
			System.out.println("created id: " + id);
			Assert.assertEquals(true, (jsonAsString.contains(updatedEntityName)));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("created_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 7)
	public void TC004_Superadmin_Verify_user_is_able_to_delete_notification_settings_bulk_to_system()
			throws IOException {
		Object testrailObj = jsonsuitetestrailData.getOrDefault(
				"TC004_Superadmin_Verify_user_is_able_to_delete_notification_settings_bulk_to_system", "id");
		try {
			String testName = "TC004_Superadmin_Verify_user_is_able_to_delete_notification_settings_bulk_to_system";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.CREATE_JOB_POST_URL + "/notifications-settings/bulk");
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(false, (jsonAsString.contains(EntityName)));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("deleted_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	public void TC005_Superadmin_Verify_user_is_able_to_get_notification_settings_by_id() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC005_Superadmin_Verify_user_is_able_to_get_notification_settings_by_id", "id");
		try {
			String testName = "TC005_Superadmin_Verify_user_is_able_to_get_notification_settings_by_id";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = getWithHeader(headers,
					Constants.CREATE_JOB_POST_URL + "/notifications-settings/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(true, (jsonAsString.contains(EntityName)));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 5)
	public void TC006_Superadmin_Verify_user_is_able_to_update_notification_settings_by_id() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC006_Superadmin_Verify_user_is_able_to_update_notification_settings_by_id", "id");
		JSONObject js = new JSONObject();
		String updatedSectionName = RandomJsonGenerator.randomNotificationname();
		js.put("name", updatedSectionName);
		try {
			FileWriter file = new FileWriter(System.getProperty("user.dir") + Constants.JSONOBJECT_NOTIFICATION_DIR
					+ "updateNotificationSettings.json");
			file.write(js.toJSONString());
			file.close();
			String testName = "TC006_Superadmin_Verify_user_is_able_to_update_notification_settings_by_id";
			reportUpdate(testName);
			String filePath = System.getProperty("user.dir") + Constants.JSONOBJECT_NOTIFICATION_DIR
					+ "updateNotificationSettings.json";
			payLoadBody = JsonComponent.readTextFile(filePath);
			headers = generateHeader();
			String createdid = "5f0064215a6012001bde2e81";
			Response response = putWithHeaderAndBodyForm(headers, payLoadBody,
					Constants.CREATE_JOB_POST_URL + "/notifications-settings/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "200");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}

	@SuppressWarnings("unchecked")
	@Test(priority = 6)
	public void TC007_Superadmin_Verify_user_is_able_to_delete_notification_settings_by_id() throws IOException {
		Object testrailObj = jsonsuitetestrailData
				.getOrDefault("TC007_Superadmin_Verify_user_is_able_to_delete_notification_settings_by_id", "id");
		try {
			String testName = "TC007_Superadmin_Verify_user_is_able_to_delete_notification_settings_by_id";
			reportUpdate(testName);
			headers = generateHeader();
			Response response = deleteWithHeaderAndPathParamWithoutRequestBody(headers,
					Constants.CREATE_JOB_POST_URL + "/notifications-settings/" + createdid);
			logResponseInReport(response);
			System.out.println("Status code: " + response.statusCode());
			verifyResponseCode(response, "204");
			verifyContentType(response, Constants.JSON_CONTENTTYPE);
			verifyResponseTime(response, 1000);
			int responsecode = response.statusCode();
			String jsonAsString = response.asString();
			String rcode = String.valueOf(responsecode);
			Assert.assertEquals(false, (jsonAsString.contains(EntityName)));
			Assert.assertEquals(rcode, jsonsuitetestSuccessData.get("deleted_response_code"));
			updateTestRailResultAsPass(getTestRailID(testrailObj));

		} catch (Exception e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		} catch (AssertionError e) {
			test.log(Status.ERROR, "StackTrace Result: " + e);
			updateTestRailResultAsFail(getTestRailID(testrailObj));
			Assert.fail(e.getMessage());
			reportRequest("Exception occured while executing the method" + e.getMessage(), "FAIL");
		}
	}
}
